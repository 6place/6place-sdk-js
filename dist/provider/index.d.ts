import { Order } from '../order/types';
import { Product, ProductResponse } from '../products/types';
import { CreateProviderParams, Dashboard, EvaluationParams, HoursParams, OrderResponse, ProductParams, Provider, ProviderEvaluation, ProviderHour, ProviderPayment, ProviderRepresentative, ProviderResponse, ProviderStatus, ProviderTypes, RecommendationParams } from './types';
/**
 * Get all providers
 * @param type one of [[ProviderTypes]]
 * @param status one of [[ProviderStatus]]
 * @returns A promise with a [[ProviderResponse]] with a [[Provider]] array if request is successful
 * @throws Error when the request is not successful
 */
export declare const getProviders: (type?: ProviderTypes | undefined, status?: ProviderStatus | undefined) => Promise<ProviderResponse>;
/**
 * Get single provider details
 * @param id to find the provider
 * @returns A promise with a [[Provider]] object if request was successful
 * @throws Error when the request is not successful
 */
export declare const getProvider: (id: number) => Promise<Provider>;
/**
 * Get all products from provider
 * @param id of the provider
 * @param search search query to filter
 * @param isPromotion set true to search only for products with promotion
 * @param isDetail set to true to fetch exta details of the product
 * @param minValue search for values <= this param
 * @param page number of page to fetch
 * @param limit limit of products to fetch
 * @returns A promise with an array of [[Product]] objects if request was successful
 * @throws Error when the request is not successful
 */
export declare const getProducts: (id: number, search?: string | undefined, isPromotion?: boolean | undefined, isDetail?: boolean | undefined, page?: number | undefined, limit?: number | undefined, minValue?: number | undefined) => Promise<ProductResponse>;
/**
 * Get single product from provider
 * @param id of the provider
 * @param product id of the product to fetch
 * @returns A promise with a [[Product]] object if request was successful
 * @throws Error when the request is not successful
 */
export declare const getProduct: (id: number, product: number) => Promise<Product>;
/**
 * Get provider evaluations
 * @param id of the provider
 * @returns A promise with an array of [[ProviderEvaluation]] objects if request was successful
 * @throws Error when the request is not successful
 */
export declare const getEvaluations: (id: number) => Promise<ProviderEvaluation[]>;
/**
 * Get provider working hours
 * @param id of the provider
 * @returns A promise with an array of [[ProviderHour]] objects if request was successful
 * @throws Error when the request is not successful
 */
export declare const getHours: (id: number) => Promise<ProviderHour[]>;
/**
 * Get provider payment types
 * @param id of the provider
 * @returns A promise with an array of [[ProviderPayment]] objects if request was successful
 * @throws Error when the request is not successful
 */
export declare const getPayments: (id: number) => Promise<ProviderPayment[]>;
/**
 * Get provider representatives list
 * @param id of the provider
 * @returns A promise with an array of [[ProviderRepresentative]] objects if request was successful
 * @throws Error when the request is not successful
 */
export declare const getRepresentatives: (id: number) => Promise<ProviderRepresentative[]>;
/**
 * Cretae a provider
 * @param params Object of type [[CreateProviderParams]]
 * @returns A promise with a boolean indicating if the request was successful or not
 */
export declare const createProvider: (params: CreateProviderParams) => Promise<Provider>;
/**
 * Create a product for a provider
 * @param id of provider to associate with product
 * @param params Object of type [[ProductParams]]
 * @returns A promise with a [[Product]] object if the request was successful
 * @throws Error when request is unsuccessful
 */
export declare const createProduct: (id: number, params: ProductParams) => Promise<Product>;
/**
 * Update a product from a provider
 * @param id of provider to associate with product
 * @param productId id of the product to be updated
 * @param params Object of type [[ProductParams]]
 * @returns A promise with a [[Product]] object if the request was successful
 * @throws Error when request is unsuccessful
 */
export declare const updateProduct: (id: number, productId: number, params: ProductParams) => Promise<Product>;
/**
 * Recommend a provider to the platform
 * @param params Object of type [[RecommendationParams]]
 * @returns A promise with a boolean indicating if the request was successful or not
 */
export declare const recommendProvider: (params: RecommendationParams) => Promise<boolean>;
/**
 * Update provider payments list
 * @param id of provider to add payments to
 * @param payments Array of ids of payment types
 * @returns A promise with a boolean indicating if the request was successful or not
 */
export declare const updatePayments: (id: number, payments: string[]) => Promise<boolean>;
/**
 * Update provider office hours
 * @param id of provider to add payments to
 * @param hours Array of [[HoursParams]] object
 * @returns A promise with a boolean indicating if the request was successful or not
 */
export declare const updateHours: (id: number, hours: HoursParams[]) => Promise<boolean>;
/**
 * Get single provider details
 * @param id of provider to associate with evaluation
 * @param params Object of type [[EvaluationParams]]
 * @returns A promise with a boolean indicating if the request was successful or not
 */
export declare const createEvaluation: (id: number, params: EvaluationParams) => Promise<ProviderEvaluation>;
/**
 * Get orders from provider
 * @param id of provider to get orders
 * @returns A promise with a [[OrderResponse]] with an [[Order]] array request was successful
 * @throws Error when the request is not successful
 */
export declare const getOrders: (id: number) => Promise<OrderResponse>;
/**
 * Get order detail from provider
 * @param id of provider to get order
 * @param order id of the order to fetch
 * @returns A promise with an [[Order]] object if request was successful
 * @throws Error when the request is not successful
 */
export declare const getOrder: (id: number, order: number) => Promise<Order>;
/**
 * Get dashboard data for a given provider id
 * @param id of provider to get dashboard
 * @returns A promise with a [[Dashboard]] if request was successful
 * @throws Error when the request is not successful
 */
export declare const getDashboard: (id: number) => Promise<Dashboard>;
/**
 * Check if document doesn't exist in database
 * @param document to verify
 * @returns A promise with a true if document doesn't exist or false if document exists in database
 * @throws Error when the request is not successful
 */
export declare const checkDocument: (document: string) => Promise<boolean>;
/**
 * Delete a provider product
 * @param id of the provider
 * @param productId id of the product to delete
 * @returns A promise with a boolean indicating if the operation was successful or not
 * @throws Error when the request is not successful
 */
export declare const deleteProduct: (id: number, productId: number) => Promise<boolean>;
