"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const ts_auto_mock_1 = require("ts-auto-mock");
const config_1 = require("../config");
const _1 = require("./");
const types_1 = require("./types");
describe('providers', () => {
    beforeAll(() => { var _a; return (0, config_1.setEnvironment)((_a = process.env.NODE_ENV) !== null && _a !== void 0 ? _a : 'test'); });
    afterAll(config_1._unsetEnvConf);
    it('should get providers successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
        const expected = (0, ts_auto_mock_1.createMock)();
        jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
        const result = yield (0, _1.getProviders)();
        expect(typeof result).toBe('object');
        expect(result).toBe(expected);
    }));
    it('throws error for unfulfilled request', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error('unknown error'));
        yield expect((0, _1.getProviders)()).rejects.toThrow('unknown error');
    }));
    it('should get one provider successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
        const expected = (0, ts_auto_mock_1.createMock)();
        jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
        const result = yield (0, _1.getProvider)(123);
        expect(typeof result).toBe('object');
        expect(result).toBe(expected);
    }));
    it('throws error for unfulfilled request', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error('unknown error'));
        yield expect((0, _1.getProvider)(123)).rejects.toThrow('unknown error');
    }));
    it('should create provider successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
        const params = (0, ts_auto_mock_1.createMock)();
        const expected = (0, ts_auto_mock_1.createMock)();
        jest.spyOn(axios_1.default, 'post').mockResolvedValueOnce({ data: { success: true, data: expected } });
        const result = yield (0, _1.createProvider)(params);
        expect(typeof result).toBe('object');
        expect(result).toBe(expected);
    }));
    it('throws error for unfulfilled requests', () => __awaiter(void 0, void 0, void 0, function* () {
        const params = (0, ts_auto_mock_1.createMock)();
        jest.spyOn(axios_1.default, 'post').mockRejectedValueOnce(new Error('unknown error'));
        yield expect((0, _1.createProvider)(params)).rejects.toThrow('unknown error');
        jest.spyOn(axios_1.default, 'post').mockRejectedValueOnce({
            response: { data: { success: false, error: { code: 408, message: 'Document exists' } } },
        });
        yield expect((0, _1.createProvider)(params)).rejects.toThrow(types_1.DocumentAlreadyExistsError);
        jest.spyOn(axios_1.default, 'post').mockRejectedValueOnce({
            response: { data: { success: false, error: { code: 410, message: 'Document exists' } } },
        });
        yield expect((0, _1.createProvider)(params)).rejects.toThrow(types_1.NameAlreadyExistsError);
    }));
    describe('providers recommendations', () => {
        it('should return true when request is successfull', () => __awaiter(void 0, void 0, void 0, function* () {
            const mockParams = (0, ts_auto_mock_1.createMock)();
            jest.spyOn(axios_1.default, 'post').mockResolvedValueOnce({ data: { success: true } });
            yield expect((0, _1.recommendProvider)(mockParams)).resolves.toBe(true);
        }));
        it('should return false when request is unsuccessfull', () => __awaiter(void 0, void 0, void 0, function* () {
            const mockParams = (0, ts_auto_mock_1.createMock)();
            jest.spyOn(axios_1.default, 'post').mockRejectedValueOnce(new Error('any error whatever'));
            yield expect((0, _1.recommendProvider)(mockParams)).resolves.toBe(false);
        }));
    });
    describe('providers orders', () => {
        it('should get orders successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
            const expected = (0, ts_auto_mock_1.createMock)();
            jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
            const result = yield (0, _1.getOrders)(123);
            expect(typeof result).toBe('object');
            expect(result).toBe(expected);
        }));
        it('throws error for unfulfilled request', () => __awaiter(void 0, void 0, void 0, function* () {
            jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error('unknown error'));
            yield expect((0, _1.getOrders)(123)).rejects.toThrow('unknown error');
        }));
        it('should get single order successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
            const expected = (0, ts_auto_mock_1.createMock)();
            jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
            const result = yield (0, _1.getOrder)(123, 321);
            expect(typeof result).toBe('object');
            expect(result).toBe(expected);
        }));
        it('throws error for unfulfilled request', () => __awaiter(void 0, void 0, void 0, function* () {
            jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error('unknown error'));
            yield expect((0, _1.getOrder)(123, 321)).rejects.toThrow('unknown error');
        }));
    });
    describe('providers products', () => {
        it('should get products successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
            const expected = (0, ts_auto_mock_1.createMock)();
            jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
            const result = yield (0, _1.getProducts)(123);
            expect(typeof result).toBe('object');
            expect(result).toBe(expected);
        }));
        it('throws error for unfulfilled request', () => __awaiter(void 0, void 0, void 0, function* () {
            jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error('unknown error'));
            yield expect((0, _1.getProducts)(123)).rejects.toThrow('unknown error');
        }));
        it('should get single product successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
            const expected = (0, ts_auto_mock_1.createMock)();
            jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
            const result = yield (0, _1.getProduct)(123, 321);
            expect(typeof result).toBe('object');
            expect(result).toBe(expected);
        }));
        it('throws error for unfulfilled request', () => __awaiter(void 0, void 0, void 0, function* () {
            jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error('unknown error'));
            yield expect((0, _1.getProduct)(444, 333)).rejects.toThrow('unknown error');
        }));
        it('should create product successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
            const params = (0, ts_auto_mock_1.createMock)();
            const expected = (0, ts_auto_mock_1.createMock)();
            jest.spyOn(axios_1.default, 'post').mockResolvedValueOnce({ data: { success: true, data: expected } });
            const result = yield (0, _1.createProduct)(123, params);
            expect(typeof result).toBe('object');
            expect(result).toBe(expected);
        }));
        it('throws error for unfulfilled requests', () => __awaiter(void 0, void 0, void 0, function* () {
            const params = (0, ts_auto_mock_1.createMock)();
            jest.spyOn(axios_1.default, 'post').mockRejectedValueOnce(new Error('unknown error'));
            yield expect((0, _1.createProduct)(444, params)).rejects.toThrow('unknown error');
            jest.spyOn(axios_1.default, 'post').mockRejectedValueOnce({
                response: { data: { success: false, error: { code: 99, message: 'Generic Message' } } },
            });
            yield expect((0, _1.createProduct)(444, params)).rejects.toThrow(types_1.MissingParamsError);
            jest.spyOn(axios_1.default, 'post').mockRejectedValueOnce({
                response: { data: { success: false, error: { code: 1400, message: 'Generic Message' } } },
            });
            yield expect((0, _1.createProduct)(444, params)).rejects.toThrow(types_1.ProviderNotFoundError);
            jest.spyOn(axios_1.default, 'post').mockRejectedValueOnce({
                response: { data: { success: false, error: { code: 1600, message: 'Generic Message' } } },
            });
            yield expect((0, _1.createProduct)(444, params)).rejects.toThrow(types_1.ShelfLifeTimeIsNotANumberError);
            jest.spyOn(axios_1.default, 'post').mockRejectedValueOnce({
                response: { data: { success: false, error: { code: 1601, message: 'Generic Message' } } },
            });
            yield expect((0, _1.createProduct)(444, params)).rejects.toThrow(types_1.ShelfLifeTimeValueError);
            jest.spyOn(axios_1.default, 'post').mockRejectedValueOnce({
                response: { data: { success: false, error: { code: 1602, message: 'Generic Message' } } },
            });
            yield expect((0, _1.createProduct)(444, params)).rejects.toThrow(types_1.ShelfLifeTypeIncompatibleError);
            jest.spyOn(axios_1.default, 'post').mockRejectedValueOnce({
                response: { data: { success: false, error: { code: 1700, message: 'Generic Message' } } },
            });
            yield expect((0, _1.createProduct)(444, params)).rejects.toThrow(types_1.SuggestedPriceIsNotANumberError);
            jest.spyOn(axios_1.default, 'post').mockRejectedValueOnce({
                response: { data: { success: false, error: { code: 1701, message: 'Generic Message' } } },
            });
            yield expect((0, _1.createProduct)(444, params)).rejects.toThrow(types_1.SuggestedPriceValueError);
        }));
        it('should update product successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
            const params = (0, ts_auto_mock_1.createMock)();
            const expected = (0, ts_auto_mock_1.createMock)();
            jest.spyOn(axios_1.default, 'put').mockResolvedValueOnce({ data: { success: true, data: expected } });
            const result = yield (0, _1.updateProduct)(123, 321, params);
            expect(typeof result).toBe('object');
            expect(result).toBe(expected);
        }));
        it('throws error for unfulfilled requests', () => __awaiter(void 0, void 0, void 0, function* () {
            const params = (0, ts_auto_mock_1.createMock)();
            jest.spyOn(axios_1.default, 'put').mockRejectedValueOnce(new Error('unknown error'));
            yield expect((0, _1.updateProduct)(444, 333, params)).rejects.toThrow('unknown error');
            jest.spyOn(axios_1.default, 'put').mockRejectedValueOnce({
                response: { data: { success: false, error: { code: 99, message: 'Generic Message' } } },
            });
            yield expect((0, _1.updateProduct)(444, 333, params)).rejects.toThrow(types_1.MissingParamsError);
            jest.spyOn(axios_1.default, 'put').mockRejectedValueOnce({
                response: { data: { success: false, error: { code: 1400, message: 'Generic Message' } } },
            });
            yield expect((0, _1.updateProduct)(444, 333, params)).rejects.toThrow(types_1.ProviderNotFoundError);
            jest.spyOn(axios_1.default, 'put').mockRejectedValueOnce({
                response: { data: { success: false, error: { code: 1600, message: 'Generic Message' } } },
            });
            yield expect((0, _1.updateProduct)(444, 333, params)).rejects.toThrow(types_1.ShelfLifeTimeIsNotANumberError);
            jest.spyOn(axios_1.default, 'put').mockRejectedValueOnce({
                response: { data: { success: false, error: { code: 1601, message: 'Generic Message' } } },
            });
            yield expect((0, _1.updateProduct)(444, 333, params)).rejects.toThrow(types_1.ShelfLifeTimeValueError);
            jest.spyOn(axios_1.default, 'put').mockRejectedValueOnce({
                response: { data: { success: false, error: { code: 1602, message: 'Generic Message' } } },
            });
            yield expect((0, _1.updateProduct)(444, 333, params)).rejects.toThrow(types_1.ShelfLifeTypeIncompatibleError);
            jest.spyOn(axios_1.default, 'put').mockRejectedValueOnce({
                response: { data: { success: false, error: { code: 1700, message: 'Generic Message' } } },
            });
            yield expect((0, _1.updateProduct)(444, 333, params)).rejects.toThrow(types_1.SuggestedPriceIsNotANumberError);
            jest.spyOn(axios_1.default, 'put').mockRejectedValueOnce({
                response: { data: { success: false, error: { code: 1701, message: 'Generic Message' } } },
            });
            yield expect((0, _1.updateProduct)(444, 333, params)).rejects.toThrow(types_1.SuggestedPriceValueError);
        }));
        it('should delete product successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
            jest.spyOn(axios_1.default, 'delete').mockResolvedValueOnce({ data: { success: true } });
            const result = yield (0, _1.deleteProduct)(123, 321);
            expect(typeof result).toBe('boolean');
            expect(result).toStrictEqual(true);
        }));
        it('throws error for unfulfilled request delete', () => __awaiter(void 0, void 0, void 0, function* () {
            jest.spyOn(axios_1.default, 'delete').mockRejectedValueOnce(new Error('unknown error'));
            yield expect((0, _1.deleteProduct)(444, 333)).resolves.toBe(false);
        }));
    });
    describe('evaluations', () => {
        it('should get evaluations successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
            const mockEvaluation = (0, ts_auto_mock_1.createMock)();
            const expected = [mockEvaluation];
            jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
            const result = yield (0, _1.getEvaluations)(123);
            expect(typeof result).toBe('object');
            expect(result).toBe(expected);
        }));
        it('throws error for unfulfilled request', () => __awaiter(void 0, void 0, void 0, function* () {
            jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error('unknown error'));
            yield expect((0, _1.getEvaluations)(123)).rejects.toThrow('unknown error');
        }));
        it('should create evaluation successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
            const params = (0, ts_auto_mock_1.createMock)();
            const expected = (0, ts_auto_mock_1.createMock)();
            jest.spyOn(axios_1.default, 'post').mockResolvedValueOnce({ data: { success: true, data: expected } });
            const result = yield (0, _1.createEvaluation)(123, params);
            expect(typeof result).toBe('object');
            expect(result).toBe(expected);
        }));
        it('throws error for unfulfilled request', () => __awaiter(void 0, void 0, void 0, function* () {
            const params = (0, ts_auto_mock_1.createMock)();
            jest.spyOn(axios_1.default, 'post').mockRejectedValueOnce(new Error('unknown error'));
            yield expect((0, _1.createEvaluation)(444, params)).rejects.toThrow('unknown error');
        }));
    });
    describe('office hours', () => {
        it('should get hours successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
            const mockHour = (0, ts_auto_mock_1.createMock)();
            const expected = [mockHour];
            jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
            const result = yield (0, _1.getHours)(123);
            expect(typeof result).toBe('object');
            expect(result).toBe(expected);
        }));
        it('throws error for unfulfilled request', () => __awaiter(void 0, void 0, void 0, function* () {
            jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error('unknown error'));
            yield expect((0, _1.getHours)(123)).rejects.toThrow('unknown error');
        }));
        it('should update hours successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
            const params = (0, ts_auto_mock_1.createMock)();
            const expected = true;
            jest.spyOn(axios_1.default, 'put').mockResolvedValueOnce({ data: { success: true } });
            const result = yield (0, _1.updateHours)(123, [params]);
            expect(typeof result).toBe('boolean');
            expect(result).toBe(expected);
        }));
        it('should return false on update hours error', () => __awaiter(void 0, void 0, void 0, function* () {
            jest.spyOn(axios_1.default, 'put').mockRejectedValueOnce(new Error('unknown error'));
            yield expect((0, _1.updateHours)(123, [])).resolves.toBe(false);
        }));
    });
    describe('payments', () => {
        it('should get payments successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
            const mockPayment = (0, ts_auto_mock_1.createMock)();
            const expected = [mockPayment];
            jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
            const result = yield (0, _1.getPayments)(123);
            expect(typeof result).toBe('object');
            expect(result).toBe(expected);
        }));
        it('throws error for unfulfilled request', () => __awaiter(void 0, void 0, void 0, function* () {
            jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error('unknown error'));
            yield expect((0, _1.getPayments)(123)).rejects.toThrow('unknown error');
        }));
        it('should update payments successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
            const params = ['234', '342'];
            const expected = true;
            jest.spyOn(axios_1.default, 'put').mockResolvedValueOnce({ data: { success: true } });
            const result = yield (0, _1.updatePayments)(123, params);
            expect(typeof result).toBe('boolean');
            expect(result).toBe(expected);
        }));
        it('should return false on update payments error', () => __awaiter(void 0, void 0, void 0, function* () {
            jest.spyOn(axios_1.default, 'put').mockRejectedValueOnce(new Error('unknown error'));
            yield expect((0, _1.updatePayments)(123, [])).resolves.toBe(false);
        }));
    });
    describe('representatives', () => {
        it('should get representatives successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
            const mockRepresentative = (0, ts_auto_mock_1.createMock)();
            const expected = [mockRepresentative];
            jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
            const result = yield (0, _1.getRepresentatives)(123);
            expect(typeof result).toBe('object');
            expect(result).toBe(expected);
        }));
        it('throws error for unfulfilled request', () => __awaiter(void 0, void 0, void 0, function* () {
            jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error('unknown error'));
            yield expect((0, _1.getRepresentatives)(123)).rejects.toThrow('unknown error');
        }));
    });
    describe('dashboard', () => {
        it('should get dashboard successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
            const expected = (0, ts_auto_mock_1.createMock)();
            jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
            const result = yield (0, _1.getDashboard)(123);
            expect(typeof result).toBe('object');
            expect(result).toBe(expected);
        }));
        it('throws error for unfulfilled request', () => __awaiter(void 0, void 0, void 0, function* () {
            jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error('unknown error'));
            yield expect((0, _1.getDashboard)(123)).rejects.toThrow('unknown error');
        }));
    });
    describe('document', () => {
        it('should return true on successful response', () => __awaiter(void 0, void 0, void 0, function* () {
            jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({ data: { success: true } });
            const result = yield (0, _1.checkDocument)('qwerty');
            expect(typeof result).toBe('boolean');
            expect(result).toBe(true);
        }));
        it('should return false when document already exists', () => __awaiter(void 0, void 0, void 0, function* () {
            jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce({
                response: {
                    data: { success: false, error: { code: 408, message: 'Document already exists' } },
                },
            });
            const result = yield (0, _1.checkDocument)('qwerty');
            expect(typeof result).toBe('boolean');
            expect(result).toBe(false);
        }));
        it('throws error for unfulfilled request', () => __awaiter(void 0, void 0, void 0, function* () {
            jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error('unknown error'));
            yield expect((0, _1.checkDocument)('ytrewq')).rejects.toThrow('unknown error');
        }));
    });
});
