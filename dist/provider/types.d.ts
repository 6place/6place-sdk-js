import { Order } from '../order/types';
import { Payment } from '../payments/types';
import { CustomError } from '../_base/types/error';
export declare enum ProviderTypes {
    DUMMY = "dummy",
    RETAIL = "retail",
    WHOLESALE = "wholesale",
    REPRESENTATION_OFFICE = "representation_office"
}
export declare enum ProviderStatus {
    DISABLE = "disable",
    APPROVED = "approved",
    WAITING_VALIDATION_DATA = "waiting_validation_data"
}
export declare enum ProductDescriptionTypes {
    TEXT = "text",
    HTML = "html"
}
export declare enum ShelfLifeTypes {
    DAYS = "days",
    WEEKS = "weeks",
    MONTHS = "months",
    YEARS = "years"
}
export declare enum PriceTypes {
    WHOLESALE = "wholesale",
    WHOLESALE_PRIMARY = "wholesale_primary",
    RETAIL = "retail"
}
export declare enum PromotionTypes {
    VALUE = "value",
    PERCENTAGE = "percentage"
}
export declare class NameAlreadyExistsError extends CustomError {
}
export declare class DocumentAlreadyExistsError extends CustomError {
}
export declare class CodeInvalidError extends CustomError {
}
export declare class ProviderNotFoundError extends CustomError {
}
export declare class MissingParamsError extends CustomError {
}
export declare class ShelfLifeTimeIsNotANumberError extends CustomError {
}
export declare class ShelfLifeTimeValueError extends CustomError {
}
export declare class ShelfLifeTypeIncompatibleError extends CustomError {
}
export declare class SuggestedPriceIsNotANumberError extends CustomError {
}
export declare class SuggestedPriceValueError extends CustomError {
}
export declare type Unit = {
    id: number;
    name: string;
    cep: string;
    address: string;
    number: string;
    province: string;
    complement?: string;
    city: string;
    state: string;
    country: string;
    latitude: string;
    longitude: string;
    radius: number;
};
export declare type Provider = {
    id: number;
    type?: string;
    status?: string;
    document?: string;
    name: string;
    description?: string;
    shortDescription?: string;
    minValue?: number;
    score?: number;
    commissionPrivate?: number;
    commissionPublic?: number;
    taxTiffinPrivate?: number;
    taxTiffinPublic?: number;
    position?: number;
    freightServiceOn?: boolean;
    freightServiceRealtimeOn?: boolean;
    taxServiceRealtimeOn?: boolean;
    jsonSubAccount?: Object;
    images: Array<{
        imageUrl?: string;
    }>;
    tags?: Array<any>;
    deletedAt?: string;
    createdAt?: string;
    updatedAt?: string;
    units?: Unit[];
};
export declare type Dashboard = {
    chart: {
        labels: Array<string>;
        values: Array<number>;
    };
    totalRevenue: number;
    totalTaxTiffin: number;
    ordersInProgress: number;
    ordersTotal: number;
};
export interface ProviderResponse extends Response {
    count: number;
    rows: Array<Provider>;
}
export interface OrderResponse extends Response {
    count: number;
    rows: Array<Order>;
}
export interface CreateProviderParams {
    document: string;
    name: string;
    type: ProviderTypes;
}
export interface RecommendationParams {
    name: string;
    contactName: string;
    contactPhone: string;
    contactEmail: string;
    address: string;
    number: number;
    city: string;
    state: string;
    complement: string;
}
export interface ProductParams {
    code?: string;
    highlightId?: number;
    unitId: number;
    weight: number;
    name: string;
    description: string;
    descriptionType: ProductDescriptionTypes;
    minimum: number;
    multiplier: number;
    shelfLifeTime?: number;
    shelfLifeType?: ShelfLifeTypes;
    suggestedPrice?: number;
    height?: number;
    width?: number;
    length?: number;
    cubicMeter?: number;
    isVisible?: boolean;
    categories?: number[];
    nutritions?: {
        id: number;
        value: string;
    }[];
    prices?: {
        value: number;
        range?: number;
        type?: PriceTypes;
        commissionPrivate?: number;
        commissionPublic?: number;
        taxTiffinPrivate?: number;
        taxTiffinPublic?: number;
    }[];
    providerUnits?: {
        providerUnitId?: number;
        stock: number;
    };
    promotion?: {
        type: PromotionTypes;
        value: number;
        startedAt?: string;
        finalizedAt?: string;
        showTimer?: boolean;
        spotlight?: boolean;
    };
    images?: ({
        imageBase64: string;
        extension: string;
        position: number;
    } | {
        uri: string;
        position: number;
    })[];
}
export interface EvaluationParams {
    score: number;
    comment?: string;
}
export interface HoursParams {
    id: number;
    startHour: string;
    endHour: string;
}
export declare type ProviderEvaluation = {
    id: number;
    providerId: number;
    userId: number;
    score: number;
    comment?: string;
    answer?: string;
    deletedAt?: string;
    createdAt: string;
    updatedAt: string;
};
export declare type ProviderHour = {
    providerId: number;
    hourId: number;
    startHour: string;
    endHour: string;
    deletedAt?: string;
    createdAt: string;
    updatedAt: string;
};
export declare type ProviderPayment = {
    paymentType: string;
    payment: Payment;
};
export declare type ProviderRepresentative = {
    userId: number;
    providerId: number;
    roleId: number;
    type: string;
    commission?: number;
    deletedAt?: string;
    createdAt: string;
    updatedAt: string;
    user: {
        id: number;
        name: string;
        email: string;
    };
};
