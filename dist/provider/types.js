"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SuggestedPriceValueError = exports.SuggestedPriceIsNotANumberError = exports.ShelfLifeTypeIncompatibleError = exports.ShelfLifeTimeValueError = exports.ShelfLifeTimeIsNotANumberError = exports.MissingParamsError = exports.ProviderNotFoundError = exports.CodeInvalidError = exports.DocumentAlreadyExistsError = exports.NameAlreadyExistsError = exports.PromotionTypes = exports.PriceTypes = exports.ShelfLifeTypes = exports.ProductDescriptionTypes = exports.ProviderStatus = exports.ProviderTypes = void 0;
const error_1 = require("../_base/types/error");
var ProviderTypes;
(function (ProviderTypes) {
    ProviderTypes["DUMMY"] = "dummy";
    ProviderTypes["RETAIL"] = "retail";
    ProviderTypes["WHOLESALE"] = "wholesale";
    ProviderTypes["REPRESENTATION_OFFICE"] = "representation_office";
})(ProviderTypes = exports.ProviderTypes || (exports.ProviderTypes = {}));
var ProviderStatus;
(function (ProviderStatus) {
    ProviderStatus["DISABLE"] = "disable";
    ProviderStatus["APPROVED"] = "approved";
    ProviderStatus["WAITING_VALIDATION_DATA"] = "waiting_validation_data";
})(ProviderStatus = exports.ProviderStatus || (exports.ProviderStatus = {}));
var ProductDescriptionTypes;
(function (ProductDescriptionTypes) {
    ProductDescriptionTypes["TEXT"] = "text";
    ProductDescriptionTypes["HTML"] = "html";
})(ProductDescriptionTypes = exports.ProductDescriptionTypes || (exports.ProductDescriptionTypes = {}));
var ShelfLifeTypes;
(function (ShelfLifeTypes) {
    ShelfLifeTypes["DAYS"] = "days";
    ShelfLifeTypes["WEEKS"] = "weeks";
    ShelfLifeTypes["MONTHS"] = "months";
    ShelfLifeTypes["YEARS"] = "years";
})(ShelfLifeTypes = exports.ShelfLifeTypes || (exports.ShelfLifeTypes = {}));
var PriceTypes;
(function (PriceTypes) {
    PriceTypes["WHOLESALE"] = "wholesale";
    PriceTypes["WHOLESALE_PRIMARY"] = "wholesale_primary";
    PriceTypes["RETAIL"] = "retail";
})(PriceTypes = exports.PriceTypes || (exports.PriceTypes = {}));
var PromotionTypes;
(function (PromotionTypes) {
    PromotionTypes["VALUE"] = "value";
    PromotionTypes["PERCENTAGE"] = "percentage";
})(PromotionTypes = exports.PromotionTypes || (exports.PromotionTypes = {}));
class NameAlreadyExistsError extends error_1.CustomError {
}
exports.NameAlreadyExistsError = NameAlreadyExistsError;
class DocumentAlreadyExistsError extends error_1.CustomError {
}
exports.DocumentAlreadyExistsError = DocumentAlreadyExistsError;
class CodeInvalidError extends error_1.CustomError {
}
exports.CodeInvalidError = CodeInvalidError;
class ProviderNotFoundError extends error_1.CustomError {
}
exports.ProviderNotFoundError = ProviderNotFoundError;
class MissingParamsError extends error_1.CustomError {
}
exports.MissingParamsError = MissingParamsError;
class ShelfLifeTimeIsNotANumberError extends error_1.CustomError {
}
exports.ShelfLifeTimeIsNotANumberError = ShelfLifeTimeIsNotANumberError;
class ShelfLifeTimeValueError extends error_1.CustomError {
}
exports.ShelfLifeTimeValueError = ShelfLifeTimeValueError;
class ShelfLifeTypeIncompatibleError extends error_1.CustomError {
}
exports.ShelfLifeTypeIncompatibleError = ShelfLifeTypeIncompatibleError;
class SuggestedPriceIsNotANumberError extends error_1.CustomError {
}
exports.SuggestedPriceIsNotANumberError = SuggestedPriceIsNotANumberError;
class SuggestedPriceValueError extends error_1.CustomError {
}
exports.SuggestedPriceValueError = SuggestedPriceValueError;
