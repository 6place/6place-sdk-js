"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteProduct = exports.checkDocument = exports.getDashboard = exports.getOrder = exports.getOrders = exports.createEvaluation = exports.updateHours = exports.updatePayments = exports.recommendProvider = exports.updateProduct = exports.createProduct = exports.createProvider = exports.getRepresentatives = exports.getPayments = exports.getHours = exports.getEvaluations = exports.getProduct = exports.getProducts = exports.getProvider = exports.getProviders = void 0;
/**
 * Manages order related methods
 * @module
 */
const axios_1 = __importDefault(require("axios"));
const config_1 = require("../config");
const types_1 = require("./types");
var _bearer = '';
(0, config_1.getBearerAuth)().then(token => { _bearer = token; });
const _getRequestConfig = () => ({
    withCredentials: true,
    headers: {
        authorization: `Bearer ${_bearer}`,
    },
});
/**
 * Get all providers
 * @param type one of [[ProviderTypes]]
 * @param status one of [[ProviderStatus]]
 * @returns A promise with a [[ProviderResponse]] with a [[Provider]] array if request is successful
 * @throws Error when the request is not successful
 */
const getProviders = (type, status) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    const { data } = yield axios_1.default.get(`${(_a = (0, config_1._getEnvConf)()) === null || _a === void 0 ? void 0 : _a.endpoints.api}/v3/provider/`, Object.assign(Object.assign({}, _getRequestConfig()), { params: {
            type,
            status,
        } }));
    return ((data === null || data === void 0 ? void 0 : data.data) || []);
});
exports.getProviders = getProviders;
/**
 * Get single provider details
 * @param id to find the provider
 * @returns A promise with a [[Provider]] object if request was successful
 * @throws Error when the request is not successful
 */
const getProvider = (id) => __awaiter(void 0, void 0, void 0, function* () {
    var _b;
    const { data } = yield axios_1.default.get(`${(_b = (0, config_1._getEnvConf)()) === null || _b === void 0 ? void 0 : _b.endpoints.api}/v1/provider/${id}`, _getRequestConfig());
    return data.data;
});
exports.getProvider = getProvider;
/**
 * Get all products from provider
 * @param id of the provider
 * @param search search query to filter
 * @param isPromotion set true to search only for products with promotion
 * @param isDetail set to true to fetch exta details of the product
 * @param minValue search for values <= this param
 * @param page number of page to fetch
 * @param limit limit of products to fetch
 * @returns A promise with an array of [[Product]] objects if request was successful
 * @throws Error when the request is not successful
 */
const getProducts = (id, search, isPromotion, isDetail, page, limit, minValue) => __awaiter(void 0, void 0, void 0, function* () {
    var _c;
    const params = {
        search,
        isPromotion,
        isDetail,
        page,
        limit,
        minValue,
    };
    const { data } = yield axios_1.default.get(`${(_c = (0, config_1._getEnvConf)()) === null || _c === void 0 ? void 0 : _c.endpoints.api}/v3/provider/${id}/product/`, Object.assign({ params }, _getRequestConfig()));
    return data.data;
});
exports.getProducts = getProducts;
/**
 * Get single product from provider
 * @param id of the provider
 * @param product id of the product to fetch
 * @returns A promise with a [[Product]] object if request was successful
 * @throws Error when the request is not successful
 */
const getProduct = (id, product) => __awaiter(void 0, void 0, void 0, function* () {
    var _d;
    const { data } = yield axios_1.default.get(`${(_d = (0, config_1._getEnvConf)()) === null || _d === void 0 ? void 0 : _d.endpoints.api}/v1/provider/${id}/product/${product}`, _getRequestConfig());
    return data.data;
});
exports.getProduct = getProduct;
/**
 * Get provider evaluations
 * @param id of the provider
 * @returns A promise with an array of [[ProviderEvaluation]] objects if request was successful
 * @throws Error when the request is not successful
 */
const getEvaluations = (id) => __awaiter(void 0, void 0, void 0, function* () {
    var _e;
    const { data } = yield axios_1.default.get(`${(_e = (0, config_1._getEnvConf)()) === null || _e === void 0 ? void 0 : _e.endpoints.api}/v1/provider/${id}/evaluation`, _getRequestConfig());
    return data.data;
});
exports.getEvaluations = getEvaluations;
/**
 * Get provider working hours
 * @param id of the provider
 * @returns A promise with an array of [[ProviderHour]] objects if request was successful
 * @throws Error when the request is not successful
 */
const getHours = (id) => __awaiter(void 0, void 0, void 0, function* () {
    var _f;
    const { data } = yield axios_1.default.get(`${(_f = (0, config_1._getEnvConf)()) === null || _f === void 0 ? void 0 : _f.endpoints.api}/v1/provider/${id}/hour`, _getRequestConfig());
    return data.data;
});
exports.getHours = getHours;
/**
 * Get provider payment types
 * @param id of the provider
 * @returns A promise with an array of [[ProviderPayment]] objects if request was successful
 * @throws Error when the request is not successful
 */
const getPayments = (id) => __awaiter(void 0, void 0, void 0, function* () {
    var _g;
    const { data } = yield axios_1.default.get(`${(_g = (0, config_1._getEnvConf)()) === null || _g === void 0 ? void 0 : _g.endpoints.api}/v1/provider/${id}/payment`, _getRequestConfig());
    return data.data;
});
exports.getPayments = getPayments;
/**
 * Get provider representatives list
 * @param id of the provider
 * @returns A promise with an array of [[ProviderRepresentative]] objects if request was successful
 * @throws Error when the request is not successful
 */
const getRepresentatives = (id) => __awaiter(void 0, void 0, void 0, function* () {
    var _h;
    const { data } = yield axios_1.default.get(`${(_h = (0, config_1._getEnvConf)()) === null || _h === void 0 ? void 0 : _h.endpoints.api}/v1/provider/${id}/representative`, _getRequestConfig());
    return data.data;
});
exports.getRepresentatives = getRepresentatives;
/**
 * Cretae a provider
 * @param params Object of type [[CreateProviderParams]]
 * @returns A promise with a boolean indicating if the request was successful or not
 */
const createProvider = (params) => __awaiter(void 0, void 0, void 0, function* () {
    var _j, _k, _l, _m;
    try {
        const { data } = yield axios_1.default.post(`${(_j = (0, config_1._getEnvConf)()) === null || _j === void 0 ? void 0 : _j.endpoints.api}/v1/provider`, Object.assign({ params }, _getRequestConfig()));
        return data.data;
    }
    catch (e) {
        switch ((_m = (_l = (_k = e.response) === null || _k === void 0 ? void 0 : _k.data) === null || _l === void 0 ? void 0 : _l.error) === null || _m === void 0 ? void 0 : _m.code) {
            case 408:
                throw new types_1.DocumentAlreadyExistsError(e.response.data);
            case 410:
                throw new types_1.NameAlreadyExistsError(e.response.data);
            default:
                throw new Error(e.message);
        }
    }
});
exports.createProvider = createProvider;
/**
 * @private
 */
const _createOrUpdateProduct = (params, id, productId) => __awaiter(void 0, void 0, void 0, function* () {
    var _o, _p, _q, _r;
    try {
        const { data } = yield axios_1.default[productId ? 'put' : 'post'](`${(_o = (0, config_1._getEnvConf)()) === null || _o === void 0 ? void 0 : _o.endpoints.api}/v1/provider/${id}/product/${productId || ''}`, params, _getRequestConfig());
        return data.data;
    }
    catch (e) {
        switch ((_r = (_q = (_p = e.response) === null || _p === void 0 ? void 0 : _p.data) === null || _q === void 0 ? void 0 : _q.error) === null || _r === void 0 ? void 0 : _r.code) {
            case 99:
                throw new types_1.MissingParamsError(e.response.data);
            case 1400:
                throw new types_1.ProviderNotFoundError(e.response.data);
            case 1600:
                throw new types_1.ShelfLifeTimeIsNotANumberError(e.response.data);
            case 1601:
                throw new types_1.ShelfLifeTimeValueError(e.response.data);
            case 1602:
                throw new types_1.ShelfLifeTypeIncompatibleError(e.response.data);
            case 1700:
                throw new types_1.SuggestedPriceIsNotANumberError(e.response.data);
            case 1701:
                throw new types_1.SuggestedPriceValueError(e.response.data);
            default:
                throw new Error(e.message);
        }
    }
});
/**
 * Create a product for a provider
 * @param id of provider to associate with product
 * @param params Object of type [[ProductParams]]
 * @returns A promise with a [[Product]] object if the request was successful
 * @throws Error when request is unsuccessful
 */
const createProduct = (id, params) => __awaiter(void 0, void 0, void 0, function* () {
    return _createOrUpdateProduct(params, id);
});
exports.createProduct = createProduct;
/**
 * Update a product from a provider
 * @param id of provider to associate with product
 * @param productId id of the product to be updated
 * @param params Object of type [[ProductParams]]
 * @returns A promise with a [[Product]] object if the request was successful
 * @throws Error when request is unsuccessful
 */
const updateProduct = (id, productId, params) => __awaiter(void 0, void 0, void 0, function* () {
    return _createOrUpdateProduct(params, id, productId);
});
exports.updateProduct = updateProduct;
/**
 * Recommend a provider to the platform
 * @param params Object of type [[RecommendationParams]]
 * @returns A promise with a boolean indicating if the request was successful or not
 */
const recommendProvider = (params) => __awaiter(void 0, void 0, void 0, function* () {
    var _s;
    try {
        yield axios_1.default.post(`${(_s = (0, config_1._getEnvConf)()) === null || _s === void 0 ? void 0 : _s.endpoints.api}/v1/provider/indicate`, Object.assign({ params }, _getRequestConfig()));
        return true;
    }
    catch (e) {
        return false;
    }
});
exports.recommendProvider = recommendProvider;
/**
 * Update provider payments list
 * @param id of provider to add payments to
 * @param payments Array of ids of payment types
 * @returns A promise with a boolean indicating if the request was successful or not
 */
const updatePayments = (id, payments) => __awaiter(void 0, void 0, void 0, function* () {
    var _t;
    try {
        const { data } = yield axios_1.default.put(`${(_t = (0, config_1._getEnvConf)()) === null || _t === void 0 ? void 0 : _t.endpoints.api}/v1/provider/${id}/payment`, Object.assign({ params: { payments } }, _getRequestConfig()));
        return data.success;
    }
    catch (e) {
        return false;
    }
});
exports.updatePayments = updatePayments;
/**
 * Update provider office hours
 * @param id of provider to add payments to
 * @param hours Array of [[HoursParams]] object
 * @returns A promise with a boolean indicating if the request was successful or not
 */
const updateHours = (id, hours) => __awaiter(void 0, void 0, void 0, function* () {
    var _u;
    try {
        const { data } = yield axios_1.default.put(`${(_u = (0, config_1._getEnvConf)()) === null || _u === void 0 ? void 0 : _u.endpoints.api}/v1/provider/${id}/hour`, Object.assign({ params: { hours } }, _getRequestConfig()));
        return data.success;
    }
    catch (e) {
        return false;
    }
});
exports.updateHours = updateHours;
/**
 * Get single provider details
 * @param id of provider to associate with evaluation
 * @param params Object of type [[EvaluationParams]]
 * @returns A promise with a boolean indicating if the request was successful or not
 */
const createEvaluation = (id, params) => __awaiter(void 0, void 0, void 0, function* () {
    var _v;
    const { data } = yield axios_1.default.post(`${(_v = (0, config_1._getEnvConf)()) === null || _v === void 0 ? void 0 : _v.endpoints.api}/v1/provider/${id}/evaluation`, Object.assign({ params }, _getRequestConfig()));
    return data.data;
});
exports.createEvaluation = createEvaluation;
/**
 * Get orders from provider
 * @param id of provider to get orders
 * @returns A promise with a [[OrderResponse]] with an [[Order]] array request was successful
 * @throws Error when the request is not successful
 */
const getOrders = (id) => __awaiter(void 0, void 0, void 0, function* () {
    var _w;
    const { data } = yield axios_1.default.get(`${(_w = (0, config_1._getEnvConf)()) === null || _w === void 0 ? void 0 : _w.endpoints.api}/v2/provider/${id}/order`, _getRequestConfig());
    return data.data;
});
exports.getOrders = getOrders;
/**
 * Get order detail from provider
 * @param id of provider to get order
 * @param order id of the order to fetch
 * @returns A promise with an [[Order]] object if request was successful
 * @throws Error when the request is not successful
 */
const getOrder = (id, order) => __awaiter(void 0, void 0, void 0, function* () {
    var _x;
    const { data } = yield axios_1.default.get(`${(_x = (0, config_1._getEnvConf)()) === null || _x === void 0 ? void 0 : _x.endpoints.api}/v1/provider/${id}/order/${order}`, _getRequestConfig());
    return data.data;
});
exports.getOrder = getOrder;
/**
 * Get dashboard data for a given provider id
 * @param id of provider to get dashboard
 * @returns A promise with a [[Dashboard]] if request was successful
 * @throws Error when the request is not successful
 */
const getDashboard = (id) => __awaiter(void 0, void 0, void 0, function* () {
    var _y;
    const { data } = yield axios_1.default.get(`${(_y = (0, config_1._getEnvConf)()) === null || _y === void 0 ? void 0 : _y.endpoints.api}/v1/provider/${id}/dashboard`, _getRequestConfig());
    return data.data;
});
exports.getDashboard = getDashboard;
/**
 * Check if document doesn't exist in database
 * @param document to verify
 * @returns A promise with a true if document doesn't exist or false if document exists in database
 * @throws Error when the request is not successful
 */
const checkDocument = (document) => __awaiter(void 0, void 0, void 0, function* () {
    var _z, _0, _1, _2;
    const params = { document };
    try {
        const { data } = yield axios_1.default.get(`${(_z = (0, config_1._getEnvConf)()) === null || _z === void 0 ? void 0 : _z.endpoints.api}/v1/provider/document`, Object.assign({ params }, _getRequestConfig()));
        return data.success;
    }
    catch (e) {
        switch ((_2 = (_1 = (_0 = e.response) === null || _0 === void 0 ? void 0 : _0.data) === null || _1 === void 0 ? void 0 : _1.error) === null || _2 === void 0 ? void 0 : _2.code) {
            case 408: // DOCUMENT_ALREADY_EXISTS
                return false;
            default:
                throw new Error(e.message);
        }
    }
});
exports.checkDocument = checkDocument;
/**
 * Delete a provider product
 * @param id of the provider
 * @param productId id of the product to delete
 * @returns A promise with a boolean indicating if the operation was successful or not
 * @throws Error when the request is not successful
 */
const deleteProduct = (id, productId) => __awaiter(void 0, void 0, void 0, function* () {
    var _3;
    try {
        const { data } = yield axios_1.default.delete(`${(_3 = (0, config_1._getEnvConf)()) === null || _3 === void 0 ? void 0 : _3.endpoints.api}/v1/provider/${id}/product/${productId}`, Object.assign({}, _getRequestConfig()));
        return data.success;
    }
    catch (e) {
        return false;
    }
});
exports.deleteProduct = deleteProduct;
