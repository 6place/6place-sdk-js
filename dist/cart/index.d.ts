import { CartItem, UpdateCartProductParams } from './types';
/**
 * Get Cart
 * @returns A promise with a cart array if request is successful
 * @throws Error when the request is not successful
 */
export declare const getCart: () => Promise<CartItem[]>;
/**
 * Post Cart
 * create cart with all products
 * @throws Error when the request is not successful
 */
export declare const postCreateCart: (params: UpdateCartProductParams) => Promise<CartItem[]>;
