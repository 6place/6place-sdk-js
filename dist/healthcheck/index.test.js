"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const config_1 = require("../config");
const index_1 = require("./index");
describe('index', () => {
    beforeAll(() => {
        var _a;
        (0, config_1.setEnvironment)((_a = process.env.NODE_ENV) !== null && _a !== void 0 ? _a : 'test');
    });
    afterAll(config_1._unsetEnvConf);
    it('gets health check', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({
            data: { name: 'mock-environment', version: '0.0.0-test', env: 'test', uptime: 123456 },
        });
        const healthCheck = yield (0, index_1.getHealthcheck)();
        expect(healthCheck).toHaveProperty('uptime');
        expect(healthCheck.uptime).toBeGreaterThan(0);
    }));
    it('health check not ok', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error('error'));
        yield expect((0, index_1.getHealthcheck)()).rejects.toThrow('error');
    }));
});
