/**
 * HealthCheck object interface
 */
export declare type HealthCheck = {
    /** name of the application */
    name: string;
    /** version tag */
    version: string;
    /** api environment string */
    env: string;
    /** uptime in seconds */
    uptime: number;
};
/**
 * Get health check status from the server
 * @returns Promise with object of type [[HealthCheck]]
 * @throws Error when the request is not successful
 */
export declare const getHealthcheck: () => Promise<HealthCheck>;
