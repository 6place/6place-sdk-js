"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getHealthcheck = void 0;
/**
 * Health check endpoint ping
 * @module
 */
const axios_1 = __importDefault(require("axios"));
const config_1 = require("../config");
/**
 * Get health check status from the server
 * @returns Promise with object of type [[HealthCheck]]
 * @throws Error when the request is not successful
 */
const getHealthcheck = () => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    const result = yield axios_1.default.get(`${(_a = (0, config_1._getEnvConf)()) === null || _a === void 0 ? void 0 : _a.endpoints.api}/v1/`);
    return result.data;
});
exports.getHealthcheck = getHealthcheck;
