"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getAllOrdersByUser = exports.renewBankslip = exports.createPix = exports.createBankslip = exports.getCancellationReasons = exports.updateOrderStatus = exports.updateOrder = exports.createOrder = exports.getOrder = exports.getOrders = void 0;
/**
 * Manages order related methods
 * @module
 */
const axios_1 = __importDefault(require("axios"));
const config_1 = require("../config");
let _bearer = '';
(0, config_1.getBearerAuth)().then((token) => {
    _bearer = token;
});
const _getRequestConfig = () => ({
    withCredentials: true,
    headers: {
        authorization: `Bearer ${_bearer}`,
    },
});
/**
 * Get all orders by user
 * @returns A promise with a order array if request is successful
 * @throws Error when the request is not successful
 */
const getOrders = (providerId) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    const params = { providerId };
    const { data } = yield axios_1.default.get(`${(_a = (0, config_1._getEnvConf)()) === null || _a === void 0 ? void 0 : _a.endpoints.api}/v2/order/`, Object.assign({ params }, _getRequestConfig()));
    return data.data;
});
exports.getOrders = getOrders;
/**
 * Get order by ID
 * @returns A promise with a order object if request is successful
 * @throws Error when the request is not successful
 */
const getOrder = (id) => __awaiter(void 0, void 0, void 0, function* () {
    var _b;
    const params = { id };
    const { data } = yield axios_1.default.get(`${(_b = (0, config_1._getEnvConf)()) === null || _b === void 0 ? void 0 : _b.endpoints.api}/v1/order/${id}`, Object.assign({ params }, _getRequestConfig()));
    return data.data;
});
exports.getOrder = getOrder;
/**
 * Create order
 * @returns A promise void if request is successful
 * @throws Error when the request is not successful
 */
const createOrder = (params) => __awaiter(void 0, void 0, void 0, function* () {
    var _c;
    const { data } = yield axios_1.default.post(`${(_c = (0, config_1._getEnvConf)()) === null || _c === void 0 ? void 0 : _c.endpoints.api}/v1/order/`, params, _getRequestConfig());
    return data.data;
});
exports.createOrder = createOrder;
/**
 * Update order by ID
 * @returns A promise void if request is successful
 * @throws Error when the request is not successful
 */
const updateOrder = (id, body) => __awaiter(void 0, void 0, void 0, function* () {
    var _d;
    const { data } = yield axios_1.default.put(`${(_d = (0, config_1._getEnvConf)()) === null || _d === void 0 ? void 0 : _d.endpoints.api}/v1/order/${id}`, body, _getRequestConfig());
    return data.data;
});
exports.updateOrder = updateOrder;
/**
 * Update Order status by status name
 * @returns A promise void if request is successful
 * @throws Error when the request is not successful
 */
const updateOrderStatus = (id, status) => __awaiter(void 0, void 0, void 0, function* () {
    var _e;
    const { data } = yield axios_1.default.put(`${(_e = (0, config_1._getEnvConf)()) === null || _e === void 0 ? void 0 : _e.endpoints.api}/v1/order/${id}/status`, { status }, _getRequestConfig());
    return data.data;
});
exports.updateOrderStatus = updateOrderStatus;
/**
 * Get all order cancellation reasons
 * @returns A promise with a cancel reasons array if request is successful
 * @throws Error when the request is not successful
 */
const getCancellationReasons = () => __awaiter(void 0, void 0, void 0, function* () {
    var _f;
    const { data } = yield axios_1.default.get(`${(_f = (0, config_1._getEnvConf)()) === null || _f === void 0 ? void 0 : _f.endpoints.api}/v1/order/reasons`, _getRequestConfig());
    return data.data;
});
exports.getCancellationReasons = getCancellationReasons;
/**
 * Create bankslip transaction for Order
 * @returns A promise any if request is successful
 * @throws Error when the request is not successful
 */
const createBankslip = (id, orderId) => __awaiter(void 0, void 0, void 0, function* () {
    var _g;
    const params = { id };
    const body = { orderId };
    const { data } = yield axios_1.default.post(`${(_g = (0, config_1._getEnvConf)()) === null || _g === void 0 ? void 0 : _g.endpoints.api}/v1/order/${id}/bankslip`, Object.assign({ params,
        body }, _getRequestConfig()));
    return data.data;
});
exports.createBankslip = createBankslip;
/**
 * Create pix transaction for Order
 * @returns A promise any if request is successful
 * @throws Error when the request is not successful
 */
const createPix = (id, orderId) => __awaiter(void 0, void 0, void 0, function* () {
    var _h;
    const params = { id };
    const body = { orderId };
    const { data } = yield axios_1.default.post(`${(_h = (0, config_1._getEnvConf)()) === null || _h === void 0 ? void 0 : _h.endpoints.api}/v1/order/${id}/pix`, Object.assign({ params,
        body }, _getRequestConfig()));
    return data.data;
});
exports.createPix = createPix;
/**
 * Renew payment for Order
 * @returns A promise any if request is successful
 * @throws Error when the request is not successful
 */
const renewBankslip = (id, orderId) => __awaiter(void 0, void 0, void 0, function* () {
    var _j;
    const params = { id };
    const body = { orderId };
    const { data } = yield axios_1.default.post(`${(_j = (0, config_1._getEnvConf)()) === null || _j === void 0 ? void 0 : _j.endpoints.api}/v1/order/${id}/payment/renew`, Object.assign({ params,
        body }, _getRequestConfig()));
    return data.data;
});
exports.renewBankslip = renewBankslip;
/**
 * @deprecated in favor of {@link getOrders}
 */
exports.getAllOrdersByUser = exports.getOrders;
