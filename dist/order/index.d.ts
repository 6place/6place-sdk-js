import { Order, CreateOrderParams, UpdateOrderParams, OrderCancelReason } from './types';
/**
 * Get all orders by user
 * @returns A promise with a order array if request is successful
 * @throws Error when the request is not successful
 */
export declare const getOrders: (providerId?: number | undefined) => Promise<{
    count: number;
    rows: Order[];
}>;
/**
 * Get order by ID
 * @returns A promise with a order object if request is successful
 * @throws Error when the request is not successful
 */
export declare const getOrder: (id: number) => Promise<Order>;
/**
 * Create order
 * @returns A promise void if request is successful
 * @throws Error when the request is not successful
 */
export declare const createOrder: (params: CreateOrderParams) => Promise<void>;
/**
 * Update order by ID
 * @returns A promise void if request is successful
 * @throws Error when the request is not successful
 */
export declare const updateOrder: (id: number, body: UpdateOrderParams) => Promise<void>;
/**
 * Update Order status by status name
 * @returns A promise void if request is successful
 * @throws Error when the request is not successful
 */
export declare const updateOrderStatus: (id: number, status: string) => Promise<void>;
/**
 * Get all order cancellation reasons
 * @returns A promise with a cancel reasons array if request is successful
 * @throws Error when the request is not successful
 */
export declare const getCancellationReasons: () => Promise<OrderCancelReason[]>;
/**
 * Create bankslip transaction for Order
 * @returns A promise any if request is successful
 * @throws Error when the request is not successful
 */
export declare const createBankslip: (id: number, orderId: number) => Promise<any>;
/**
 * Create pix transaction for Order
 * @returns A promise any if request is successful
 * @throws Error when the request is not successful
 */
export declare const createPix: (id: number, orderId: number) => Promise<any>;
/**
 * Renew payment for Order
 * @returns A promise any if request is successful
 * @throws Error when the request is not successful
 */
export declare const renewBankslip: (id: number, orderId: number) => Promise<any>;
/**
 * @deprecated in favor of {@link getOrders}
 */
export declare const getAllOrdersByUser: (providerId?: number | undefined) => Promise<{
    count: number;
    rows: Order[];
}>;
