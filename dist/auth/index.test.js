"use strict";
/**
 * @jest-environment jsdom
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const config_1 = require("../config");
const index_1 = require("./index");
describe('auth with environment', () => {
    beforeAll(() => { var _a; return (0, config_1.setEnvironment)((_a = process.env.NODE_ENV) !== null && _a !== void 0 ? _a : 'test'); });
    afterAll(config_1._unsetEnvConf);
    it('should return true when login request is successful', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'post').mockResolvedValueOnce({ data: {} });
        expect(yield (0, index_1.loginUser)('mock-valid-username', '123456')).toBe(true);
    }));
    it('should return boolean when status is requested', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({ data: { data: { loggedIn: true } } });
        expect(yield (0, index_1.isLoggedIn)()).toBe(true);
        jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({ data: { data: { loggedIn: false } } });
        expect(yield (0, index_1.isLoggedIn)()).toBe(false);
        jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error());
        expect(yield (0, index_1.isLoggedIn)()).toBe(false);
    }));
    it('refresh should return true when request is successful', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'post').mockResolvedValueOnce({ data: {} });
        expect(yield (0, index_1._refreshToken)()).toBe(true);
    }));
    it('should always return true on logout', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'post').mockResolvedValueOnce({ data: {} });
        expect(yield (0, index_1.logoutUser)()).toBe(true);
        jest.spyOn(axios_1.default, 'post').mockRejectedValueOnce(new Error());
        expect(yield (0, index_1.logoutUser)()).toBe(true);
    }));
});
