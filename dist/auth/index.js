"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.logoutUser = exports._refreshToken = exports.isLoggedIn = exports.loginUser = void 0;
/**
 * Manages user authentication methods and session data
 * @module
 */
const axios_1 = __importDefault(require("axios"));
const config_1 = require("../config");
const _bearer = (0, config_1.getBearerAuth)();
const _getRequestConfig = () => ({
    withCredentials: true,
    headers: {
        authorization: `Bearer ${_bearer}`,
    },
});
/**
 * Logs user using cookies to manage session.
 * @param username e-mail used to login
 * @param password password in plain text
 * @returns A promise with a boolean true if login was successful
 * @throws Error when the request is not successful
 */
const loginUser = (username, password) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    const params = new URLSearchParams();
    var token = '';
    params.append('username', username);
    params.append('password', password);
    params.append('grant_type', 'password');
    const response = yield axios_1.default.post(`${(_a = (0, config_1._getEnvConf)()) === null || _a === void 0 ? void 0 : _a.endpoints.api}/v1/oauth/token`, params, {
        withCredentials: true,
        headers: {
            authorization: `Basic ZDUwZTA1ZTJjN2M5NmZmMzY0NjYwYTdiOWM5M2ZmNDc6QlQ5bUt5a3lQVFBtR000RTlLTGRnMkR2dEdwM2pFRzV1S0RQUnQ1QnlLUHcyR0Z5eXFFQXFndmRjOUNHWUZlUXpmSFhnaw==`,
        },
    });
    var authResult = response.data;
    if (authResult.token_type == "Bearer") {
        yield (0, config_1.setBearerAuth)(authResult.access_token);
        token = authResult.access_token;
    }
    return token;
});
exports.loginUser = loginUser;
/**
 * Checks if the user is logged in by refreshing token
 * @returns boolean
 */
const isLoggedIn = () => __awaiter(void 0, void 0, void 0, function* () {
    var _b;
    try {
        const status = yield axios_1.default.get(`${(_b = (0, config_1._getEnvConf)()) === null || _b === void 0 ? void 0 : _b.endpoints.api}/v1/oauth/status`, _getRequestConfig());
        return status.data.data.loggedIn;
    }
    catch (err) {
        return false;
    }
});
exports.isLoggedIn = isLoggedIn;
/**
 * Refreshes token
 * @internal
 * @returns Promise with boolean true
 * @throws Error when the request is not successful
 */
const _refreshToken = () => __awaiter(void 0, void 0, void 0, function* () {
    var _c;
    const params = new URLSearchParams();
    params.append('grant_type', 'refresh_token');
    yield axios_1.default.post(`${(_c = (0, config_1._getEnvConf)()) === null || _c === void 0 ? void 0 : _c.endpoints.api}/v1/oauth/token`, params, _getRequestConfig());
    return true;
});
exports._refreshToken = _refreshToken;
/**
 * Logs out the user from the API, erasing session data and related cookies
 * @returns Promise with boolean true always
 */
const logoutUser = () => __awaiter(void 0, void 0, void 0, function* () {
    var _d;
    try {
        const params = new URLSearchParams();
        yield axios_1.default.post(`${(_d = (0, config_1._getEnvConf)()) === null || _d === void 0 ? void 0 : _d.endpoints.api}/v1/oauth/revoke`, params, _getRequestConfig());
    }
    catch (err) { }
    if (typeof window !== 'undefined')
        localStorage.removeItem('token');
    return true;
});
exports.logoutUser = logoutUser;
