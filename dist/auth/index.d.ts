/**
 * Logs user using cookies to manage session.
 * @param username e-mail used to login
 * @param password password in plain text
 * @returns A promise with a boolean true if login was successful
 * @throws Error when the request is not successful
 */
export declare const loginUser: (username: string, password: string) => Promise<string>;
/**
 * Checks if the user is logged in by refreshing token
 * @returns boolean
 */
export declare const isLoggedIn: () => Promise<boolean>;
/**
 * Refreshes token
 * @internal
 * @returns Promise with boolean true
 * @throws Error when the request is not successful
 */
export declare const _refreshToken: () => Promise<boolean>;
/**
 * Logs out the user from the API, erasing session data and related cookies
 * @returns Promise with boolean true always
 */
export declare const logoutUser: () => Promise<boolean>;
