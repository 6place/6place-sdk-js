import { Faq } from './types';
/**
 * Get all faqs
 * @returns A promise with a categories array if request is successful
 * @throws Error when the request is not successful
 */
export declare const getAllFaqs: () => Promise<Faq[]>;
/**
 * Get one faq by id
 * @param faqId to find by that id
 * @returns A promise with a highlight if request was successful
 * @throws Error when the request is not successful
 */
export declare const getFaq: (faqId: number) => Promise<Faq>;
