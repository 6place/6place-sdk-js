export declare type Faq = {
    id: number;
    question: string;
    answer: number;
    deletedAt: string;
    createdAt: string;
    updatedAt: string;
};
