/**
 * Application configurations methods
 * @module
 */
declare const config: {
    development: {
        endpoints: {
            api: string;
            chat: string;
        };
    };
    developmentv1: {
        endpoints: {
            api: string;
            chat: string;
        };
    };
    developmentv2: {
        endpoints: {
            api: string;
            chat: string;
        };
    };
    test: {
        endpoints: {
            api: string;
            chat: string;
        };
    };
    staging: {
        endpoints: {
            api: string;
            chat: string;
        };
    };
    production: {
        endpoints: {
            api: string;
            chat: string;
        };
    };
};
export declare type ConfigKeys = keyof typeof config;
export declare type Config = typeof config[ConfigKeys];
export declare let envConfig: null | Config;
/**
 * Error for when setting an unexistent environment
 */
export declare const UNKNOWN_ENV_ERROR: Error;
/**
 * Error for when the user is trying to use the API without setting an environment first
 */
export declare const UNSET_ENV_ERROR: Error;
/**
 * Get configuration for current environment
 * @returns object with the current environment configuration
 * @throws [[UNSET_ENV_ERROR]]
 */
export declare const _getEnvConf: () => Config;
/**
 * Get configuration for current environment
 * @returns object with the current environment configuration
 * @throws [[UNSET_ENV_ERROR]]
 */
export declare const _unsetEnvConf: () => null;
/**
 * Set and keep environment set from the app
 * @returns object with configuration from the env set
 * @throws [[UNKNOWN_ENV_ERROR]]
 */
export declare const setEnvironment: (env: string) => Config;
/**
 * Gets string fot Basic Authorization header to communicate with API
 * @ignore
 */
export declare const getBearerAuth: () => Promise<string | null>;
export declare const setBearerAuth: (token: string) => Promise<void>;
export declare const getBasicAuth: () => string;
export {};
