"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = require("./index");
let testConfig = null;
describe('config', () => {
    afterAll(index_1._unsetEnvConf);
    it('throws error when env not set', () => {
        expect(index_1._getEnvConf).toThrow(index_1.UNSET_ENV_ERROR);
    });
    it('throws error when setting wrong env', () => {
        expect(() => (0, index_1.setEnvironment)('wrong_environment_123')).toThrow(index_1.UNKNOWN_ENV_ERROR);
    });
    it('sets environment returning env config', () => {
        var _a;
        testConfig = (0, index_1.setEnvironment)((_a = process.env.NODE_ENV) !== null && _a !== void 0 ? _a : 'test');
        expect(typeof testConfig).toBe('object');
        expect(testConfig).toHaveProperty('endpoints');
        expect(testConfig.endpoints).toHaveProperty('api');
        expect(testConfig.endpoints).toHaveProperty('chat');
    });
    it('sets environment returning env config', () => {
        var _a;
        const newTestConfig = (0, index_1.setEnvironment)((_a = process.env.NODE_ENV) !== null && _a !== void 0 ? _a : 'test');
        expect(newTestConfig).toBe(testConfig);
    });
    it('get env config correctly', () => {
        const newTestConfig = (0, index_1._getEnvConf)();
        expect(newTestConfig).toBe(testConfig);
    });
});
