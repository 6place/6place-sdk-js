"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getBasicAuth = exports.setBearerAuth = exports.getBearerAuth = exports.setEnvironment = exports._unsetEnvConf = exports._getEnvConf = exports.UNSET_ENV_ERROR = exports.UNKNOWN_ENV_ERROR = exports.envConfig = void 0;
/**
 * Application configurations methods
 * @module
 */
const config = {
    development: {
        endpoints: { api: 'http://localhost:3333', chat: 'http://localhost:3334' },
    },
    developmentv1: {
        endpoints: { api: 'https://api.dev.v1.b2b.6place.com.br', chat: 'https://chat.dev.v1.b2b.6place.com.br' },
    },
    developmentv2: {
        endpoints: { api: 'https://api.dev.v2.b2b.6place.com.br', chat: 'https://chat.dev.v2.b2b.6place.com.br' },
    },
    test: {
        endpoints: { api: 'http://localhost:3333', chat: 'http://localhost:3334' },
    },
    staging: {
        endpoints: {
            api: 'https://api.b2b.staging.6place.com.br',
            chat: 'https://chat.b2b.staging.6place.com.br',
        },
    },
    production: {
        endpoints: { api: 'https://api.b2b.6place.com.br', chat: 'https://chat.b2b.6place.com.br' },
    },
};
exports.envConfig = null;
/**
 * Error for when setting an unexistent environment
 */
exports.UNKNOWN_ENV_ERROR = new Error(`Environment does not exist. Please use one of the following: [${Object.keys(config).join(', ')}]`);
/**
 * Error for when the user is trying to use the API without setting an environment first
 */
exports.UNSET_ENV_ERROR = new Error(`Environment not set. Please set environment before using the SDK: [${Object.keys(config).join(', ')}]`);
/**
 * Get configuration for current environment
 * @returns object with the current environment configuration
 * @throws [[UNSET_ENV_ERROR]]
 */
const _getEnvConf = () => {
    if (exports.envConfig === null) {
        throw exports.UNSET_ENV_ERROR;
    }
    return exports.envConfig;
};
exports._getEnvConf = _getEnvConf;
/**
 * Get configuration for current environment
 * @returns object with the current environment configuration
 * @throws [[UNSET_ENV_ERROR]]
 */
const _unsetEnvConf = () => {
    return (exports.envConfig = null);
};
exports._unsetEnvConf = _unsetEnvConf;
/**
 * Set and keep environment set from the app
 * @returns object with configuration from the env set
 * @throws [[UNKNOWN_ENV_ERROR]]
 */
const setEnvironment = (env) => {
    if (exports.envConfig !== null) {
        return exports.envConfig;
    }
    if (!(env in config)) {
        throw exports.UNKNOWN_ENV_ERROR;
    }
    return (exports.envConfig = config[env]);
};
exports.setEnvironment = setEnvironment;
/**
 * Gets string fot Basic Authorization header to communicate with API
 * @ignore
 */
const getBearerAuth = () => __awaiter(void 0, void 0, void 0, function* () {
    let item = '';
    if (typeof window !== 'undefined') {
        item = yield localStorage.getItem('token');
    }
    return item;
});
exports.getBearerAuth = getBearerAuth;
const setBearerAuth = (token) => __awaiter(void 0, void 0, void 0, function* () {
    if (typeof window !== 'undefined') {
        yield localStorage.setItem('token', token);
    }
});
exports.setBearerAuth = setBearerAuth;
const getBasicAuth = () => 'ZDUwZTA1ZTJjN2M5NmZmMzY0NjYwYTdiOWM5M2ZmNDc6QlQ5bUt5a3lQVFBtR000RTlLTGRnMkR2dEdwM2pFRzV1S0RQUnQ1QnlLUHcyR0Z5eXFFQXFndmRjOUNHWUZlUXpmSFhnaw==';
exports.getBasicAuth = getBasicAuth;
