"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomError = void 0;
class CustomError extends Error {
    constructor(errorData) {
        var _a;
        super(((_a = errorData.error) === null || _a === void 0 ? void 0 : _a.message) || 'Unknown Error');
        this.success = errorData.success;
        this.error = errorData.error;
    }
}
exports.CustomError = CustomError;
