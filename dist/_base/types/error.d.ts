export interface ErrorData {
    code: number;
    message: string;
    details?: string;
    stack?: string;
    logging?: boolean;
}
export interface CustomErrorInterface {
    success: boolean;
    error: ErrorData;
}
export declare class CustomError extends Error {
    success: boolean;
    error: ErrorData;
    constructor(errorData: CustomErrorInterface);
}
