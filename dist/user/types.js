"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserInvalidTypeError = exports.UserInvalidNameError = exports.CodeInvalidError = exports.CodeInvalidDateError = exports.CodeExpiredError = exports.CnpjaError = exports.InvalidCompanyError = exports.PhoneAlreadyExistsError = exports.DocumentAlreadyExistsError = exports.EmailAlreadyExistsError = exports.InvalidEmailError = exports.ValidationTypes = exports.ProviderTypesEnum = exports.DocumentTypesEnum = exports.UserTypesEnum = void 0;
const error_1 = require("../_base/types/error");
var UserTypesEnum;
(function (UserTypesEnum) {
    UserTypesEnum["NATURAL"] = "user_natural";
    UserTypesEnum["LEGAL"] = "user_legal";
})(UserTypesEnum = exports.UserTypesEnum || (exports.UserTypesEnum = {}));
var DocumentTypesEnum;
(function (DocumentTypesEnum) {
    DocumentTypesEnum["CNPJ"] = "cnpj";
    DocumentTypesEnum["CPF"] = "cpf";
})(DocumentTypesEnum = exports.DocumentTypesEnum || (exports.DocumentTypesEnum = {}));
var ProviderTypesEnum;
(function (ProviderTypesEnum) {
    ProviderTypesEnum["RETAIL"] = "retail";
    ProviderTypesEnum["WHOLESALE"] = "wholesale";
})(ProviderTypesEnum = exports.ProviderTypesEnum || (exports.ProviderTypesEnum = {}));
var ValidationTypes;
(function (ValidationTypes) {
    ValidationTypes["PHONE"] = "phone";
    ValidationTypes["EMAIL"] = "email";
})(ValidationTypes = exports.ValidationTypes || (exports.ValidationTypes = {}));
class InvalidEmailError extends error_1.CustomError {
}
exports.InvalidEmailError = InvalidEmailError;
class EmailAlreadyExistsError extends error_1.CustomError {
}
exports.EmailAlreadyExistsError = EmailAlreadyExistsError;
class DocumentAlreadyExistsError extends error_1.CustomError {
}
exports.DocumentAlreadyExistsError = DocumentAlreadyExistsError;
class PhoneAlreadyExistsError extends error_1.CustomError {
}
exports.PhoneAlreadyExistsError = PhoneAlreadyExistsError;
class InvalidCompanyError extends error_1.CustomError {
}
exports.InvalidCompanyError = InvalidCompanyError;
class CnpjaError extends error_1.CustomError {
}
exports.CnpjaError = CnpjaError;
class CodeExpiredError extends error_1.CustomError {
}
exports.CodeExpiredError = CodeExpiredError;
class CodeInvalidDateError extends error_1.CustomError {
}
exports.CodeInvalidDateError = CodeInvalidDateError;
class CodeInvalidError extends error_1.CustomError {
}
exports.CodeInvalidError = CodeInvalidError;
class UserInvalidNameError extends error_1.CustomError {
}
exports.UserInvalidNameError = UserInvalidNameError;
class UserInvalidTypeError extends error_1.CustomError {
}
exports.UserInvalidTypeError = UserInvalidTypeError;
