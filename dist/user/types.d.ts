import { Provider } from '../provider/types';
import { CustomError } from '../_base/types/error';
export declare enum UserTypesEnum {
    NATURAL = "user_natural",
    LEGAL = "user_legal"
}
export declare enum DocumentTypesEnum {
    CNPJ = "cnpj",
    CPF = "cpf"
}
export declare enum ProviderTypesEnum {
    RETAIL = "retail",
    WHOLESALE = "wholesale"
}
export declare enum ValidationTypes {
    PHONE = "phone",
    EMAIL = "email"
}
export declare class InvalidEmailError extends CustomError {
}
export declare class EmailAlreadyExistsError extends CustomError {
}
export declare class DocumentAlreadyExistsError extends CustomError {
}
export declare class PhoneAlreadyExistsError extends CustomError {
}
export declare class InvalidCompanyError extends CustomError {
}
export declare class CnpjaError extends CustomError {
}
export declare class CodeExpiredError extends CustomError {
}
export declare class CodeInvalidDateError extends CustomError {
}
export declare class CodeInvalidError extends CustomError {
}
export declare class UserInvalidNameError extends CustomError {
}
export declare class UserInvalidTypeError extends CustomError {
}
export declare type CreateUserType = {
    email: string;
    password: string;
    type: UserTypesEnum;
    name: string;
    phone: string;
    document: string;
    documentType: DocumentTypesEnum;
    providerType: ProviderTypesEnum;
};
export declare type UpdateUserType = {
    email: string;
    type: UserTypesEnum;
    name: string;
    phone: string;
    document: string;
    documentType: DocumentTypesEnum;
    status: string;
    ie: string;
    active: boolean;
    validated: boolean;
    companyName: string;
};
export declare type Support = {
    id: number;
    email: string;
    phone: string;
    document: string;
    documentType: string;
    ie: string;
    type: string;
    name: string;
    photoUrl?: string;
};
export declare type User = {
    id: number;
    email: string;
    emailValidated: boolean;
    phone: string;
    phoneValidated: boolean;
    photoUrl?: string;
    validated: boolean;
    name: string;
    status: string;
    type: string;
    document: string;
    documentType: string;
    createdBy?: string;
    companyName: string;
    ie: string;
    fup: boolean;
    lastFupAt?: any;
    nextFupAt?: any;
    originId: number;
    companyTypeId: number;
    commentsSupport: string;
    supportId: number;
    supportStatusId: number;
    createdAt: string;
    origin: {
        id: number;
        name: string;
    };
    companyType: {
        id: number;
        name: string;
    };
    supportStatus: {
        id: number;
        name: string;
    };
    support: Support;
};
export declare type DetailsResponse = {
    user: User;
    providers: ProviderDefinition[];
};
export declare type ProviderDefinition = {
    provider: Provider;
    type: string;
    commission: number;
};
