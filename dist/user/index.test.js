"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const ts_auto_mock_1 = require("ts-auto-mock");
const config_1 = require("../config");
const index_1 = require("./index");
const types_1 = require("./types");
describe('password recovery', () => {
    beforeAll(() => {
        var _a;
        (0, config_1.setEnvironment)((_a = process.env.NODE_ENV) !== null && _a !== void 0 ? _a : 'test');
    });
    afterAll(config_1._unsetEnvConf);
    it('recovers password for existent user', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'post').mockResolvedValueOnce({
            data: { success: true },
        });
        const result = yield (0, index_1.recoverPassword)('existing-user@domain.com');
        expect(result).toBe(true);
    }));
    it('throws error for inexistent user', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'post').mockRejectedValueOnce(new Error('error'));
        yield expect((0, index_1.recoverPassword)('i.dont.know.who@that.is')).rejects.toThrow('error');
    }));
});
describe('user creation', () => {
    const userResponseMock = (0, ts_auto_mock_1.createMock)();
    beforeAll(() => {
        var _a;
        (0, config_1.setEnvironment)((_a = process.env.NODE_ENV) !== null && _a !== void 0 ? _a : 'test');
    });
    afterAll(config_1._unsetEnvConf);
    const userMock = (0, ts_auto_mock_1.createMock)();
    it('successfuly creates user with valid data', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'post').mockResolvedValueOnce({
            data: { success: true, data: userResponseMock },
        });
        expect(yield (0, index_1.createUser)(userMock)).toBe(userResponseMock);
    }));
    it('should throw InvalidEmailError correctly', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'post').mockRejectedValueOnce({
            response: {
                data: {
                    success: false,
                    error: { code: 201 },
                },
            },
        });
        yield expect((0, index_1.createUser)(userMock)).rejects.toThrow(types_1.InvalidEmailError);
    }));
    it('should throw EmailAlreadyExistsError correctly', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'post').mockRejectedValueOnce({
            response: {
                data: {
                    success: false,
                    error: { code: 401 },
                },
            },
        });
        yield expect((0, index_1.createUser)(userMock)).rejects.toThrow(types_1.EmailAlreadyExistsError);
    }));
    it('should throw UserInvalidNameError correctly', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'post').mockRejectedValueOnce({
            response: {
                data: {
                    success: false,
                    error: { code: 402 },
                },
            },
        });
        yield expect((0, index_1.createUser)(userMock)).rejects.toThrow(types_1.UserInvalidNameError);
    }));
    it('should throw UserInvalidTypeError correctly', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'post').mockRejectedValueOnce({
            response: {
                data: {
                    success: false,
                    error: { code: 404 },
                },
            },
        });
        yield expect((0, index_1.createUser)(userMock)).rejects.toThrow(types_1.UserInvalidTypeError);
    }));
    it('should throw DocumentAlreadyExistsError correctly', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'post').mockRejectedValueOnce({
            response: {
                data: {
                    success: false,
                    error: { code: 408 },
                },
            },
        });
        yield expect((0, index_1.createUser)(userMock)).rejects.toThrow(types_1.DocumentAlreadyExistsError);
    }));
    it('should throw PhoneAlreadyExistsError correctly', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'post').mockRejectedValueOnce({
            response: {
                data: {
                    success: false,
                    error: { code: 411 },
                },
            },
        });
        yield expect((0, index_1.createUser)(userMock)).rejects.toThrow(types_1.PhoneAlreadyExistsError);
    }));
    it('throws error when request is not successful', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'post').mockRejectedValueOnce(new Error('error'));
        yield expect((0, index_1.createUser)(userMock)).rejects.toThrow('error');
    }));
});
describe('user update', () => {
    const userResponseMock = (0, ts_auto_mock_1.createMock)();
    beforeAll(() => {
        var _a;
        (0, config_1.setEnvironment)((_a = process.env.NODE_ENV) !== null && _a !== void 0 ? _a : 'test');
    });
    afterAll(config_1._unsetEnvConf);
    const userMock = (0, ts_auto_mock_1.createMock)();
    it('successfuly creates user with valid data', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'put').mockResolvedValueOnce({
            data: { success: true, data: userResponseMock },
        });
        expect(yield (0, index_1.updateUser)(userMock)).toBe(userResponseMock);
    }));
    it('should throw InvalidEmailError correctly', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'put').mockRejectedValueOnce({
            response: {
                data: {
                    success: false,
                    error: { code: 201 },
                },
            },
        });
        yield expect((0, index_1.updateUser)(userMock)).rejects.toThrow(types_1.InvalidEmailError);
    }));
    it('should throw EmailAlreadyExistsError correctly', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'put').mockRejectedValueOnce({
            response: {
                data: {
                    success: false,
                    error: { code: 401 },
                },
            },
        });
        yield expect((0, index_1.updateUser)(userMock)).rejects.toThrow(types_1.EmailAlreadyExistsError);
    }));
    it('should throw UserInvalidNameError correctly', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'put').mockRejectedValueOnce({
            response: {
                data: {
                    success: false,
                    error: { code: 402 },
                },
            },
        });
        yield expect((0, index_1.updateUser)(userMock)).rejects.toThrow(types_1.UserInvalidNameError);
    }));
    it('should throw UserInvalidTypeError correctly', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'put').mockRejectedValueOnce({
            response: {
                data: {
                    success: false,
                    error: { code: 404 },
                },
            },
        });
        yield expect((0, index_1.updateUser)(userMock)).rejects.toThrow(types_1.UserInvalidTypeError);
    }));
    it('should throw DocumentAlreadyExistsError correctly', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'put').mockRejectedValueOnce({
            response: {
                data: {
                    success: false,
                    error: { code: 408 },
                },
            },
        });
        yield expect((0, index_1.updateUser)(userMock)).rejects.toThrow(types_1.DocumentAlreadyExistsError);
    }));
    it('should throw PhoneAlreadyExistsError correctly', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'put').mockRejectedValueOnce({
            response: {
                data: {
                    success: false,
                    error: { code: 411 },
                },
            },
        });
        yield expect((0, index_1.updateUser)(userMock)).rejects.toThrow(types_1.PhoneAlreadyExistsError);
    }));
    it('throws error when request is not successful', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'put').mockRejectedValueOnce(new Error('error'));
        yield expect((0, index_1.updateUser)(userMock)).rejects.toThrow('error');
    }));
});
describe('user details', () => {
    const detailsMock = (0, ts_auto_mock_1.createMock)();
    beforeAll(() => {
        var _a;
        (0, config_1.setEnvironment)((_a = process.env.NODE_ENV) !== null && _a !== void 0 ? _a : 'test');
    });
    afterAll(config_1._unsetEnvConf);
    it('successfuly gets user details with valid data', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({
            data: { success: true, data: detailsMock },
        });
        expect(yield (0, index_1.getDetails)()).toBe(detailsMock);
    }));
    it('throws error when request is not successful', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error('error'));
        yield expect((0, index_1.getDetails)()).rejects.toThrow('error');
    }));
});
describe('user origins', () => {
    const responseMock = [{ id: 1, name: 'origin 1' }];
    beforeAll(() => {
        var _a;
        (0, config_1.setEnvironment)((_a = process.env.NODE_ENV) !== null && _a !== void 0 ? _a : 'test');
    });
    afterAll(config_1._unsetEnvConf);
    it('successfuly gets user origins with valid data', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({
            data: { success: true, data: responseMock },
        });
        expect(yield (0, index_1.getOrigins)()).toBe(responseMock);
    }));
    it('throws error when request is not successful', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error('error'));
        yield expect((0, index_1.getOrigins)()).rejects.toThrow('error');
    }));
});
describe('user company types', () => {
    const responseMock = [{ id: 1, name: 'company type 1' }];
    beforeAll(() => {
        var _a;
        (0, config_1.setEnvironment)((_a = process.env.NODE_ENV) !== null && _a !== void 0 ? _a : 'test');
    });
    afterAll(config_1._unsetEnvConf);
    it('successfuly gets user company types with valid data', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({
            data: { success: true, data: responseMock },
        });
        expect(yield (0, index_1.getCompanyTypes)()).toBe(responseMock);
    }));
    it('throws error when request is not successful', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error('error'));
        yield expect((0, index_1.getCompanyTypes)()).rejects.toThrow('error');
    }));
});
describe('user support statuses', () => {
    const responseMock = [{ id: 1, name: 'status 1', color: 'red' }];
    beforeAll(() => {
        var _a;
        (0, config_1.setEnvironment)((_a = process.env.NODE_ENV) !== null && _a !== void 0 ? _a : 'test');
    });
    afterAll(config_1._unsetEnvConf);
    it('successfuly gets user support statuses with valid data', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({
            data: { success: true, data: responseMock },
        });
        expect(yield (0, index_1.getSupportStatuses)()).toBe(responseMock);
    }));
    it('throws error when request is not successful', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error('error'));
        yield expect((0, index_1.getSupportStatuses)()).rejects.toThrow('error');
    }));
});
describe('check email', () => {
    beforeAll(() => {
        var _a;
        (0, config_1.setEnvironment)((_a = process.env.NODE_ENV) !== null && _a !== void 0 ? _a : 'test');
    });
    afterAll(config_1._unsetEnvConf);
    it('successfuly gets user support statuses with valid data', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({
            data: { success: true },
        });
        expect(yield (0, index_1.checkEmail)('valid@email.true')).toBe(true);
    }));
    it('should throw InvalidEmailError correctly', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce({
            response: {
                data: {
                    success: false,
                    error: { code: 201 },
                },
            },
        });
        yield expect((0, index_1.checkEmail)('invalid email address')).rejects.toThrow(types_1.InvalidEmailError);
    }));
    it('should throw EmailAlreadyExistsError correctly', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce({
            response: {
                data: {
                    success: false,
                    error: { code: 401 },
                },
            },
        });
        yield expect((0, index_1.checkEmail)('email@already.exists')).rejects.toThrow(types_1.EmailAlreadyExistsError);
    }));
    it('throws error when request is not successful', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error('error'));
        yield expect((0, index_1.checkEmail)('request error')).rejects.toThrow('error');
    }));
});
describe('check document', () => {
    beforeAll(() => {
        var _a;
        (0, config_1.setEnvironment)((_a = process.env.NODE_ENV) !== null && _a !== void 0 ? _a : 'test');
    });
    afterAll(config_1._unsetEnvConf);
    it('successfuly gets user support statuses with valid data', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({
            data: { success: true },
        });
        expect(yield (0, index_1.checkDocument)('01234567899')).toBe(true);
    }));
    it('should throw DocumentAlreadyExistsError correctly', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce({
            response: {
                data: {
                    success: false,
                    error: { code: 408 },
                },
            },
        });
        yield expect((0, index_1.checkDocument)('11111111111')).rejects.toThrow(types_1.DocumentAlreadyExistsError);
    }));
    it('throws error when request is not successful', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error('error'));
        yield expect((0, index_1.checkDocument)('request error')).rejects.toThrow('error');
    }));
});
describe('verification code', () => {
    beforeAll(() => {
        var _a;
        (0, config_1.setEnvironment)((_a = process.env.NODE_ENV) !== null && _a !== void 0 ? _a : 'test');
    });
    afterAll(config_1._unsetEnvConf);
    it('should let create a phone validation code', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'post').mockResolvedValueOnce({
            data: { success: true },
        });
        expect(yield (0, index_1.createPhoneValidationCode)('jest', '11999995555')).toBe(true);
    }));
    it('should let create a e-mail validation code', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'post').mockResolvedValueOnce({
            data: { success: true },
        });
        expect(yield (0, index_1.createEmailValidationCode)('jest', 'jest@jesting.mock')).toBe(true);
    }));
    it('should throw error when request is not successfull', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'post').mockRejectedValueOnce(new Error('oops'));
        yield expect((0, index_1.createEmailValidationCode)('', '')).rejects.toThrow('oops');
    }));
    it('should return true when successfully validating code', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'post').mockResolvedValueOnce({
            data: { success: true },
        });
        expect(yield (0, index_1.validateCode)('123456', types_1.ValidationTypes.EMAIL)).toBe(true);
    }));
    it('should throw CodeInvalidDateError correctly', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'post').mockRejectedValueOnce({
            response: {
                data: {
                    success: false,
                    error: { code: 1102 },
                },
            },
        });
        yield expect((0, index_1.validateCode)('123456', types_1.ValidationTypes.EMAIL)).rejects.toThrow(types_1.CodeInvalidDateError);
    }));
    it('should throw CodeExpiredError correctly', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'post').mockRejectedValueOnce({
            response: {
                data: {
                    success: false,
                    error: { code: 1101 },
                },
            },
        });
        yield expect((0, index_1.validateCode)('123456', types_1.ValidationTypes.EMAIL)).rejects.toThrow(types_1.CodeExpiredError);
    }));
    it('should throw CodeInvalidError correctly', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'post').mockRejectedValueOnce({
            response: {
                data: {
                    success: false,
                    error: { code: 1100 },
                },
            },
        });
        yield expect((0, index_1.validateCode)('123456', types_1.ValidationTypes.EMAIL)).rejects.toThrow(types_1.CodeInvalidError);
    }));
});
