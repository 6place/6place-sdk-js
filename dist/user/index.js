"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.changePassword = exports.validateCode = exports.createPhoneValidationCode = exports.createEmailValidationCode = exports.recoverPassword = exports.checkDocument = exports.checkEmail = exports.getSupportStatuses = exports.getCompanyTypes = exports.getOrigins = exports.getDetails = exports.updateUser = exports.createUser = void 0;
/**
 * Manages user related methods
 * @module
 */
const axios_1 = __importDefault(require("axios"));
const config_1 = require("../config");
const types_1 = require("./types");
var _bearer = '';
(0, config_1.getBearerAuth)().then(token => { _bearer = token; });
const _getRequestConfig = () => ({
    withCredentials: true,
    headers: {
        authorization: `Bearer ${_bearer}`,
    },
});
/**
 * Create new user with given data
 * @param user User Object of type [[CreateUserType]]
 * @returns A promise with a [[User]] if request was successful
 * @throws Error when the request is not successful
 */
const createUser = (user) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b;
    try {
        const { data } = yield axios_1.default.post(`${(_a = (0, config_1._getEnvConf)()) === null || _a === void 0 ? void 0 : _a.endpoints.api}/v1/user/`, user, _getRequestConfig());
        return data.data;
    }
    catch (e) {
        switch ((_b = e.response) === null || _b === void 0 ? void 0 : _b.data.error.code) {
            case 201:
                throw new types_1.InvalidEmailError(e.response.data);
            case 401:
                throw new types_1.EmailAlreadyExistsError(e.response.data);
            case 402:
                throw new types_1.UserInvalidNameError(e.response.data);
            case 404:
                throw new types_1.UserInvalidTypeError(e.response.data);
            case 408:
                throw new types_1.DocumentAlreadyExistsError(e.response.data);
            case 411:
                throw new types_1.PhoneAlreadyExistsError(e.response.data);
            case 412:
                throw new types_1.InvalidCompanyError(e.response.data);
            case 1300:
                throw new types_1.CnpjaError(e.response.data);
            default:
                throw new Error(e.message);
        }
    }
});
exports.createUser = createUser;
/**
 * Update current user with given data
 * @param user User Object of type [[UpdateUserType]]
 * @returns A promise with a [[User]] if request was successful
 * @throws Error when the request is not successful
 */
const updateUser = (user) => __awaiter(void 0, void 0, void 0, function* () {
    var _c, _d;
    try {
        const { data } = yield axios_1.default.put(`${(_c = (0, config_1._getEnvConf)()) === null || _c === void 0 ? void 0 : _c.endpoints.api}/v1/user/`, user, _getRequestConfig());
        return data.data;
    }
    catch (e) {
        switch ((_d = e.response) === null || _d === void 0 ? void 0 : _d.data.error.code) {
            case 201:
                throw new types_1.InvalidEmailError(e.response.data);
            case 401:
                throw new types_1.EmailAlreadyExistsError(e.response.data);
            case 402:
                throw new types_1.UserInvalidNameError(e.response.data);
            case 404:
                throw new types_1.UserInvalidTypeError(e.response.data);
            case 408:
                throw new types_1.DocumentAlreadyExistsError(e.response.data);
            case 411:
                throw new types_1.PhoneAlreadyExistsError(e.response.data);
            case 412:
                throw new types_1.InvalidCompanyError(e.response.data);
            case 1300:
                throw new types_1.CnpjaError(e.response.data);
            default:
                throw new Error(e.message);
        }
    }
});
exports.updateUser = updateUser;
/**
 * Get a user details data
 * @returns A promise with a [[User]] if request was successful
 * @throws Error when the request is not successful
 */
const getDetails = () => __awaiter(void 0, void 0, void 0, function* () {
    var _e;
    const { data } = yield axios_1.default.get(`${(_e = (0, config_1._getEnvConf)()) === null || _e === void 0 ? void 0 : _e.endpoints.api}/v1/user/details`, _getRequestConfig());
    return data.data;
});
exports.getDetails = getDetails;
/**
 * Get user origins
 * @returns A promise with an array of objects with id and name if request was successful
 * @throws Error when the request is not successful
 */
const getOrigins = () => __awaiter(void 0, void 0, void 0, function* () {
    var _f;
    const { data } = yield axios_1.default.get(`${(_f = (0, config_1._getEnvConf)()) === null || _f === void 0 ? void 0 : _f.endpoints.api}/v1/user/origin`, _getRequestConfig());
    return data.data;
});
exports.getOrigins = getOrigins;
/**
 * Get user company types
 * @returns A promise with an array of objects with id and name
 * @throws Error when the request is not successful
 */
const getCompanyTypes = () => __awaiter(void 0, void 0, void 0, function* () {
    var _g;
    const { data } = yield axios_1.default.get(`${(_g = (0, config_1._getEnvConf)()) === null || _g === void 0 ? void 0 : _g.endpoints.api}/v1/user/company`, _getRequestConfig());
    return data.data;
});
exports.getCompanyTypes = getCompanyTypes;
/**
 * Get user support statuses
 * @returns A promise with an array of objects with id, name and color
 * @throws Error when the request is not successful
 */
const getSupportStatuses = () => __awaiter(void 0, void 0, void 0, function* () {
    var _h;
    const { data } = yield axios_1.default.get(`${(_h = (0, config_1._getEnvConf)()) === null || _h === void 0 ? void 0 : _h.endpoints.api}/v1/user/status`, _getRequestConfig());
    return data.data;
});
exports.getSupportStatuses = getSupportStatuses;
/**
 * Check if an e-mail is valid
 * @param email email address to check against database
 * @param userId optional id of the user to verify against record found
 * @returns boolean true if e-mail is valid
 * @throws Error, [[InvalidEmailError]], [[EmailAlreadyExistsError]]
 */
const checkEmail = (email, userId) => __awaiter(void 0, void 0, void 0, function* () {
    var _j, _k;
    const params = { email, id: userId };
    try {
        yield axios_1.default.get(`${(_j = (0, config_1._getEnvConf)()) === null || _j === void 0 ? void 0 : _j.endpoints.api}/v1/user/email`, Object.assign({ params }, _getRequestConfig()));
        return true;
    }
    catch (e) {
        switch ((_k = e.response) === null || _k === void 0 ? void 0 : _k.data.error.code) {
            case 201:
                throw new types_1.InvalidEmailError(e.response.data);
            case 401:
                throw new types_1.EmailAlreadyExistsError(e.response.data);
            default:
                throw new Error(e.message);
        }
    }
});
exports.checkEmail = checkEmail;
/**
 * Check if a document is valid
 * @param document email address to check against database
 * @param userId optional id of the user to verify against record found
 * @returns boolean true if e-mail is valid
 * @throws Error, [[InvalidEmailError]], [[EmailAlreadyExistsError]]
 */
const checkDocument = (email, userId) => __awaiter(void 0, void 0, void 0, function* () {
    var _l, _m, _o, _p;
    const params = { email, id: userId };
    try {
        yield axios_1.default.get(`${(_l = (0, config_1._getEnvConf)()) === null || _l === void 0 ? void 0 : _l.endpoints.api}/v1/user/document`, Object.assign({ params }, _getRequestConfig()));
        return true;
    }
    catch (e) {
        switch ((_p = (_o = (_m = e.response) === null || _m === void 0 ? void 0 : _m.data) === null || _o === void 0 ? void 0 : _o.error) === null || _p === void 0 ? void 0 : _p.code) {
            case 408:
                throw new types_1.DocumentAlreadyExistsError(e.response.data);
            default:
                throw new Error(e.message);
        }
    }
});
exports.checkDocument = checkDocument;
/**
 * Send password recovery e-mail to specified user
 * @param email e-mail used to login
 * @returns A promise with a boolean true if request was successful
 * @throws Error when the request is not successful
 */
const recoverPassword = (email) => __awaiter(void 0, void 0, void 0, function* () {
    var _q;
    const params = { email };
    yield axios_1.default.post(`${(_q = (0, config_1._getEnvConf)()) === null || _q === void 0 ? void 0 : _q.endpoints.api}/v1/user/recover`, params, _getRequestConfig());
    return true;
});
exports.recoverPassword = recoverPassword;
const _sendValidationCode = (name, email = null, phone = null, type) => __awaiter(void 0, void 0, void 0, function* () {
    var _r;
    const params = { name, email, phone, type };
    const response = yield axios_1.default.post(`${(_r = (0, config_1._getEnvConf)()) === null || _r === void 0 ? void 0 : _r.endpoints.api}/v1/user/code`, params, _getRequestConfig());
    return response.data.success;
});
/**
 * Send e-mail validation message to registering user
 * @param name Name of the user
 * @param email e-mail used to login, which will receive the validation code
 * @returns A promise with a boolean true if request was successful
 * @throws Error when the request is not successful
 */
const createEmailValidationCode = (name, email) => __awaiter(void 0, void 0, void 0, function* () {
    return yield _sendValidationCode(name, email, null, types_1.ValidationTypes.EMAIL);
});
exports.createEmailValidationCode = createEmailValidationCode;
/**
 * Send phone validation message to registering user
 * @param name Name of the user
 * @param phone phone used to login, which will receive the validation code
 * @returns A promise with a boolean true if request was successful
 * @throws Error when the request is not successful
 */
const createPhoneValidationCode = (name, phone) => __awaiter(void 0, void 0, void 0, function* () {
    return yield _sendValidationCode(name, null, phone, types_1.ValidationTypes.PHONE);
});
exports.createPhoneValidationCode = createPhoneValidationCode;
/**
 * Validates the verification code
 * @param code code to validate
 * @param type type of code that is going to be validated
 * @returns A promise with a boolean true if request was successful
 * @throws Error, [[CodeInvalidError]], [[CodeExpiredError]], [[CodeInvalidDateError]]
 */
const validateCode = (code, type) => __awaiter(void 0, void 0, void 0, function* () {
    var _s, _t, _u, _v;
    try {
        const params = { code, type };
        const response = yield axios_1.default.post(`${(_s = (0, config_1._getEnvConf)()) === null || _s === void 0 ? void 0 : _s.endpoints.api}/v1/user/code/validate`, params, _getRequestConfig());
        return response.data.success;
    }
    catch (e) {
        switch ((_v = (_u = (_t = e.response) === null || _t === void 0 ? void 0 : _t.data) === null || _u === void 0 ? void 0 : _u.error) === null || _v === void 0 ? void 0 : _v.code) {
            case 1100:
                throw new types_1.CodeInvalidError(e.response.data);
            case 1101:
                throw new types_1.CodeExpiredError(e.response.data);
            case 1102:
                throw new types_1.CodeInvalidDateError(e.response.data);
            default:
                throw new Error(e.message);
        }
    }
});
exports.validateCode = validateCode;
/**
 * Update password of specified user
 * @param oldPassword oldpassword of user
 * @param newPassword oldpassword of user
 * @returns A promise with a boolean true if request was successful
 * @throws Error when the request is not successful
 */
const changePassword = (userId, oldPassword, newPassword) => __awaiter(void 0, void 0, void 0, function* () {
    var _w;
    const params = { userId, oldPassword, password: newPassword };
    const { data } = yield axios_1.default.put(`${(_w = (0, config_1._getEnvConf)()) === null || _w === void 0 ? void 0 : _w.endpoints.api}/v1/user/password`, params, _getRequestConfig());
    if (data instanceof Error) {
        throw data;
    }
    return true;
});
exports.changePassword = changePassword;
