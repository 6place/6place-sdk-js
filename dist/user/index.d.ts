import { CreateUserType, DetailsResponse, UpdateUserType, User, ValidationTypes } from './types';
/**
 * Create new user with given data
 * @param user User Object of type [[CreateUserType]]
 * @returns A promise with a [[User]] if request was successful
 * @throws Error when the request is not successful
 */
export declare const createUser: (user: CreateUserType) => Promise<User>;
/**
 * Update current user with given data
 * @param user User Object of type [[UpdateUserType]]
 * @returns A promise with a [[User]] if request was successful
 * @throws Error when the request is not successful
 */
export declare const updateUser: (user: UpdateUserType) => Promise<User>;
/**
 * Get a user details data
 * @returns A promise with a [[User]] if request was successful
 * @throws Error when the request is not successful
 */
export declare const getDetails: () => Promise<DetailsResponse>;
/**
 * Get user origins
 * @returns A promise with an array of objects with id and name if request was successful
 * @throws Error when the request is not successful
 */
export declare const getOrigins: () => Promise<{
    id: number;
    name: string;
}>;
/**
 * Get user company types
 * @returns A promise with an array of objects with id and name
 * @throws Error when the request is not successful
 */
export declare const getCompanyTypes: () => Promise<{
    id: number;
    name: string;
}>;
/**
 * Get user support statuses
 * @returns A promise with an array of objects with id, name and color
 * @throws Error when the request is not successful
 */
export declare const getSupportStatuses: () => Promise<{
    id: number;
    name: string;
    color: string;
}>;
/**
 * Check if an e-mail is valid
 * @param email email address to check against database
 * @param userId optional id of the user to verify against record found
 * @returns boolean true if e-mail is valid
 * @throws Error, [[InvalidEmailError]], [[EmailAlreadyExistsError]]
 */
export declare const checkEmail: (email: string, userId?: number | undefined) => Promise<true>;
/**
 * Check if a document is valid
 * @param document email address to check against database
 * @param userId optional id of the user to verify against record found
 * @returns boolean true if e-mail is valid
 * @throws Error, [[InvalidEmailError]], [[EmailAlreadyExistsError]]
 */
export declare const checkDocument: (email: string, userId?: number | undefined) => Promise<true>;
/**
 * Send password recovery e-mail to specified user
 * @param email e-mail used to login
 * @returns A promise with a boolean true if request was successful
 * @throws Error when the request is not successful
 */
export declare const recoverPassword: (email: string) => Promise<boolean>;
/**
 * Send e-mail validation message to registering user
 * @param name Name of the user
 * @param email e-mail used to login, which will receive the validation code
 * @returns A promise with a boolean true if request was successful
 * @throws Error when the request is not successful
 */
export declare const createEmailValidationCode: (name: string, email: string) => Promise<boolean>;
/**
 * Send phone validation message to registering user
 * @param name Name of the user
 * @param phone phone used to login, which will receive the validation code
 * @returns A promise with a boolean true if request was successful
 * @throws Error when the request is not successful
 */
export declare const createPhoneValidationCode: (name: string, phone: string) => Promise<boolean>;
/**
 * Validates the verification code
 * @param code code to validate
 * @param type type of code that is going to be validated
 * @returns A promise with a boolean true if request was successful
 * @throws Error, [[CodeInvalidError]], [[CodeExpiredError]], [[CodeInvalidDateError]]
 */
export declare const validateCode: (code: string, type: ValidationTypes) => Promise<boolean>;
/**
 * Update password of specified user
 * @param oldPassword oldpassword of user
 * @param newPassword oldpassword of user
 * @returns A promise with a boolean true if request was successful
 * @throws Error when the request is not successful
 */
export declare const changePassword: (userId: number, oldPassword: string, newPassword: string) => Promise<Boolean>;
