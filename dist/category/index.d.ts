import { Category } from './types';
/**
 * Get all categories
 * @returns A promise with a categories array if request is successful
 * @throws Error when the request is not successful
 */
export declare const getAllCategories: () => Promise<Category[]>;
/**
 * Get one category by id
 * @param categoryId to find by that id
 * @returns A promise with a highlight if request was successful
 * @throws Error when the request is not successful
 */
export declare const getCategory: (categoryId: number) => Promise<Category>;
