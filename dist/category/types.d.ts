export declare type Category = {
    id: number;
    name: string;
    imageUrl: string;
    position: number;
    segmentKey: string;
    deletedAt: string;
    createdAt: string;
    updatedAt: string;
};
