/**
 * VendorSettings object interface
 */
export declare type VendorSettings = {
    success: boolean;
    data?: {
        name: string;
        theme: {
            primaryRGB: Array<number>;
        };
        assetsPath: string;
        cnpj: string;
        email: string;
        phone: string;
        logoUrl: string;
        website: string;
        socialMedia: {
            facebook: string;
            instagram: string;
            linkedin: string;
            youtube: string;
        };
        rights: string;
    };
};
/**
 * Gets configuration settings for specific vendor
 * @param vendor string do id do vendor/marketplace
 * @returns Promise with object of type [[VendorSettings]]
 * @throws Error when the request is not successful
 */
export declare const getVendorSettings: (vendor: string) => Promise<VendorSettings>;
