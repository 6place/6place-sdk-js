"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getVendorSettings = void 0;
/**
 * Manages user related methods
 * @module
 */
const axios_1 = __importDefault(require("axios"));
const config_1 = require("../config");
var _bearer = '';
(0, config_1.getBearerAuth)().then(token => { _bearer = token; });
const _getRequestConfig = () => ({
    withCredentials: true,
    headers: {
        authorization: `Bearer ${_bearer}`,
    },
});
/**
 * Gets configuration settings for specific vendor
 * @param vendor string do id do vendor/marketplace
 * @returns Promise with object of type [[VendorSettings]]
 * @throws Error when the request is not successful
 */
const getVendorSettings = (vendor) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    const params = { vendor };
    const response = (yield axios_1.default.get(`${(_a = (0, config_1._getEnvConf)()) === null || _a === void 0 ? void 0 : _a.endpoints.api}/v1/setting/vendor`, Object.assign({ params }, _getRequestConfig())));
    return response.data;
});
exports.getVendorSettings = getVendorSettings;
