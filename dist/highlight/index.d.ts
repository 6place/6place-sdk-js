import { Hightlight } from './types';
/**
 * Get all highlights
 * @returns A promise with a highlights array if request is successful
 * @throws Error when the request is not successful
 */
export declare const getAllHighlights: () => Promise<Hightlight[]>;
/**
 * Get one highlight by id
 * @param highlightId to find by that id
 * @returns A promise with a highlight if request was successful
 * @throws Error when the request is not successful
 */
export declare const getHighlight: (highlightId: string) => Promise<Hightlight>;
