import { CreateEvaluation, Product, ProductEvaluation, ProductResponse, ProductUnit } from './types';
/**
 * Get list of products with or without details, with optinal filters
 * @param search search query to filter
 * @param providerId ID of the provider to search products from
 * @param isPromotion set true to search only for products with promotion
 * @param isDetail set to true to fetch exta details of the product
 * @param lat latitude of lng to search by location
 * @param lng longitude of the location to search nearest providers
 * @param minValue search for values <= this param
 * @param page number of page to fetch
 * @param limit limit of products to fetch
 * @returns Promise with [[ProductResponse]] with an array of objects of type [[Product]]
 * @throws IBGEError when the request is not successful
 */
export declare const getProducts: (lat?: number | undefined, lng?: number | undefined, search?: string | undefined, providerId?: number | undefined, isPromotion?: boolean | undefined, isDetail?: boolean | undefined, categoryId?: number | undefined, page?: number | undefined, limit?: number | undefined, minValue?: number | undefined) => Promise<ProductResponse>;
/**
 * Get product details by id
 * @param id of the product to retrieve
 * @returns A promise with a product array if request is successful
 * @throws Error when the request is not successful
 */
export declare const getProduct: (id: number) => Promise<Product>;
/**
 * Get list of product units
 * @returns A promise with a [[ProductUnit]] array if request is successful
 * @throws Error when the request is not successful
 */
export declare const getMeasureUnits: () => Promise<ProductUnit[]>;
/**
 * Get total number of products for the current user, in the app or in a given area
 * @param latitude latitude you want to filter products
 * @param longitude longitude you want info about
 * @returns A promise with the number of products if request is successful
 * @throws Error when the request is not successful
 */
export declare const getCount: (latitude?: number | undefined, longitude?: number | undefined) => Promise<number>;
/**
 * Get all product evaluations from given product ID
 * @param id of the product to retrieve evaluations from
 * @returns A promise with a [[ProductEvaluation]] array if request is successful
 * @throws Error when the request is not successful
 */
export declare const getEvaluations: (id: number) => Promise<ProductEvaluation[]>;
/**
 * Get all product evaluations from given product ID
 * @param id of the product to add the evaluation to
 * @param params of the product evaluation
 * @returns A promise with a [[ProductEvaluation]] array if request is successful
 * @throws Error when the request is not successful
 */
export declare const createEvaluation: (id: number, params: CreateEvaluation) => Promise<ProductEvaluation>;
