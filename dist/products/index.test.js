"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const ts_auto_mock_1 = require("ts-auto-mock");
const config_1 = require("../config");
const _1 = require("./");
describe('getting products', () => {
    beforeAll(() => { var _a; return (0, config_1.setEnvironment)((_a = process.env.NODE_ENV) !== null && _a !== void 0 ? _a : 'test'); });
    afterAll(config_1._unsetEnvConf);
    it('should get products successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
        const expected = (0, ts_auto_mock_1.createMock)();
        jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
        const result = yield (0, _1.getProducts)();
        expect(typeof result).toBe('object');
        expect(result).toBe(expected);
    }));
    it('throws error for unfulfilled request', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error('unknown error'));
        yield expect((0, _1.getProducts)()).rejects.toThrow('unknown error');
    }));
    it('should get single product successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
        const expected = (0, ts_auto_mock_1.createMock)();
        jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
        const result = yield (0, _1.getProduct)(123);
        expect(typeof result).toBe('object');
        expect(result).toBe(expected);
    }));
    it('throws error for unfulfilled request', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error('unknown error'));
        yield expect((0, _1.getProduct)(444)).rejects.toThrow('unknown error');
    }));
    it('should count products successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
        const expected = 123;
        jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
        const result = yield (0, _1.getCount)();
        expect(typeof result).toBe('number');
        expect(result).toBe(expected);
    }));
    it('throws error for unfulfilled request', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error('unknown error'));
        yield expect((0, _1.getCount)()).rejects.toThrow('unknown error');
    }));
});
describe('getting product units', () => {
    beforeAll(() => { var _a; return (0, config_1.setEnvironment)((_a = process.env.NODE_ENV) !== null && _a !== void 0 ? _a : 'test'); });
    afterAll(config_1._unsetEnvConf);
    it('should get product units successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
        const expected = (0, ts_auto_mock_1.createMock)();
        jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({ data: { success: true, data: [expected] } });
        const result = yield (0, _1.getMeasureUnits)();
        expect(typeof result).toBe('object');
        expect(result).toStrictEqual([expected]);
    }));
    it('throws error for unfulfilled request', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error('unknown error'));
        yield expect((0, _1.getMeasureUnits)()).rejects.toThrow('unknown error');
    }));
    describe('product evaluations', () => {
        beforeAll(() => { var _a; return (0, config_1.setEnvironment)((_a = process.env.NODE_ENV) !== null && _a !== void 0 ? _a : 'test'); });
        afterAll(config_1._unsetEnvConf);
        it('should get product evaluations successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
            const expected = (0, ts_auto_mock_1.createMock)();
            jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({ data: { success: true, data: [expected] } });
            const result = yield (0, _1.getEvaluations)(123);
            expect(typeof result).toBe('object');
            expect(result).toStrictEqual([expected]);
        }));
        it('throws error for unfulfilled request', () => __awaiter(void 0, void 0, void 0, function* () {
            jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error('unknown error'));
            yield expect((0, _1.getEvaluations)(555)).rejects.toThrow('unknown error');
        }));
        it('should create product evaluation successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
            const params = (0, ts_auto_mock_1.createMock)();
            const expected = (0, ts_auto_mock_1.createMock)({
                score: params.score,
                comment: params.comment,
            });
            jest.spyOn(axios_1.default, 'post').mockResolvedValueOnce({ data: { success: true, data: expected } });
            const result = yield (0, _1.createEvaluation)(123, params);
            expect(typeof result).toBe('object');
            expect(result).toStrictEqual(expected);
        }));
        it('throws error for unfulfilled request', () => __awaiter(void 0, void 0, void 0, function* () {
            const params = (0, ts_auto_mock_1.createMock)();
            jest.spyOn(axios_1.default, 'post').mockRejectedValueOnce(new Error('unknown error'));
            yield expect((0, _1.createEvaluation)(555, params)).rejects.toThrow('unknown error');
        }));
    });
});
