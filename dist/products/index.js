"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createEvaluation = exports.getEvaluations = exports.getCount = exports.getMeasureUnits = exports.getProduct = exports.getProducts = void 0;
const axios_1 = __importDefault(require("axios"));
const config_1 = require("../config");
var _bearer = '';
(0, config_1.getBearerAuth)().then(token => { _bearer = token; });
const _getRequestConfig = () => ({
    withCredentials: true,
    headers: {
        authorization: `Bearer ${_bearer}`,
    },
});
/**
 * Get list of products with or without details, with optinal filters
 * @param search search query to filter
 * @param providerId ID of the provider to search products from
 * @param isPromotion set true to search only for products with promotion
 * @param isDetail set to true to fetch exta details of the product
 * @param lat latitude of lng to search by location
 * @param lng longitude of the location to search nearest providers
 * @param minValue search for values <= this param
 * @param page number of page to fetch
 * @param limit limit of products to fetch
 * @returns Promise with [[ProductResponse]] with an array of objects of type [[Product]]
 * @throws IBGEError when the request is not successful
 */
const getProducts = (lat, lng, search, providerId, isPromotion, isDetail, categoryId, page, limit, minValue) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b;
    const params = {
        lat,
        lng,
        search,
        providerId,
        isPromotion,
        isDetail,
        page,
        limit,
        minValue,
        categoryId
    };
    const response = yield axios_1.default.get(`${(_a = (0, config_1._getEnvConf)()) === null || _a === void 0 ? void 0 : _a.endpoints.api}/v3/product`, Object.assign({ params }, _getRequestConfig()));
    return (((_b = response.data) === null || _b === void 0 ? void 0 : _b.data) || []);
});
exports.getProducts = getProducts;
/**
 * Get product details by id
 * @param id of the product to retrieve
 * @returns A promise with a product array if request is successful
 * @throws Error when the request is not successful
 */
const getProduct = (id) => __awaiter(void 0, void 0, void 0, function* () {
    var _c;
    const { data } = yield axios_1.default.get(`${(_c = (0, config_1._getEnvConf)()) === null || _c === void 0 ? void 0 : _c.endpoints.api}/v1/product/${id}`, _getRequestConfig());
    return data.data;
});
exports.getProduct = getProduct;
/**
 * Get list of product units
 * @returns A promise with a [[ProductUnit]] array if request is successful
 * @throws Error when the request is not successful
 */
const getMeasureUnits = () => __awaiter(void 0, void 0, void 0, function* () {
    var _d;
    const { data } = yield axios_1.default.get(`${(_d = (0, config_1._getEnvConf)()) === null || _d === void 0 ? void 0 : _d.endpoints.api}/v1/product/unit`, _getRequestConfig());
    return data.data;
});
exports.getMeasureUnits = getMeasureUnits;
/**
 * Get total number of products for the current user, in the app or in a given area
 * @param latitude latitude you want to filter products
 * @param longitude longitude you want info about
 * @returns A promise with the number of products if request is successful
 * @throws Error when the request is not successful
 */
const getCount = (latitude, longitude) => __awaiter(void 0, void 0, void 0, function* () {
    var _e;
    const params = { lat: latitude, lng: longitude };
    const { data } = yield axios_1.default.get(`${(_e = (0, config_1._getEnvConf)()) === null || _e === void 0 ? void 0 : _e.endpoints.api}/v1/product/count`, Object.assign({ params }, _getRequestConfig()));
    return data.data;
});
exports.getCount = getCount;
/**
 * Get all product evaluations from given product ID
 * @param id of the product to retrieve evaluations from
 * @returns A promise with a [[ProductEvaluation]] array if request is successful
 * @throws Error when the request is not successful
 */
const getEvaluations = (id) => __awaiter(void 0, void 0, void 0, function* () {
    var _f;
    const { data } = yield axios_1.default.get(`${(_f = (0, config_1._getEnvConf)()) === null || _f === void 0 ? void 0 : _f.endpoints.api}/v1/product/${id}/evaluation`, _getRequestConfig());
    return data.data;
});
exports.getEvaluations = getEvaluations;
/**
 * Get all product evaluations from given product ID
 * @param id of the product to add the evaluation to
 * @param params of the product evaluation
 * @returns A promise with a [[ProductEvaluation]] array if request is successful
 * @throws Error when the request is not successful
 */
const createEvaluation = (id, params) => __awaiter(void 0, void 0, void 0, function* () {
    var _g;
    const { data } = yield axios_1.default.post(`${(_g = (0, config_1._getEnvConf)()) === null || _g === void 0 ? void 0 : _g.endpoints.api}/v1/product/${id}/evaluation`, params, _getRequestConfig());
    return data.data;
});
exports.createEvaluation = createEvaluation;
