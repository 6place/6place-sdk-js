"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getChats = exports.getChat = void 0;
/**
 * Manages user related methods
 * @module
 */
const axios_1 = __importDefault(require("axios"));
const config_1 = require("../config");
const types_1 = require("./types");
var _bearer = '';
(0, config_1.getBearerAuth)().then(token => { _bearer = token; });
const _getRequestConfig = () => ({
    withCredentials: true,
    headers: {
        authorization: `Bearer ${_bearer}`,
    },
});
/**
 * Get a single chat data from given ID
 * @param id ID of the chat to be fetched
 * @returns A promise with a [[Chat]] object if request was successful
 * @throws Error or [[ChatNotFoundError]] when request is not successful
 */
const getChat = (id) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b, _c, _d, _e;
    try {
        const response = (yield axios_1.default.get(`${(_a = (0, config_1._getEnvConf)()) === null || _a === void 0 ? void 0 : _a.endpoints.chat}/v1/chat/${id}`, _getRequestConfig()));
        return (_b = response.data) === null || _b === void 0 ? void 0 : _b.data;
    }
    catch (e) {
        switch ((_e = (_d = (_c = e.response) === null || _c === void 0 ? void 0 : _c.data) === null || _d === void 0 ? void 0 : _d.error) === null || _e === void 0 ? void 0 : _e.code) {
            case 1600:
                throw new types_1.ChatNotFoundError(e.response.data);
            default:
                throw new Error(e.message);
        }
    }
});
exports.getChat = getChat;
/**
 * Get all chats from given reference and type
 * @param referenceId ID of the user/provider you want to get messages from
 * @param referenceType ID of the user/provider you want to get messages from
 * @param options object of type [[ChatRequestOptions]] with other options to filter and sort query
 * @returns A promise with an array of [[Chat]] object if request was successful
 * @throws Error or [[ChatNotFoundError]] when request is not successful
 */
const getChats = (referenceId, referenceType, options) => __awaiter(void 0, void 0, void 0, function* () {
    var _f, _g, _h, _j, _k;
    try {
        const params = Object.assign({ referenceId, referenceType }, options);
        const response = (yield axios_1.default.get(`${(_f = (0, config_1._getEnvConf)()) === null || _f === void 0 ? void 0 : _f.endpoints.chat}/v1/chat/`, Object.assign({ params }, _getRequestConfig())));
        return (_g = response.data) === null || _g === void 0 ? void 0 : _g.data;
    }
    catch (e) {
        switch ((_k = (_j = (_h = e.response) === null || _h === void 0 ? void 0 : _h.data) === null || _j === void 0 ? void 0 : _j.error) === null || _k === void 0 ? void 0 : _k.code) {
            default:
                throw new Error(e.message);
        }
    }
});
exports.getChats = getChats;
