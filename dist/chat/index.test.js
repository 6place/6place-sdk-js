"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const ts_auto_mock_1 = require("ts-auto-mock");
const config_1 = require("../config");
const _1 = require("./");
const types_1 = require("./types");
describe('chat', () => {
    beforeAll(() => { var _a; return (0, config_1.setEnvironment)((_a = process.env.NODE_ENV) !== null && _a !== void 0 ? _a : 'test'); });
    afterAll(config_1._unsetEnvConf);
    const mockChat = (0, ts_auto_mock_1.createMock)();
    describe('getting a single chat', () => {
        it('should get a chat by id successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
            const expected = mockChat;
            jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
            const result = yield (0, _1.getChat)(123);
            expect(typeof result).toBe('object');
            expect(result).toBe(expected);
        }));
        it('throws ChatNotFoundError for unfulfilled request', () => __awaiter(void 0, void 0, void 0, function* () {
            jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce({
                response: { data: { success: false, error: { code: 1600 } } },
            });
            yield expect((0, _1.getChat)(123)).rejects.toThrow(types_1.ChatNotFoundError);
        }));
        it('throws default Error for unfulfilled request', () => __awaiter(void 0, void 0, void 0, function* () {
            jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error('jest error'));
            yield expect((0, _1.getChat)(123)).rejects.toThrow('jest error');
        }));
    });
    describe('getting multiple chats', () => {
        it('should get chats successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
            const expected = { count: 1, rows: [mockChat] };
            jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
            const result = yield (0, _1.getChats)(123, types_1.ReferenceTypes.PROVIDER, {});
            expect(typeof result).toBe('object');
            expect(result).toBe(expected);
        }));
        it('throws default Error for unfulfilled request', () => __awaiter(void 0, void 0, void 0, function* () {
            jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error('jest error'));
            yield expect((0, _1.getChats)(123, types_1.ReferenceTypes.USER, {})).rejects.toThrow('jest error');
        }));
    });
    describe('getting chat messages', () => {
        it('should get all messages successfully', () => __awaiter(void 0, void 0, void 0, function* () {
            const expected = { count: 1, rows: [mockChat] };
            jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
            const result = yield (0, _1.getChats)(123, types_1.ReferenceTypes.PROVIDER, {});
            expect(typeof result).toBe('object');
            expect(result).toBe(expected);
        }));
        it('throws default Error for unfulfilled request', () => __awaiter(void 0, void 0, void 0, function* () {
            jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error('jest error'));
            yield expect((0, _1.getChats)(123, types_1.ReferenceTypes.USER, {})).rejects.toThrow('jest error');
        }));
    });
});
