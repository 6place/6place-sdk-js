"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReferenceTypes = exports.ChatNotFoundError = void 0;
const error_1 = require("../_base/types/error");
class ChatNotFoundError extends error_1.CustomError {
}
exports.ChatNotFoundError = ChatNotFoundError;
var ReferenceTypes;
(function (ReferenceTypes) {
    ReferenceTypes["USER"] = "user";
    ReferenceTypes["PROVIDER"] = "provider";
    ReferenceTypes["SUPPORT"] = "support";
})(ReferenceTypes = exports.ReferenceTypes || (exports.ReferenceTypes = {}));
var SortOrder;
(function (SortOrder) {
    SortOrder["ASC"] = "ASC";
    SortOrder["DESC"] = "DESC";
})(SortOrder || (SortOrder = {}));
