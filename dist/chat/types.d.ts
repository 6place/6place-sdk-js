import { CustomError } from '../_base/types/error';
export declare class ChatNotFoundError extends CustomError {
}
export declare enum ReferenceTypes {
    USER = "user",
    PROVIDER = "provider",
    SUPPORT = "support"
}
declare enum SortOrder {
    ASC = "ASC",
    DESC = "DESC"
}
export interface ChatRequestOptions {
    page?: number;
    limit?: number;
    column?: string;
    order?: SortOrder;
}
export declare type Reference = {
    id: number;
    chatId: number;
    referenceId: number;
    referenceType: string;
    notViewedCount: number;
    deletedAt: string;
    createdAt: string;
    updatedAt: string;
    referenceImage: string;
    referenceName: string;
};
export declare type Chat = {
    id: number;
    lastMessage: string;
    lastMessageType: string;
    lastMessageReferenceId: number;
    lastMessageReferenceType: string;
    lastMessageAt: string;
    type: string;
    deletedAt: string;
    createdAt: string;
    updatedAt: string;
    references: Array<Reference>;
};
export {};
