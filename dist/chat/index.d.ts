import { Chat, ChatRequestOptions, ReferenceTypes } from './types';
/**
 * Get a single chat data from given ID
 * @param id ID of the chat to be fetched
 * @returns A promise with a [[Chat]] object if request was successful
 * @throws Error or [[ChatNotFoundError]] when request is not successful
 */
export declare const getChat: (id: number) => Promise<Chat>;
/**
 * Get all chats from given reference and type
 * @param referenceId ID of the user/provider you want to get messages from
 * @param referenceType ID of the user/provider you want to get messages from
 * @param options object of type [[ChatRequestOptions]] with other options to filter and sort query
 * @returns A promise with an array of [[Chat]] object if request was successful
 * @throws Error or [[ChatNotFoundError]] when request is not successful
 */
export declare const getChats: (referenceId: number, referenceType: ReferenceTypes, options: ChatRequestOptions) => Promise<Chat>;
