"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getLocaleByLatLon = exports.getLocaleByZIP = exports.getCities = exports.getStates = void 0;
const axios_1 = __importDefault(require("axios"));
const config_1 = require("../config");
const types_1 = require("./types");
var _bearer = '';
(0, config_1.getBearerAuth)().then(token => { _bearer = token; });
const _getRequestConfig = () => ({
    withCredentials: true,
    headers: {
        authorization: `Bearer ${_bearer}`,
    },
});
/**
 * Get list of brazilian states
 * @returns Promise with array of objects of types [[State]]
 * @throws [[IBGEError]] when the request is not successful
 */
const getStates = () => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b, _c, _d, _e;
    try {
        const response = (yield axios_1.default.get(`${(_a = (0, config_1._getEnvConf)()) === null || _a === void 0 ? void 0 : _a.endpoints.api}/v1/locale/states`, _getRequestConfig()));
        return (((_b = response.data) === null || _b === void 0 ? void 0 : _b.data) || []);
    }
    catch (e) {
        switch ((_e = (_d = (_c = e.response) === null || _c === void 0 ? void 0 : _c.data) === null || _d === void 0 ? void 0 : _d.error) === null || _e === void 0 ? void 0 : _e.code) {
            case 500:
                throw new types_1.IBGEError(e.response.data);
            default:
                throw new Error(e.message);
        }
    }
});
exports.getStates = getStates;
/**
 * Get list of cities for a given state
 * @param state id or initials of state to get the list of cities from
 * @returns Promise with array of objects of type [[City]]
 * @throws [[IBGEError]] when the request is not successful
 */
const getCities = (state) => __awaiter(void 0, void 0, void 0, function* () {
    var _f, _g, _h, _j, _k;
    try {
        const response = (yield axios_1.default.get(`${(_f = (0, config_1._getEnvConf)()) === null || _f === void 0 ? void 0 : _f.endpoints.api}/v1/locale/states/${state}/cities`, _getRequestConfig()));
        return (((_g = response.data) === null || _g === void 0 ? void 0 : _g.data) || []);
    }
    catch (e) {
        switch ((_k = (_j = (_h = e.response) === null || _h === void 0 ? void 0 : _h.data) === null || _j === void 0 ? void 0 : _j.error) === null || _k === void 0 ? void 0 : _k.code) {
            case 500:
                throw new types_1.IBGEError(e.response.data);
            default:
                throw new Error(e.message);
        }
    }
});
exports.getCities = getCities;
/**
 * Get location info for a given ZIP Code and address query
 * @param postalCode zip code for the address you want info about
 * @param search complement of the zip code, address lines
 * @returns Promise with object of type [[Locale]]
 * @throws LocationNotFoundError when the request is not successful
 */
const getLocaleByZIP = (postalCode = '', search = '') => __awaiter(void 0, void 0, void 0, function* () {
    var _l, _m, _o, _p, _q;
    try {
        const params = { postalCode, search, type: types_1._LocaleTypes.ADDRESS };
        const response = (yield axios_1.default.get(`${(_l = (0, config_1._getEnvConf)()) === null || _l === void 0 ? void 0 : _l.endpoints.api}/v1/locale/`, Object.assign({ params }, _getRequestConfig())));
        return (((_m = response.data) === null || _m === void 0 ? void 0 : _m.data) || {});
    }
    catch (e) {
        switch ((_q = (_p = (_o = e.response) === null || _o === void 0 ? void 0 : _o.data) === null || _p === void 0 ? void 0 : _p.error) === null || _q === void 0 ? void 0 : _q.code) {
            case 800:
                throw new types_1.LocationNotFoundError(e.response.data);
            default:
                throw new Error(e.message);
        }
    }
});
exports.getLocaleByZIP = getLocaleByZIP;
/**
 * Get location info for a given latitude and longitude
 * @param latitude latitude you want info about
 * @param longitude longitude you want info about
 * @returns Promise with object of type [[Locale]]
 * @throws LocationNotFoundError when the request is not successful
 */
const getLocaleByLatLon = (latitude, longitude) => __awaiter(void 0, void 0, void 0, function* () {
    var _r, _s, _t, _u, _v;
    try {
        const params = { latitude, longitude, type: types_1._LocaleTypes.GEO };
        const response = (yield axios_1.default.get(`${(_r = (0, config_1._getEnvConf)()) === null || _r === void 0 ? void 0 : _r.endpoints.api}/v1/locale/`, Object.assign({ params }, _getRequestConfig())));
        return (((_s = response.data) === null || _s === void 0 ? void 0 : _s.data) || {});
    }
    catch (e) {
        switch ((_v = (_u = (_t = e.response) === null || _t === void 0 ? void 0 : _t.data) === null || _u === void 0 ? void 0 : _u.error) === null || _v === void 0 ? void 0 : _v.code) {
            case 800:
                throw new types_1.LocationNotFoundError(e.response.data);
            default:
                throw new Error(e.message);
        }
    }
});
exports.getLocaleByLatLon = getLocaleByLatLon;
