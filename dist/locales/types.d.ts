import { CustomError } from '../_base/types/error';
export declare class IBGEError extends CustomError {
}
export declare class LocationNotFoundError extends CustomError {
}
export declare type State = {
    id: number;
    name: string;
    initials: string;
};
export declare type City = {
    id: number;
    name: string;
    stateId: number;
};
export declare type Locale = {
    street: string | false;
    district: string | false;
    city: string | false;
    state: {
        long: string | false;
        short: string | false;
    };
    country: {
        long: string | false;
        short: string | false;
    };
    postalCode: string | false;
    latitude: number | false;
    longitude: number | false;
    formattedAddress: string | false;
};
export declare const _LocaleTypes: Readonly<{
    ADDRESS: string;
    GEO: string;
}>;
