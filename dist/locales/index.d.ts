import { City, Locale, State } from './types';
/**
 * Get list of brazilian states
 * @returns Promise with array of objects of types [[State]]
 * @throws [[IBGEError]] when the request is not successful
 */
export declare const getStates: () => Promise<Array<State>>;
/**
 * Get list of cities for a given state
 * @param state id or initials of state to get the list of cities from
 * @returns Promise with array of objects of type [[City]]
 * @throws [[IBGEError]] when the request is not successful
 */
export declare const getCities: (state: number | string) => Promise<Array<City>>;
/**
 * Get location info for a given ZIP Code and address query
 * @param postalCode zip code for the address you want info about
 * @param search complement of the zip code, address lines
 * @returns Promise with object of type [[Locale]]
 * @throws LocationNotFoundError when the request is not successful
 */
export declare const getLocaleByZIP: (postalCode?: string, search?: string) => Promise<Locale>;
/**
 * Get location info for a given latitude and longitude
 * @param latitude latitude you want info about
 * @param longitude longitude you want info about
 * @returns Promise with object of type [[Locale]]
 * @throws LocationNotFoundError when the request is not successful
 */
export declare const getLocaleByLatLon: (latitude: number, longitude: number) => Promise<Locale>;
