"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports._LocaleTypes = exports.LocationNotFoundError = exports.IBGEError = void 0;
const error_1 = require("../_base/types/error");
class IBGEError extends error_1.CustomError {
}
exports.IBGEError = IBGEError;
class LocationNotFoundError extends error_1.CustomError {
}
exports.LocationNotFoundError = LocationNotFoundError;
exports._LocaleTypes = Object.freeze({
    ADDRESS: 'address',
    GEO: 'geo',
});
