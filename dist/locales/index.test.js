"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const config_1 = require("../config");
const _1 = require("./");
const types_1 = require("./types");
describe('states', () => {
    beforeAll(() => { var _a; return (0, config_1.setEnvironment)((_a = process.env.NODE_ENV) !== null && _a !== void 0 ? _a : 'test'); });
    afterAll(config_1._unsetEnvConf);
    it('should get states successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
        const expected = [{ id: 1, name: 'Estado', initials: 'ES' }];
        jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({
            data: { success: true, data: expected },
        });
        const result = yield (0, _1.getStates)();
        expect(typeof result).toBe('object');
        expect(result).toBe(expected);
    }));
    it('throws error for unfulfilled request', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce({
            response: { data: { success: false, error: { code: 500, message: 'IBGE error' } } },
        });
        yield expect((0, _1.getStates)()).rejects.toThrow(types_1.IBGEError);
    }));
});
describe('cities', () => {
    beforeAll(() => { var _a; return (0, config_1.setEnvironment)((_a = process.env.NODE_ENV) !== null && _a !== void 0 ? _a : 'test'); });
    afterAll(config_1._unsetEnvConf);
    it('should get cities successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
        const expected = [{ id: 1, name: 'Cidade', stateId: 5 }];
        jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({
            data: { success: true, data: expected },
        });
        const result = yield (0, _1.getCities)(5);
        expect(typeof result).toBe('object');
        expect(result).toBe(expected);
    }));
    it('throws error for unfulfilled request', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce({
            response: { data: { success: false, error: { code: 500, message: 'IBGE error' } } },
        });
        yield expect((0, _1.getCities)(555)).rejects.toThrow(types_1.IBGEError);
    }));
});
describe('cities', () => {
    beforeAll(() => { var _a; return (0, config_1.setEnvironment)((_a = process.env.NODE_ENV) !== null && _a !== void 0 ? _a : 'test'); });
    afterAll(config_1._unsetEnvConf);
    it('should get cities successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
        const expected = [{ id: 1, name: 'Cidade', stateId: 5 }];
        jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({
            data: { success: true, data: expected },
        });
        const result = yield (0, _1.getCities)(5);
        expect(typeof result).toBe('object');
        expect(result).toBe(expected);
    }));
    it('throws error for unfulfilled request', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce({
            response: { data: { success: false, error: { code: 500, message: 'IBGE error' } } },
        });
        yield expect((0, _1.getCities)(555)).rejects.toThrow(types_1.IBGEError);
    }));
});
describe('locales', () => {
    beforeAll(() => { var _a; return (0, config_1.setEnvironment)((_a = process.env.NODE_ENV) !== null && _a !== void 0 ? _a : 'test'); });
    afterAll(config_1._unsetEnvConf);
    const expected = {
        street: 'Avenida Marcos Penteado de Ulhôa Rodrigues',
        district: 'Tamboré',
        city: 'Santana de Parnaíba',
        state: { long: 'São Paulo', short: 'SP' },
        country: { long: 'Brasil', short: 'BR' },
        postalCode: '06543-001',
        latitude: -23.4686241,
        longitude: -46.8500218,
        formattedAddress: 'Av. Marcos Penteado de Ulhôa Rodrigues, Santana de Parnaíba - SP, Brasil',
    };
    it('should query address successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({
            data: { success: true, data: expected },
        });
        const result = yield (0, _1.getLocaleByZIP)('06543-001', 'Avenida Marcos Penteado');
        expect(typeof result).toBe('object');
        expect(result).toStrictEqual(expected);
    }));
    it('should query latitude and longitude successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({
            data: { success: true, data: expected },
        });
        const result = yield (0, _1.getLocaleByLatLon)(-23.4686241, -46.8500218);
        expect(typeof result).toBe('object');
        expect(result).toStrictEqual(expected);
    }));
    it('throws error for unfulfilled address request', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce({
            response: { data: { success: false, error: { code: 800 } } },
        });
        yield expect((0, _1.getLocaleByZIP)('')).rejects.toThrow(types_1.LocationNotFoundError);
    }));
    it('throws error for unfulfilled latitude and longitude request', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce({
            response: {
                data: {
                    success: false,
                    error: { code: 800 },
                },
            },
        });
        yield expect((0, _1.getLocaleByLatLon)(NaN, NaN)).rejects.toThrow(types_1.LocationNotFoundError);
    }));
});
