"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const ts_auto_mock_1 = require("ts-auto-mock");
const config_1 = require("../config");
const _1 = require("./");
describe('address', () => {
    beforeAll(() => { var _a; return (0, config_1.setEnvironment)((_a = process.env.NODE_ENV) !== null && _a !== void 0 ? _a : 'test'); });
    afterAll(config_1._unsetEnvConf);
    const mockAddress = (0, ts_auto_mock_1.createMock)();
    const mockAddressParams = (0, ts_auto_mock_1.createMock)();
    it('should get addresses successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
        const expected = [mockAddress];
        jest.spyOn(axios_1.default, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
        const result = yield (0, _1.getAddresses)();
        expect(typeof result).toBe('object');
        expect(result).toBe(expected);
    }));
    it('throws error for unfulfilled request', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'get').mockRejectedValueOnce(new Error('unknown error'));
        yield expect((0, _1.getAddresses)()).rejects.toThrow('unknown error');
    }));
    it('should create address successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
        const expected = [mockAddress];
        jest.spyOn(axios_1.default, 'post').mockResolvedValueOnce({ data: { success: true, data: expected } });
        const result = yield (0, _1.createAddress)(mockAddressParams);
        expect(typeof result).toBe('object');
        expect(result).toBe(expected);
    }));
    it('throws error for unfulfilled request create', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'post').mockRejectedValueOnce(new Error('unknown error'));
        yield expect((0, _1.createAddress)(mockAddressParams)).rejects.toThrow('unknown error');
    }));
    it('should update address successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
        const expected = [mockAddress];
        jest.spyOn(axios_1.default, 'patch').mockResolvedValueOnce({ data: { success: true, data: expected } });
        const result = yield (0, _1.updateAddress)(123, mockAddressParams);
        expect(typeof result).toBe('object');
        expect(result).toBe(expected);
    }));
    it('throws error for unfulfilled request update', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'patch').mockRejectedValueOnce(new Error('unknown error'));
        yield expect((0, _1.updateAddress)(123, mockAddressParams)).rejects.toThrow('unknown error');
    }));
    it('should delete address successfuly', () => __awaiter(void 0, void 0, void 0, function* () {
        const expected = { id: 123 };
        jest.spyOn(axios_1.default, 'delete').mockResolvedValueOnce({ data: { success: true, data: expected } });
        const result = yield (0, _1.deleteAddress)(mockAddress.id);
        expect(typeof result).toBe('object');
        expect(result).toStrictEqual(expected);
    }));
    it('throws error for unfulfilled request delete', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(axios_1.default, 'delete').mockRejectedValueOnce(new Error('unknown error'));
        yield expect((0, _1.deleteAddress)(3)).rejects.toThrow('unknown error');
    }));
});
