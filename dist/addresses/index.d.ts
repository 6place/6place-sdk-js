import { Address, CreateAddressParams } from './types';
/**
 * Get users list of addresses from logged in user
 * @returns Promise with array of objects of types [[Address]]
 * @throws Error when the request is not successful
 */
export declare const getAddresses: () => Promise<Array<Address>>;
/**
 * Create address for current logged in user
 * @param data Object of type [[CreateAddressParams]]
 * @returns Promise with address [[Address]]
 * @throws Error when the request is not successful
 */
export declare const createAddress: (data: CreateAddressParams) => Promise<Address>;
/**
 * Update given ID address of logged in user
 * @param id ID of the address to update
 * @param data Object of type [[CreateAddressParams]]
 * @returns Promise with address [[Address]]
 * @throws Error when the request is not successful
 */
export declare const updateAddress: (id: number, data: CreateAddressParams) => Promise<Address>;
/**
 * Delete logged in user address for the given ID
 * @param id ID of the address to delete
 * @returns id of the deleted registry
 * @throws Error when the request is not successful
 */
export declare const deleteAddress: (id: number) => Promise<number>;
