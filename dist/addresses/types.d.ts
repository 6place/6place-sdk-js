export interface CreateAddressParams {
    name?: string;
    street: string;
    number?: number;
    complement?: string;
    district?: string;
    city: string;
    state: string;
    country: string;
    postalCode: string;
    latitude: number;
    longitude: number;
}
export declare type Address = {
    id: number;
    userId: number;
    name?: string;
    street: string;
    number: string;
    complement?: string;
    district?: string;
    city: string;
    state: string;
    country: string;
    postalCode: string;
    latitude: number;
    longitude: number;
    deletedAt?: string;
    createdAt: string;
    updatedAt: string;
};
