"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteAddress = exports.updateAddress = exports.createAddress = exports.getAddresses = void 0;
const axios_1 = __importDefault(require("axios"));
const config_1 = require("../config");
var _bearer = '';
(0, config_1.getBearerAuth)().then(token => { _bearer = token; });
const _getRequestConfig = () => ({
    withCredentials: true,
    headers: {
        authorization: `Bearer ${_bearer}`,
    },
});
/**
 * Get users list of addresses from logged in user
 * @returns Promise with array of objects of types [[Address]]
 * @throws Error when the request is not successful
 */
const getAddresses = () => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b;
    const response = (yield axios_1.default.get(`${(_a = (0, config_1._getEnvConf)()) === null || _a === void 0 ? void 0 : _a.endpoints.api}/v1/address`, _getRequestConfig()));
    return (((_b = response.data) === null || _b === void 0 ? void 0 : _b.data) || []);
});
exports.getAddresses = getAddresses;
/**
 * Create address for current logged in user
 * @param data Object of type [[CreateAddressParams]]
 * @returns Promise with address [[Address]]
 * @throws Error when the request is not successful
 */
const createAddress = (data) => __awaiter(void 0, void 0, void 0, function* () {
    var _c, _d;
    const response = (yield axios_1.default.post(`${(_c = (0, config_1._getEnvConf)()) === null || _c === void 0 ? void 0 : _c.endpoints.api}/v1/address`, data, _getRequestConfig()));
    return (((_d = response.data) === null || _d === void 0 ? void 0 : _d.data) || []);
});
exports.createAddress = createAddress;
/**
 * Update given ID address of logged in user
 * @param id ID of the address to update
 * @param data Object of type [[CreateAddressParams]]
 * @returns Promise with address [[Address]]
 * @throws Error when the request is not successful
 */
const updateAddress = (id, data) => __awaiter(void 0, void 0, void 0, function* () {
    var _e, _f;
    const response = (yield axios_1.default.patch(`${(_e = (0, config_1._getEnvConf)()) === null || _e === void 0 ? void 0 : _e.endpoints.api}/v1/address/${id}`, data, _getRequestConfig()));
    return (((_f = response.data) === null || _f === void 0 ? void 0 : _f.data) || []);
});
exports.updateAddress = updateAddress;
/**
 * Delete logged in user address for the given ID
 * @param id ID of the address to delete
 * @returns id of the deleted registry
 * @throws Error when the request is not successful
 */
const deleteAddress = (id) => __awaiter(void 0, void 0, void 0, function* () {
    var _g, _h;
    const response = (yield axios_1.default.delete(`${(_g = (0, config_1._getEnvConf)()) === null || _g === void 0 ? void 0 : _g.endpoints.api}/v1/address/${id}`, _getRequestConfig()));
    return (_h = response.data) === null || _h === void 0 ? void 0 : _h.data;
});
exports.deleteAddress = deleteAddress;
