import { Payment } from './types';
/**
 * Get all order by user
 * @returns A promise with a order array if request is successful
 * @throws Error when the request is not successful
 */
export declare const getPayments: () => Promise<Payment[]>;
