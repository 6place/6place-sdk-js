# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.11.13](https://bitbucket.org/6place/6place-sdk-js/compare/v0.11.12...v0.11.13) (2022-01-19)


### Bug Fixes

* added more error codes for create user function ([a3d5925](https://bitbucket.org/6place/6place-sdk-js/commit/a3d5925b011277c3edebd9a90942f74d978ecf20))

### [0.11.12](https://bitbucket.org/6place/6place-sdk-js/compare/v0.11.11...v0.11.12) (2022-01-18)


### Bug Fixes

* removed switch case from thrown exception inside create user function ([85fdf92](https://bitbucket.org/6place/6place-sdk-js/commit/85fdf92cadb1a18b7462b00b5f90adc838d38076))
* test adjustment ([753cce2](https://bitbucket.org/6place/6place-sdk-js/commit/753cce22b76f6c0157579d111568eda1e96293a1))

### [0.11.11](https://bitbucket.org/6place/6place-sdk-js/compare/v0.11.10...v0.11.11) (2022-01-18)


### Features

* included getProducts with categoryId params ([111624a](https://bitbucket.org/6place/6place-sdk-js/commit/111624ab9c6dfbf203f9149e04e37e4aeb3e9c72))

### [0.11.10](https://bitbucket.org/6place/6place-sdk-js/compare/v0.11.9...v0.11.10) (2022-01-17)


### Bug Fixes

* fixed address apis ([3297cff](https://bitbucket.org/6place/6place-sdk-js/commit/3297cffd205cc2b1f94e20f1cb6b582e5d93d33b))

### [0.11.9](https://bitbucket.org/6place/6place-sdk-js/compare/v0.11.8...v0.11.9) (2022-01-14)

### [0.11.8](https://bitbucket.org/6place/6place-sdk-js/compare/v0.11.7...v0.11.8) (2022-01-14)


### Bug Fixes

* vendor settings call adjustment ([25e65f4](https://bitbucket.org/6place/6place-sdk-js/commit/25e65f4ab202fa8bc38620fe41ee35e8d8922bf6))

### [0.11.7](https://bitbucket.org/6place/6place-sdk-js/compare/v0.11.6...v0.11.7) (2022-01-13)


### Features

* included post to create bankslip and pix payment ([cc288f9](https://bitbucket.org/6place/6place-sdk-js/commit/cc288f91a42b68a5c7843b8bf6214183922f852a))
* variable fix, change to orderId ([37e2873](https://bitbucket.org/6place/6place-sdk-js/commit/37e2873b96d51c109cc5120e6756029caf5b9f64))


### Bug Fixes

* Added more infos to VendorSettings type ([4a19424](https://bitbucket.org/6place/6place-sdk-js/commit/4a1942439f241186bd958a5829c1e1c3da2207b3))

### [0.11.6](https://bitbucket.org/6place/6place-sdk-js/compare/v0.11.5...v0.11.6) (2022-01-13)


### Features

* adjustment about change Password variable from newPassword to password ([1891f90](https://bitbucket.org/6place/6place-sdk-js/commit/1891f90be101b3fb327711da77a82ee35a3797b5))

### [0.11.5](https://bitbucket.org/6place/6place-sdk-js/compare/v0.11.4...v0.11.5) (2022-01-11)


### Features

* id adjustment in routes ([9abbdf4](https://bitbucket.org/6place/6place-sdk-js/commit/9abbdf42cf7504010deae6ee95ab2feddc8d7edd))

### [0.11.4](https://bitbucket.org/6place/6place-sdk-js/compare/v0.11.3...v0.11.4) (2022-01-11)

### [0.11.3](https://bitbucket.org/6place/6place-sdk-js/compare/v0.11.2...v0.11.3) (2022-01-11)


### Features

* fix conflict sdk version and docs ([6a01ad1](https://bitbucket.org/6place/6place-sdk-js/commit/6a01ad105b5211cd7174062b7e63d10381dd78f9))

### [0.11.2](https://bitbucket.org/6place/6place-sdk-js/compare/v0.11.1...v0.11.2) (2022-01-10)

### [0.11.1](https://bitbucket.org/6place/6place-sdk-js/compare/v0.11.0...v0.11.1) (2022-01-07)


### Bug Fixes

* unnecessary tag command ([7636647](https://bitbucket.org/6place/6place-sdk-js/commit/7636647787d6781d245baaad48ea1399e59a12f5))

## 0.11.0 (2022-01-07)


### ⚠ BREAKING CHANGES

* all methods of products and providers
* adjusting createUser response
* product detail
* responses interfaces were breaking app

### Features

* add address crud ([8967027](https://bitbucket.org/6place/6place-sdk-js/commit/896702707d1ad15cd241b77c8315f7e51524c290))
* add address delete ([9a63224](https://bitbucket.org/6place/6place-sdk-js/commit/9a632240c34ddb7d9781610b13451f39c429b1bb))
* add address tests ([c1cc873](https://bitbucket.org/6place/6place-sdk-js/commit/c1cc873189fbc7537a9d01d54c105361f4310b97))
* add categories ([cd46d62](https://bitbucket.org/6place/6place-sdk-js/commit/cd46d629f8bd12a6f4a4256c7f441baf6ce05cde))
* add errors on tests" ([8df1169](https://bitbucket.org/6place/6place-sdk-js/commit/8df11698bb0e1dd5336ef5801115d1977fd1fcb8))
* add exports ([5a187b2](https://bitbucket.org/6place/6place-sdk-js/commit/5a187b255842f617672874b617a904081b72c644))
* add get all orders ([ff9b521](https://bitbucket.org/6place/6place-sdk-js/commit/ff9b52175a945a969a759d8f414af10b8d64ef19))
* add hightlights ([d2880aa](https://bitbucket.org/6place/6place-sdk-js/commit/d2880aa8aa8762168eddf237a3ec52e3cf7105d9))
* add order method ([26dec25](https://bitbucket.org/6place/6place-sdk-js/commit/26dec2542afbc24c79d4551572dccba8e2f7daa8))
* add order type and move folders place ([7731ff4](https://bitbucket.org/6place/6place-sdk-js/commit/7731ff4b089b4341e9c4e29ac9e353cfea5e11ca))
* add product details ([8db9713](https://bitbucket.org/6place/6place-sdk-js/commit/8db97138102015c8ac7da5820a1db8d8ae2a7a06))
* add provider methods ([f5aa858](https://bitbucket.org/6place/6place-sdk-js/commit/f5aa8584a792b87d9c853b417d13f733ba32b10f))
* adding all remaining user methods ([bf3fc66](https://bitbucket.org/6place/6place-sdk-js/commit/bf3fc66ffc1524c88850a3e0a61f3e9c7139b0ef))
* all methods of products and providers ([4927a1f](https://bitbucket.org/6place/6place-sdk-js/commit/4927a1f0e70745a93741b5cfc592edfe01475258))
* bumping version and docs ([2a0309a](https://bitbucket.org/6place/6place-sdk-js/commit/2a0309a37f50c26092b03520a13770966c000f6f))
* created skd for cart and faq ([26059d0](https://bitbucket.org/6place/6place-sdk-js/commit/26059d0bda61e747bfb1e14487ec1073282ff1ca))
* creating code validation methods ([99e11c8](https://bitbucket.org/6place/6place-sdk-js/commit/99e11c81d006712b1d99fa67a4283168b0478547))
* creating getProducts method ([2c743b7](https://bitbucket.org/6place/6place-sdk-js/commit/2c743b7619b3bd0c1a4819e67040d00949e22e5f))
* creating method to get all chats ([fc2fb13](https://bitbucket.org/6place/6place-sdk-js/commit/fc2fb135242b72426b9fdf83d52cc5ef21f07394))
* documents upgrade ([bf1633e](https://bitbucket.org/6place/6place-sdk-js/commit/bf1633e0681a6d947529ba4b131fbc996e9dd13b))
* exporting ConfigKeys ([6b21327](https://bitbucket.org/6place/6place-sdk-js/commit/6b21327734684162f349e36a5b5403db8b61ed3c))
* get user details method ([bd4debd](https://bitbucket.org/6place/6place-sdk-js/commit/bd4debdf5441cea415f579711910b32784bee374))
* getChat method ([a8d79f5](https://bitbucket.org/6place/6place-sdk-js/commit/a8d79f5f84365f313641f5e5e965daaa7b8be97e))
* included changepassword in user index sdk ([5f70f06](https://bitbucket.org/6place/6place-sdk-js/commit/5f70f06f1e22ba60ad03598f8c77f11486249776))
* location methods ([3d21cc4](https://bitbucket.org/6place/6place-sdk-js/commit/3d21cc456004385ddf43e0c427927af263936c9a))
* method to get provider orders ([7376aa2](https://bitbucket.org/6place/6place-sdk-js/commit/7376aa28e01909f6f7af7adfca824f6e8ca282b5))
* method to get provider's orders ([787ba17](https://bitbucket.org/6place/6place-sdk-js/commit/787ba17192f329203a4c1c0b6cc058e7c5e90357))
* product detail ([e82278b](https://bitbucket.org/6place/6place-sdk-js/commit/e82278b842cb1df7cd76b3c0cb25e676182ba5e4))
* update user method ([fde6f09](https://bitbucket.org/6place/6place-sdk-js/commit/fde6f090fb20ae9e24bdcb4ccaf8b5d5e2f88d68))


### Bug Fixes

* adding docs to git add and adding cache config ([c386cdb](https://bitbucket.org/6place/6place-sdk-js/commit/c386cdb76494bb6beeb57147bdf65c52018aaf5a))
* adding missing modules to index.ts ([7753ba1](https://bitbucket.org/6place/6place-sdk-js/commit/7753ba189534529e556a73d4f93015374f310423))
* adding prettierignore file ([48dd94c](https://bitbucket.org/6place/6place-sdk-js/commit/48dd94c70e992cf93ffef7127f70f2afadf55838))
* adding yarn install missing line ([0393192](https://bitbucket.org/6place/6place-sdk-js/commit/0393192ac9906664249510322f9ed42c968f938c))
* adding yarn install to release step ([493a620](https://bitbucket.org/6place/6place-sdk-js/commit/493a62041427a209c4fd923d88f08a6e77cfa7b1))
* adjusting createUser response ([c1e37b8](https://bitbucket.org/6place/6place-sdk-js/commit/c1e37b8c115fa8cd2e1d6719a3c30415e992171e))
* auth isLogged method ([897ec7b](https://bitbucket.org/6place/6place-sdk-js/commit/897ec7b82049213b64859a2838cfa28853b72d0d))
* auth tests ([4833607](https://bitbucket.org/6place/6place-sdk-js/commit/48336077e4c4cb029bccd6a75eb3362465180cd4))
* change highlight ([3d2dde6](https://bitbucket.org/6place/6place-sdk-js/commit/3d2dde626e7868b16c3e777a1a42c0e75c16ace1))
* changing back to npm to build package ([d30030e](https://bitbucket.org/6place/6place-sdk-js/commit/d30030ec24f961787256aec2a547c340597150da))
* cloud build now using yarn ([7acf789](https://bitbucket.org/6place/6place-sdk-js/commit/7acf789872cd94041413fe76d8c8ffb5b0f14329))
* commit message ([d375751](https://bitbucket.org/6place/6place-sdk-js/commit/d3757518e4c32142507aa7984002d63b41963f88))
* data returning error ([a20eb59](https://bitbucket.org/6place/6place-sdk-js/commit/a20eb59097a2e83d2048c0924ba3d7a273b3104d))
* eslint path ([a5b3a3f](https://bitbucket.org/6place/6place-sdk-js/commit/a5b3a3f11d1437df08f4b6db3bcb6f553140565c))
* function signature ([42f00c8](https://bitbucket.org/6place/6place-sdk-js/commit/42f00c843a07ceb6d4db5057269bae60e24a6a0e))
* getDetails endpoint was wrong ([3bae056](https://bitbucket.org/6place/6place-sdk-js/commit/3bae05675341ac28ac617399478a54fe47657df2))
* improving test files ([d7b9f4e](https://bitbucket.org/6place/6place-sdk-js/commit/d7b9f4e7c2444d707b9a4a599e63ab78a61ff200))
* isLoggedIn return value ([6ded5da](https://bitbucket.org/6place/6place-sdk-js/commit/6ded5da975d0a811cfa481f455caa3ef36aff394))
* merging deployment pipeline ([afcab42](https://bitbucket.org/6place/6place-sdk-js/commit/afcab42429ae03e10945b2d43286fd13a1fcc372))
* method name ([96fcb69](https://bitbucket.org/6place/6place-sdk-js/commit/96fcb69250db6332246844930c85c65db45cfbe6))
* package version ([3111c35](https://bitbucket.org/6place/6place-sdk-js/commit/3111c35b10a9c6d8b01c2f7e612bc0996873394c))
* parameter type ([3b6f2fd](https://bitbucket.org/6place/6place-sdk-js/commit/3b6f2fd327785b3c588468a3ea3efc0a4150bd58))
* refactoring address methods ([7076e9e](https://bitbucket.org/6place/6place-sdk-js/commit/7076e9eb2f8d101a574a6eb8d6cb2acc5a78a35c))
* removing lint from pipeline ([d296fb3](https://bitbucket.org/6place/6place-sdk-js/commit/d296fb362501ff03c7a62f5425567e4a41c1b06d))
* responses interfaces were breaking app ([d02e086](https://bitbucket.org/6place/6place-sdk-js/commit/d02e086c0b312e88f24e5041e1efebdc52ea0825))
* setEnvironment signature ([06fd3ea](https://bitbucket.org/6place/6place-sdk-js/commit/06fd3eaba2ad87f7e3a39b54704bcdd1489434ce))
* validation method wrong type ([53bc4d1](https://bitbucket.org/6place/6place-sdk-js/commit/53bc4d1f7d1d9adbae990298362b6fe5882323b4))

## 0.10.0 (2022-01-07)


### ⚠ BREAKING CHANGES

* all methods of products and providers
* adjusting createUser response
* product detail
* responses interfaces were breaking app

### Features

* add address crud ([8967027](https://bitbucket.org/6place/6place-sdk-js/commit/896702707d1ad15cd241b77c8315f7e51524c290))
* add address delete ([9a63224](https://bitbucket.org/6place/6place-sdk-js/commit/9a632240c34ddb7d9781610b13451f39c429b1bb))
* add address tests ([c1cc873](https://bitbucket.org/6place/6place-sdk-js/commit/c1cc873189fbc7537a9d01d54c105361f4310b97))
* add categories ([cd46d62](https://bitbucket.org/6place/6place-sdk-js/commit/cd46d629f8bd12a6f4a4256c7f441baf6ce05cde))
* add errors on tests" ([8df1169](https://bitbucket.org/6place/6place-sdk-js/commit/8df11698bb0e1dd5336ef5801115d1977fd1fcb8))
* add exports ([5a187b2](https://bitbucket.org/6place/6place-sdk-js/commit/5a187b255842f617672874b617a904081b72c644))
* add get all orders ([ff9b521](https://bitbucket.org/6place/6place-sdk-js/commit/ff9b52175a945a969a759d8f414af10b8d64ef19))
* add hightlights ([d2880aa](https://bitbucket.org/6place/6place-sdk-js/commit/d2880aa8aa8762168eddf237a3ec52e3cf7105d9))
* add order method ([26dec25](https://bitbucket.org/6place/6place-sdk-js/commit/26dec2542afbc24c79d4551572dccba8e2f7daa8))
* add order type and move folders place ([7731ff4](https://bitbucket.org/6place/6place-sdk-js/commit/7731ff4b089b4341e9c4e29ac9e353cfea5e11ca))
* add product details ([8db9713](https://bitbucket.org/6place/6place-sdk-js/commit/8db97138102015c8ac7da5820a1db8d8ae2a7a06))
* add provider methods ([f5aa858](https://bitbucket.org/6place/6place-sdk-js/commit/f5aa8584a792b87d9c853b417d13f733ba32b10f))
* adding all remaining user methods ([bf3fc66](https://bitbucket.org/6place/6place-sdk-js/commit/bf3fc66ffc1524c88850a3e0a61f3e9c7139b0ef))
* all methods of products and providers ([4927a1f](https://bitbucket.org/6place/6place-sdk-js/commit/4927a1f0e70745a93741b5cfc592edfe01475258))
* bumping version and docs ([2a0309a](https://bitbucket.org/6place/6place-sdk-js/commit/2a0309a37f50c26092b03520a13770966c000f6f))
* created skd for cart and faq ([26059d0](https://bitbucket.org/6place/6place-sdk-js/commit/26059d0bda61e747bfb1e14487ec1073282ff1ca))
* creating code validation methods ([99e11c8](https://bitbucket.org/6place/6place-sdk-js/commit/99e11c81d006712b1d99fa67a4283168b0478547))
* creating getProducts method ([2c743b7](https://bitbucket.org/6place/6place-sdk-js/commit/2c743b7619b3bd0c1a4819e67040d00949e22e5f))
* creating method to get all chats ([fc2fb13](https://bitbucket.org/6place/6place-sdk-js/commit/fc2fb135242b72426b9fdf83d52cc5ef21f07394))
* documents upgrade ([bf1633e](https://bitbucket.org/6place/6place-sdk-js/commit/bf1633e0681a6d947529ba4b131fbc996e9dd13b))
* exporting ConfigKeys ([6b21327](https://bitbucket.org/6place/6place-sdk-js/commit/6b21327734684162f349e36a5b5403db8b61ed3c))
* get user details method ([bd4debd](https://bitbucket.org/6place/6place-sdk-js/commit/bd4debdf5441cea415f579711910b32784bee374))
* getChat method ([a8d79f5](https://bitbucket.org/6place/6place-sdk-js/commit/a8d79f5f84365f313641f5e5e965daaa7b8be97e))
* included changepassword in user index sdk ([5f70f06](https://bitbucket.org/6place/6place-sdk-js/commit/5f70f06f1e22ba60ad03598f8c77f11486249776))
* location methods ([3d21cc4](https://bitbucket.org/6place/6place-sdk-js/commit/3d21cc456004385ddf43e0c427927af263936c9a))
* method to get provider orders ([7376aa2](https://bitbucket.org/6place/6place-sdk-js/commit/7376aa28e01909f6f7af7adfca824f6e8ca282b5))
* method to get provider's orders ([787ba17](https://bitbucket.org/6place/6place-sdk-js/commit/787ba17192f329203a4c1c0b6cc058e7c5e90357))
* product detail ([e82278b](https://bitbucket.org/6place/6place-sdk-js/commit/e82278b842cb1df7cd76b3c0cb25e676182ba5e4))
* update user method ([fde6f09](https://bitbucket.org/6place/6place-sdk-js/commit/fde6f090fb20ae9e24bdcb4ccaf8b5d5e2f88d68))


### Bug Fixes

* adding docs to git add and adding cache config ([c386cdb](https://bitbucket.org/6place/6place-sdk-js/commit/c386cdb76494bb6beeb57147bdf65c52018aaf5a))
* adding missing modules to index.ts ([7753ba1](https://bitbucket.org/6place/6place-sdk-js/commit/7753ba189534529e556a73d4f93015374f310423))
* adding prettierignore file ([48dd94c](https://bitbucket.org/6place/6place-sdk-js/commit/48dd94c70e992cf93ffef7127f70f2afadf55838))
* adding yarn install missing line ([0393192](https://bitbucket.org/6place/6place-sdk-js/commit/0393192ac9906664249510322f9ed42c968f938c))
* adding yarn install to release step ([493a620](https://bitbucket.org/6place/6place-sdk-js/commit/493a62041427a209c4fd923d88f08a6e77cfa7b1))
* adjusting createUser response ([c1e37b8](https://bitbucket.org/6place/6place-sdk-js/commit/c1e37b8c115fa8cd2e1d6719a3c30415e992171e))
* auth isLogged method ([897ec7b](https://bitbucket.org/6place/6place-sdk-js/commit/897ec7b82049213b64859a2838cfa28853b72d0d))
* auth tests ([4833607](https://bitbucket.org/6place/6place-sdk-js/commit/48336077e4c4cb029bccd6a75eb3362465180cd4))
* change highlight ([3d2dde6](https://bitbucket.org/6place/6place-sdk-js/commit/3d2dde626e7868b16c3e777a1a42c0e75c16ace1))
* changing back to npm to build package ([d30030e](https://bitbucket.org/6place/6place-sdk-js/commit/d30030ec24f961787256aec2a547c340597150da))
* cloud build now using yarn ([7acf789](https://bitbucket.org/6place/6place-sdk-js/commit/7acf789872cd94041413fe76d8c8ffb5b0f14329))
* commit message ([d375751](https://bitbucket.org/6place/6place-sdk-js/commit/d3757518e4c32142507aa7984002d63b41963f88))
* data returning error ([a20eb59](https://bitbucket.org/6place/6place-sdk-js/commit/a20eb59097a2e83d2048c0924ba3d7a273b3104d))
* eslint path ([a5b3a3f](https://bitbucket.org/6place/6place-sdk-js/commit/a5b3a3f11d1437df08f4b6db3bcb6f553140565c))
* function signature ([42f00c8](https://bitbucket.org/6place/6place-sdk-js/commit/42f00c843a07ceb6d4db5057269bae60e24a6a0e))
* getDetails endpoint was wrong ([3bae056](https://bitbucket.org/6place/6place-sdk-js/commit/3bae05675341ac28ac617399478a54fe47657df2))
* improving test files ([d7b9f4e](https://bitbucket.org/6place/6place-sdk-js/commit/d7b9f4e7c2444d707b9a4a599e63ab78a61ff200))
* isLoggedIn return value ([6ded5da](https://bitbucket.org/6place/6place-sdk-js/commit/6ded5da975d0a811cfa481f455caa3ef36aff394))
* method name ([96fcb69](https://bitbucket.org/6place/6place-sdk-js/commit/96fcb69250db6332246844930c85c65db45cfbe6))
* package version ([3111c35](https://bitbucket.org/6place/6place-sdk-js/commit/3111c35b10a9c6d8b01c2f7e612bc0996873394c))
* parameter type ([3b6f2fd](https://bitbucket.org/6place/6place-sdk-js/commit/3b6f2fd327785b3c588468a3ea3efc0a4150bd58))
* refactoring address methods ([7076e9e](https://bitbucket.org/6place/6place-sdk-js/commit/7076e9eb2f8d101a574a6eb8d6cb2acc5a78a35c))
* removing lint from pipeline ([d296fb3](https://bitbucket.org/6place/6place-sdk-js/commit/d296fb362501ff03c7a62f5425567e4a41c1b06d))
* responses interfaces were breaking app ([d02e086](https://bitbucket.org/6place/6place-sdk-js/commit/d02e086c0b312e88f24e5041e1efebdc52ea0825))
* setEnvironment signature ([06fd3ea](https://bitbucket.org/6place/6place-sdk-js/commit/06fd3eaba2ad87f7e3a39b54704bcdd1489434ce))
* validation method wrong type ([53bc4d1](https://bitbucket.org/6place/6place-sdk-js/commit/53bc4d1f7d1d9adbae990298362b6fe5882323b4))

### [0.9.2](https://bitbucket.org/6place/6place-sdk-js/compare/v0.9.87...v0.9.2) (2022-01-06)


### Features

* update user method ([fde6f09](https://bitbucket.org/6place/6place-sdk-js/commit/fde6f090fb20ae9e24bdcb4ccaf8b5d5e2f88d68))

### [0.9.1](https://bitbucket.org/6place/6place-sdk-js/compare/v0.9.0...v0.9.1) (2022-01-05)


### Features

* adding all remaining user methods ([bf3fc66](https://bitbucket.org/6place/6place-sdk-js/commit/bf3fc66ffc1524c88850a3e0a61f3e9c7139b0ef))

### [0.8.2](https://bitbucket.org/6place/6place-sdk-js/compare/v0.8.1...v0.8.2) (2021-12-15)

## [0.9.0](https://bitbucket.org/6place/6place-sdk-js/compare/v0.8.1...v0.9.0) (2021-12-23)


### ⚠ BREAKING CHANGES

* all methods of products and providers

### Features

* all methods of products and providers ([4927a1f](https://bitbucket.org/6place/6place-sdk-js/commit/4927a1f0e70745a93741b5cfc592edfe01475258))
### [0.8.2](https://bitbucket.org/6place/6place-sdk-js/compare/v0.8.1...v0.8.2) (2021-12-15)


### Bug Fixes

* getDetails endpoint was wrong ([3bae056](https://bitbucket.org/6place/6place-sdk-js/commit/3bae05675341ac28ac617399478a54fe47657df2))

### [0.8.1](https://bitbucket.org/6place/6place-sdk-js/compare/v0.8.0...v0.8.1) (2021-12-14)


### Features

* add address crud ([8967027](https://bitbucket.org/6place/6place-sdk-js/commit/896702707d1ad15cd241b77c8315f7e51524c290))
* add address delete ([9a63224](https://bitbucket.org/6place/6place-sdk-js/commit/9a632240c34ddb7d9781610b13451f39c429b1bb))
* add address tests ([c1cc873](https://bitbucket.org/6place/6place-sdk-js/commit/c1cc873189fbc7537a9d01d54c105361f4310b97))
* add errors on tests" ([8df1169](https://bitbucket.org/6place/6place-sdk-js/commit/8df11698bb0e1dd5336ef5801115d1977fd1fcb8))


### Bug Fixes

* refactoring address methods ([7076e9e](https://bitbucket.org/6place/6place-sdk-js/commit/7076e9eb2f8d101a574a6eb8d6cb2acc5a78a35c))

## [0.8.0](https://bitbucket.org/6place/6place-sdk-js/compare/v0.7.0...v0.8.0) (2021-12-09)


### ⚠ BREAKING CHANGES

* adjusting createUser response

### Features

* get user details method ([bd4debd](https://bitbucket.org/6place/6place-sdk-js/commit/bd4debdf5441cea415f579711910b32784bee374))


### Bug Fixes

* adjusting createUser response ([c1e37b8](https://bitbucket.org/6place/6place-sdk-js/commit/c1e37b8c115fa8cd2e1d6719a3c30415e992171e))

## [0.7.0](https://bitbucket.org/6place/6place-sdk-js/compare/v0.6.3...v0.7.0) (2021-12-09)


### ⚠ BREAKING CHANGES

* product detail

### Features

* add product details ([8db9713](https://bitbucket.org/6place/6place-sdk-js/commit/8db97138102015c8ac7da5820a1db8d8ae2a7a06))
* product detail ([e82278b](https://bitbucket.org/6place/6place-sdk-js/commit/e82278b842cb1df7cd76b3c0cb25e676182ba5e4))

### [0.6.3](https://bitbucket.org/6place/6place-sdk-js/compare/v0.6.2...v0.6.3) (2021-12-08)


### Bug Fixes

* validation method wrong type ([53bc4d1](https://bitbucket.org/6place/6place-sdk-js/commit/53bc4d1f7d1d9adbae990298362b6fe5882323b4))

### [0.6.2](https://bitbucket.org/6place/6place-sdk-js/compare/v0.6.1...v0.6.2) (2021-12-07)


### Bug Fixes

* setEnvironment signature ([06fd3ea](https://bitbucket.org/6place/6place-sdk-js/commit/06fd3eaba2ad87f7e3a39b54704bcdd1489434ce))

### [0.6.1](https://bitbucket.org/6place/6place-sdk-js/compare/v0.6.0...v0.6.1) (2021-12-07)


### Features

* exporting ConfigKeys ([6b21327](https://bitbucket.org/6place/6place-sdk-js/commit/6b21327734684162f349e36a5b5403db8b61ed3c))

## [0.6.0](https://bitbucket.org/6place/6place-sdk-js/compare/v0.5.5...v0.6.0) (2021-12-07)


### ⚠ BREAKING CHANGES

* responses interfaces were breaking app

### Bug Fixes

* responses interfaces were breaking app ([d02e086](https://bitbucket.org/6place/6place-sdk-js/commit/d02e086c0b312e88f24e5041e1efebdc52ea0825))

### [0.5.5](https://bitbucket.org/6place/6place-sdk-js/compare/v0.5.4...v0.5.5) (2021-12-06)

### [0.5.4](https://bitbucket.org/6place/6place-sdk-js/compare/v0.5.3...v0.5.4) (2021-12-06)


### Features

* creating method to get all chats ([fc2fb13](https://bitbucket.org/6place/6place-sdk-js/commit/fc2fb135242b72426b9fdf83d52cc5ef21f07394))
* getChat method ([a8d79f5](https://bitbucket.org/6place/6place-sdk-js/commit/a8d79f5f84365f313641f5e5e965daaa7b8be97e))
* method to get provider orders ([7376aa2](https://bitbucket.org/6place/6place-sdk-js/commit/7376aa28e01909f6f7af7adfca824f6e8ca282b5))
* method to get provider's orders ([787ba17](https://bitbucket.org/6place/6place-sdk-js/commit/787ba17192f329203a4c1c0b6cc058e7c5e90357))


### Bug Fixes

* data returning error ([a20eb59](https://bitbucket.org/6place/6place-sdk-js/commit/a20eb59097a2e83d2048c0924ba3d7a273b3104d))

### [0.5.3](https://bitbucket.org/6place/6place-sdk-js/compare/v0.4.1...v0.5.3) (2021-11-24)


### Features

* add categories ([cd46d62](https://bitbucket.org/6place/6place-sdk-js/commit/cd46d629f8bd12a6f4a4256c7f441baf6ce05cde))
* add exports ([5a187b2](https://bitbucket.org/6place/6place-sdk-js/commit/5a187b255842f617672874b617a904081b72c644))
* add get all orders ([ff9b521](https://bitbucket.org/6place/6place-sdk-js/commit/ff9b52175a945a969a759d8f414af10b8d64ef19))
* add hightlights ([d2880aa](https://bitbucket.org/6place/6place-sdk-js/commit/d2880aa8aa8762168eddf237a3ec52e3cf7105d9))
* add order method ([26dec25](https://bitbucket.org/6place/6place-sdk-js/commit/26dec2542afbc24c79d4551572dccba8e2f7daa8))
* add order type and move folders place ([7731ff4](https://bitbucket.org/6place/6place-sdk-js/commit/7731ff4b089b4341e9c4e29ac9e353cfea5e11ca))
* add provider methods ([f5aa858](https://bitbucket.org/6place/6place-sdk-js/commit/f5aa8584a792b87d9c853b417d13f733ba32b10f))
* bumping version and docs ([2a0309a](https://bitbucket.org/6place/6place-sdk-js/commit/2a0309a37f50c26092b03520a13770966c000f6f))
* creating code validation methods ([99e11c8](https://bitbucket.org/6place/6place-sdk-js/commit/99e11c81d006712b1d99fa67a4283168b0478547))
* creating getProducts method ([2c743b7](https://bitbucket.org/6place/6place-sdk-js/commit/2c743b7619b3bd0c1a4819e67040d00949e22e5f))
* location methods ([3d21cc4](https://bitbucket.org/6place/6place-sdk-js/commit/3d21cc456004385ddf43e0c427927af263936c9a))


### Bug Fixes

* adding missing modules to index.ts ([7753ba1](https://bitbucket.org/6place/6place-sdk-js/commit/7753ba189534529e556a73d4f93015374f310423))
* auth isLogged method ([897ec7b](https://bitbucket.org/6place/6place-sdk-js/commit/897ec7b82049213b64859a2838cfa28853b72d0d))
* auth tests ([4833607](https://bitbucket.org/6place/6place-sdk-js/commit/48336077e4c4cb029bccd6a75eb3362465180cd4))
* change highlight ([3d2dde6](https://bitbucket.org/6place/6place-sdk-js/commit/3d2dde626e7868b16c3e777a1a42c0e75c16ace1))
* changing back to npm to build package ([d30030e](https://bitbucket.org/6place/6place-sdk-js/commit/d30030ec24f961787256aec2a547c340597150da))
* cloud build now using yarn ([7acf789](https://bitbucket.org/6place/6place-sdk-js/commit/7acf789872cd94041413fe76d8c8ffb5b0f14329))
* function signature ([42f00c8](https://bitbucket.org/6place/6place-sdk-js/commit/42f00c843a07ceb6d4db5057269bae60e24a6a0e))
* improving test files ([d7b9f4e](https://bitbucket.org/6place/6place-sdk-js/commit/d7b9f4e7c2444d707b9a4a599e63ab78a61ff200))
* isLoggedIn return value ([6ded5da](https://bitbucket.org/6place/6place-sdk-js/commit/6ded5da975d0a811cfa481f455caa3ef36aff394))
* method name ([96fcb69](https://bitbucket.org/6place/6place-sdk-js/commit/96fcb69250db6332246844930c85c65db45cfbe6))
* package version ([3111c35](https://bitbucket.org/6place/6place-sdk-js/commit/3111c35b10a9c6d8b01c2f7e612bc0996873394c))
* parameter type ([3b6f2fd](https://bitbucket.org/6place/6place-sdk-js/commit/3b6f2fd327785b3c588468a3ea3efc0a4150bd58))

### 0.4.1 (2021-11-11)
