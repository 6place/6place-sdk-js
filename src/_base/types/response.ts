export interface Response {
  success: true;
  status: number;
  data?: {
    data?: any;
  };
}
