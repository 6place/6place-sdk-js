import axios from 'axios';
import { setEnvironment, _unsetEnvConf } from '../config';
import { getVendorSettings } from './index';

describe('vendor settings', () => {
  beforeAll(() => setEnvironment(process.env.NODE_ENV ?? 'test'));
  afterAll(_unsetEnvConf);

  it('get vendor settings successfully', async () => {
    jest.spyOn(axios, 'get').mockResolvedValueOnce({
      data: { success: true },
    });
    expect(typeof (await getVendorSettings('6place'))).toBe('object');
  });
  it('throws error for unfulfilled request', async () => {
    jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('teste'));
    await expect(getVendorSettings('empresa-ficticia')).rejects.toThrow('teste');
  });
});
