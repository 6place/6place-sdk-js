/**
 * Manages user related methods
 * @module
 */
import axios, { AxiosRequestConfig } from 'axios';
import { getBearerAuth, _getEnvConf } from '../config';

/**
 * VendorSettings object interface
 */
export type VendorSettings = {
  success: boolean;
  data?: {
    name: string;
    theme: {
      primaryRGB: Array<number>;
    };
    assetsPath: string;
    cnpj: string;
    email: string;
    phone: string;
    logoUrl: string;
    website: string;
    socialMedia: {
      facebook: string;
      instagram: string;
      linkedin: string;
      youtube: string;
    },
    rights: string;
  };
};

var _bearer: string | null = ''; 
getBearerAuth().then(token => {_bearer = token});

const _getRequestConfig = (): AxiosRequestConfig => ({
  withCredentials: true,
  headers: {
    authorization: `Bearer ${_bearer}`,
  },
});

/**
 * Gets configuration settings for specific vendor
 * @param vendor string do id do vendor/marketplace
 * @returns Promise with object of type [[VendorSettings]]
 * @throws Error when the request is not successful
 */
export const getVendorSettings = async (vendor: string): Promise<VendorSettings> => {
  const params = { vendor };
  const response = (await axios.get(`${_getEnvConf()?.endpoints.api}/v1/setting/vendor`, {
    params,
    ..._getRequestConfig(),
  }));
  return response.data as VendorSettings;
};
