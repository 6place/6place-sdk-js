import axios from 'axios';
import { createMock } from 'ts-auto-mock';
import { setEnvironment, _unsetEnvConf } from '../config';
import { Order } from '../order/types';
import { Product, ProductResponse } from '../products/types';
import {
  getDashboard,
  getProviders,
  getOrders,
  checkDocument,
  getProvider,
  recommendProvider,
  getProducts,
  getProduct,
  getEvaluations,
  getHours,
  getPayments,
  getRepresentatives,
  createProvider,
  createProduct,
  updateProduct,
  updatePayments,
  updateHours,
  createEvaluation,
  getOrder,
  deleteProduct,
} from './';
import {
  CreateProviderParams,
  Dashboard,
  DocumentAlreadyExistsError,
  EvaluationParams,
  HoursParams,
  MissingParamsError,
  NameAlreadyExistsError,
  OrderResponse,
  ProductParams,
  Provider,
  ProviderEvaluation,
  ProviderHour,
  ProviderNotFoundError,
  ProviderPayment,
  ProviderRepresentative,
  ProviderResponse,
  RecommendationParams,
  ShelfLifeTimeIsNotANumberError,
  ShelfLifeTimeValueError,
  ShelfLifeTypeIncompatibleError,
  SuggestedPriceIsNotANumberError,
  SuggestedPriceValueError,
} from './types';

describe('providers', () => {
  beforeAll(() => setEnvironment(process.env.NODE_ENV ?? 'test'));
  afterAll(_unsetEnvConf);

  it('should get providers successfuly', async () => {
    const expected: ProviderResponse = createMock<ProviderResponse>();
    jest.spyOn(axios, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
    const result = await getProviders();
    expect(typeof result).toBe('object');
    expect(result).toBe(expected);
  });
  it('throws error for unfulfilled request', async () => {
    jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('unknown error'));
    await expect(getProviders()).rejects.toThrow('unknown error');
  });

  it('should get one provider successfuly', async () => {
    const expected: Provider = createMock<Provider>();
    jest.spyOn(axios, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
    const result = await getProvider(123);
    expect(typeof result).toBe('object');
    expect(result).toBe(expected);
  });
  it('throws error for unfulfilled request', async () => {
    jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('unknown error'));
    await expect(getProvider(123)).rejects.toThrow('unknown error');
  });

  it('should create provider successfuly', async () => {
    const params: CreateProviderParams = createMock<CreateProviderParams>();
    const expected: Provider = createMock<Provider>();
    jest.spyOn(axios, 'post').mockResolvedValueOnce({ data: { success: true, data: expected } });
    const result = await createProvider(params);
    expect(typeof result).toBe('object');
    expect(result).toBe(expected);
  });
  it('throws error for unfulfilled requests', async () => {
    const params: CreateProviderParams = createMock<CreateProviderParams>();
    jest.spyOn(axios, 'post').mockRejectedValueOnce(new Error('unknown error'));
    await expect(createProvider(params)).rejects.toThrow('unknown error');
    jest.spyOn(axios, 'post').mockRejectedValueOnce({
      response: { data: { success: false, error: { code: 408, message: 'Document exists' } } },
    });
    await expect(createProvider(params)).rejects.toThrow(DocumentAlreadyExistsError);
    jest.spyOn(axios, 'post').mockRejectedValueOnce({
      response: { data: { success: false, error: { code: 410, message: 'Document exists' } } },
    });
    await expect(createProvider(params)).rejects.toThrow(NameAlreadyExistsError);
  });

  describe('providers recommendations', () => {
    it('should return true when request is successfull', async () => {
      const mockParams: RecommendationParams = createMock<RecommendationParams>();
      jest.spyOn(axios, 'post').mockResolvedValueOnce({ data: { success: true } });
      await expect(recommendProvider(mockParams)).resolves.toBe(true);
    });
    it('should return false when request is unsuccessfull', async () => {
      const mockParams: RecommendationParams = createMock<RecommendationParams>();
      jest.spyOn(axios, 'post').mockRejectedValueOnce(new Error('any error whatever'));
      await expect(recommendProvider(mockParams)).resolves.toBe(false);
    });
  });

  describe('providers orders', () => {
    it('should get orders successfuly', async () => {
      const expected: OrderResponse = createMock<OrderResponse>();
      jest.spyOn(axios, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
      const result = await getOrders(123);
      expect(typeof result).toBe('object');
      expect(result).toBe(expected);
    });
    it('throws error for unfulfilled request', async () => {
      jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('unknown error'));
      await expect(getOrders(123)).rejects.toThrow('unknown error');
    });
    it('should get single order successfuly', async () => {
      const expected: Order = createMock<Order>();
      jest.spyOn(axios, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
      const result = await getOrder(123, 321);
      expect(typeof result).toBe('object');
      expect(result).toBe(expected);
    });
    it('throws error for unfulfilled request', async () => {
      jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('unknown error'));
      await expect(getOrder(123, 321)).rejects.toThrow('unknown error');
    });
  });

  describe('providers products', () => {
    it('should get products successfuly', async () => {
      const expected: ProductResponse = createMock<ProductResponse>();
      jest.spyOn(axios, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
      const result = await getProducts(123);
      expect(typeof result).toBe('object');
      expect(result).toBe(expected);
    });
    it('throws error for unfulfilled request', async () => {
      jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('unknown error'));
      await expect(getProducts(123)).rejects.toThrow('unknown error');
    });
    it('should get single product successfuly', async () => {
      const expected: Product = createMock<Product>();
      jest.spyOn(axios, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
      const result = await getProduct(123, 321);
      expect(typeof result).toBe('object');
      expect(result).toBe(expected);
    });
    it('throws error for unfulfilled request', async () => {
      jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('unknown error'));
      await expect(getProduct(444, 333)).rejects.toThrow('unknown error');
    });
    it('should create product successfuly', async () => {
      const params: ProductParams = createMock<ProductParams>();
      const expected: Product = createMock<Product>();
      jest.spyOn(axios, 'post').mockResolvedValueOnce({ data: { success: true, data: expected } });
      const result = await createProduct(123, params);
      expect(typeof result).toBe('object');
      expect(result).toBe(expected);
    });
    it('throws error for unfulfilled requests', async () => {
      const params: ProductParams = createMock<ProductParams>();
      jest.spyOn(axios, 'post').mockRejectedValueOnce(new Error('unknown error'));
      await expect(createProduct(444, params)).rejects.toThrow('unknown error');

      jest.spyOn(axios, 'post').mockRejectedValueOnce({
        response: { data: { success: false, error: { code: 99, message: 'Generic Message' } } },
      });
      await expect(createProduct(444, params)).rejects.toThrow(MissingParamsError);
      jest.spyOn(axios, 'post').mockRejectedValueOnce({
        response: { data: { success: false, error: { code: 1400, message: 'Generic Message' } } },
      });
      await expect(createProduct(444, params)).rejects.toThrow(ProviderNotFoundError);
      jest.spyOn(axios, 'post').mockRejectedValueOnce({
        response: { data: { success: false, error: { code: 1600, message: 'Generic Message' } } },
      });
      await expect(createProduct(444, params)).rejects.toThrow(ShelfLifeTimeIsNotANumberError);
      jest.spyOn(axios, 'post').mockRejectedValueOnce({
        response: { data: { success: false, error: { code: 1601, message: 'Generic Message' } } },
      });
      await expect(createProduct(444, params)).rejects.toThrow(ShelfLifeTimeValueError);
      jest.spyOn(axios, 'post').mockRejectedValueOnce({
        response: { data: { success: false, error: { code: 1602, message: 'Generic Message' } } },
      });
      await expect(createProduct(444, params)).rejects.toThrow(ShelfLifeTypeIncompatibleError);
      jest.spyOn(axios, 'post').mockRejectedValueOnce({
        response: { data: { success: false, error: { code: 1700, message: 'Generic Message' } } },
      });
      await expect(createProduct(444, params)).rejects.toThrow(SuggestedPriceIsNotANumberError);
      jest.spyOn(axios, 'post').mockRejectedValueOnce({
        response: { data: { success: false, error: { code: 1701, message: 'Generic Message' } } },
      });
      await expect(createProduct(444, params)).rejects.toThrow(SuggestedPriceValueError);
    });
    it('should update product successfuly', async () => {
      const params: ProductParams = createMock<ProductParams>();
      const expected: Product = createMock<Product>();
      jest.spyOn(axios, 'put').mockResolvedValueOnce({ data: { success: true, data: expected } });
      const result = await updateProduct(123, 321, params);
      expect(typeof result).toBe('object');
      expect(result).toBe(expected);
    });
    it('throws error for unfulfilled requests', async () => {
      const params: ProductParams = createMock<ProductParams>();
      jest.spyOn(axios, 'put').mockRejectedValueOnce(new Error('unknown error'));
      await expect(updateProduct(444, 333, params)).rejects.toThrow('unknown error');

      jest.spyOn(axios, 'put').mockRejectedValueOnce({
        response: { data: { success: false, error: { code: 99, message: 'Generic Message' } } },
      });
      await expect(updateProduct(444, 333, params)).rejects.toThrow(MissingParamsError);
      jest.spyOn(axios, 'put').mockRejectedValueOnce({
        response: { data: { success: false, error: { code: 1400, message: 'Generic Message' } } },
      });
      await expect(updateProduct(444, 333, params)).rejects.toThrow(ProviderNotFoundError);
      jest.spyOn(axios, 'put').mockRejectedValueOnce({
        response: { data: { success: false, error: { code: 1600, message: 'Generic Message' } } },
      });
      await expect(updateProduct(444, 333, params)).rejects.toThrow(ShelfLifeTimeIsNotANumberError);
      jest.spyOn(axios, 'put').mockRejectedValueOnce({
        response: { data: { success: false, error: { code: 1601, message: 'Generic Message' } } },
      });
      await expect(updateProduct(444, 333, params)).rejects.toThrow(ShelfLifeTimeValueError);
      jest.spyOn(axios, 'put').mockRejectedValueOnce({
        response: { data: { success: false, error: { code: 1602, message: 'Generic Message' } } },
      });
      await expect(updateProduct(444, 333, params)).rejects.toThrow(ShelfLifeTypeIncompatibleError);
      jest.spyOn(axios, 'put').mockRejectedValueOnce({
        response: { data: { success: false, error: { code: 1700, message: 'Generic Message' } } },
      });
      await expect(updateProduct(444, 333, params)).rejects.toThrow(
        SuggestedPriceIsNotANumberError
      );
      jest.spyOn(axios, 'put').mockRejectedValueOnce({
        response: { data: { success: false, error: { code: 1701, message: 'Generic Message' } } },
      });
      await expect(updateProduct(444, 333, params)).rejects.toThrow(SuggestedPriceValueError);
    });
    it('should delete product successfuly', async () => {
      jest.spyOn(axios, 'delete').mockResolvedValueOnce({ data: { success: true } });
      const result = await deleteProduct(123, 321);
      expect(typeof result).toBe('boolean');
      expect(result).toStrictEqual(true);
    });
    it('throws error for unfulfilled request delete', async () => {
      jest.spyOn(axios, 'delete').mockRejectedValueOnce(new Error('unknown error'));
      await expect(deleteProduct(444, 333)).resolves.toBe(false);
    });
  });

  describe('evaluations', () => {
    it('should get evaluations successfuly', async () => {
      const mockEvaluation: ProviderEvaluation = createMock<ProviderEvaluation>();
      const expected = [mockEvaluation];
      jest.spyOn(axios, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
      const result = await getEvaluations(123);
      expect(typeof result).toBe('object');
      expect(result).toBe(expected);
    });
    it('throws error for unfulfilled request', async () => {
      jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('unknown error'));
      await expect(getEvaluations(123)).rejects.toThrow('unknown error');
    });
    it('should create evaluation successfuly', async () => {
      const params: EvaluationParams = createMock<EvaluationParams>();
      const expected: ProviderEvaluation = createMock<ProviderEvaluation>();
      jest.spyOn(axios, 'post').mockResolvedValueOnce({ data: { success: true, data: expected } });
      const result = await createEvaluation(123, params);
      expect(typeof result).toBe('object');
      expect(result).toBe(expected);
    });
    it('throws error for unfulfilled request', async () => {
      const params: EvaluationParams = createMock<EvaluationParams>();
      jest.spyOn(axios, 'post').mockRejectedValueOnce(new Error('unknown error'));
      await expect(createEvaluation(444, params)).rejects.toThrow('unknown error');
    });
  });

  describe('office hours', () => {
    it('should get hours successfuly', async () => {
      const mockHour: ProviderHour = createMock<ProviderHour>();
      const expected = [mockHour];
      jest.spyOn(axios, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
      const result = await getHours(123);
      expect(typeof result).toBe('object');
      expect(result).toBe(expected);
    });
    it('throws error for unfulfilled request', async () => {
      jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('unknown error'));
      await expect(getHours(123)).rejects.toThrow('unknown error');
    });
    it('should update hours successfuly', async () => {
      const params: HoursParams = createMock<HoursParams>();
      const expected = true;
      jest.spyOn(axios, 'put').mockResolvedValueOnce({ data: { success: true } });
      const result = await updateHours(123, [params]);
      expect(typeof result).toBe('boolean');
      expect(result).toBe(expected);
    });
    it('should return false on update hours error', async () => {
      jest.spyOn(axios, 'put').mockRejectedValueOnce(new Error('unknown error'));
      await expect(updateHours(123, [])).resolves.toBe(false);
    });
  });

  describe('payments', () => {
    it('should get payments successfuly', async () => {
      const mockPayment: ProviderPayment = createMock<ProviderPayment>();
      const expected = [mockPayment];
      jest.spyOn(axios, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
      const result = await getPayments(123);
      expect(typeof result).toBe('object');
      expect(result).toBe(expected);
    });
    it('throws error for unfulfilled request', async () => {
      jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('unknown error'));
      await expect(getPayments(123)).rejects.toThrow('unknown error');
    });
    it('should update payments successfuly', async () => {
      const params = ['234', '342'];
      const expected = true;
      jest.spyOn(axios, 'put').mockResolvedValueOnce({ data: { success: true } });
      const result = await updatePayments(123, params);
      expect(typeof result).toBe('boolean');
      expect(result).toBe(expected);
    });
    it('should return false on update payments error', async () => {
      jest.spyOn(axios, 'put').mockRejectedValueOnce(new Error('unknown error'));
      await expect(updatePayments(123, [])).resolves.toBe(false);
    });
  });

  describe('representatives', () => {
    it('should get representatives successfuly', async () => {
      const mockRepresentative: ProviderRepresentative = createMock<ProviderRepresentative>();
      const expected = [mockRepresentative];
      jest.spyOn(axios, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
      const result = await getRepresentatives(123);
      expect(typeof result).toBe('object');
      expect(result).toBe(expected);
    });
    it('throws error for unfulfilled request', async () => {
      jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('unknown error'));
      await expect(getRepresentatives(123)).rejects.toThrow('unknown error');
    });
  });

  describe('dashboard', () => {
    it('should get dashboard successfuly', async () => {
      const expected: Dashboard = createMock<Dashboard>();
      jest.spyOn(axios, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
      const result = await getDashboard(123);
      expect(typeof result).toBe('object');
      expect(result).toBe(expected);
    });
    it('throws error for unfulfilled request', async () => {
      jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('unknown error'));
      await expect(getDashboard(123)).rejects.toThrow('unknown error');
    });
  });

  describe('document', () => {
    it('should return true on successful response', async () => {
      jest.spyOn(axios, 'get').mockResolvedValueOnce({ data: { success: true } });
      const result = await checkDocument('qwerty');
      expect(typeof result).toBe('boolean');
      expect(result).toBe(true);
    });
    it('should return false when document already exists', async () => {
      jest.spyOn(axios, 'get').mockRejectedValueOnce({
        response: {
          data: { success: false, error: { code: 408, message: 'Document already exists' } },
        },
      });
      const result = await checkDocument('qwerty');
      expect(typeof result).toBe('boolean');
      expect(result).toBe(false);
    });
    it('throws error for unfulfilled request', async () => {
      jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('unknown error'));
      await expect(checkDocument('ytrewq')).rejects.toThrow('unknown error');
    });
  });
});
