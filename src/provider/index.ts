/**
 * Manages order related methods
 * @module
 */
import axios, { AxiosRequestConfig } from 'axios';
import { getBearerAuth, _getEnvConf } from '../config';
import { Order } from '../order/types';
import { Product, ProductResponse } from '../products/types';
import { Response } from '../_base/types/response';
import {
  CreateProviderParams,
  Dashboard,
  DocumentAlreadyExistsError,
  EvaluationParams,
  HoursParams,
  MissingParamsError,
  NameAlreadyExistsError,
  OrderResponse,
  ProductParams,
  Provider,
  ProviderEvaluation,
  ProviderHour,
  ProviderNotFoundError,
  ProviderPayment,
  ProviderRepresentative,
  ProviderResponse,
  ProviderStatus,
  ProviderTypes,
  RecommendationParams,
  ShelfLifeTimeIsNotANumberError,
  ShelfLifeTimeValueError,
  ShelfLifeTypeIncompatibleError,
  SuggestedPriceIsNotANumberError,
  SuggestedPriceValueError,
} from './types';

var _bearer: string | null = ''; 
getBearerAuth().then(token => {_bearer = token});

const _getRequestConfig = (): AxiosRequestConfig => ({
  withCredentials: true,
  headers: {
    authorization: `Bearer ${_bearer}`,
  },
});

/**
 * Get all providers
 * @param type one of [[ProviderTypes]]
 * @param status one of [[ProviderStatus]]
 * @returns A promise with a [[ProviderResponse]] with a [[Provider]] array if request is successful
 * @throws Error when the request is not successful
 */
export const getProviders = async (
  type?: ProviderTypes,
  status?: ProviderStatus
): Promise<ProviderResponse> => {
  const { data } = await axios.get<any>(`${_getEnvConf()?.endpoints.api}/v3/provider/`, {
    ..._getRequestConfig(),
    params: {
      type,
      status,
    },
  });

  return (data?.data || []) as ProviderResponse;
};

/**
 * Get single provider details
 * @param id to find the provider
 * @returns A promise with a [[Provider]] object if request was successful
 * @throws Error when the request is not successful
 */
export const getProvider = async (id: number): Promise<Provider> => {
  const { data } = await axios.get<Response>(
    `${_getEnvConf()?.endpoints.api}/v1/provider/${id}`,
    _getRequestConfig()
  );
  return data.data as Provider;
};

/**
 * Get all products from provider
 * @param id of the provider
 * @param search search query to filter
 * @param isPromotion set true to search only for products with promotion
 * @param isDetail set to true to fetch exta details of the product
 * @param minValue search for values <= this param
 * @param page number of page to fetch
 * @param limit limit of products to fetch
 * @returns A promise with an array of [[Product]] objects if request was successful
 * @throws Error when the request is not successful
 */
export const getProducts = async (
  id: number,
  search?: string,
  isPromotion?: boolean,
  isDetail?: boolean,
  page?: number,
  limit?: number,
  minValue?: number
): Promise<ProductResponse> => {
  const params = {
    search,
    isPromotion,
    isDetail,
    page,
    limit,
    minValue,
  };
  const { data } = await axios.get<Response>(
    `${_getEnvConf()?.endpoints.api}/v3/provider/${id}/product/`,
    { params, ..._getRequestConfig() }
  );
  return data.data as ProductResponse;
};

/**
 * Get single product from provider
 * @param id of the provider
 * @param product id of the product to fetch
 * @returns A promise with a [[Product]] object if request was successful
 * @throws Error when the request is not successful
 */
export const getProduct = async (id: number, product: number): Promise<Product> => {
  const { data } = await axios.get<Response>(
    `${_getEnvConf()?.endpoints.api}/v1/provider/${id}/product/${product}`,
    _getRequestConfig()
  );
  return data.data as Product;
};

/**
 * Get provider evaluations
 * @param id of the provider
 * @returns A promise with an array of [[ProviderEvaluation]] objects if request was successful
 * @throws Error when the request is not successful
 */
export const getEvaluations = async (id: number): Promise<ProviderEvaluation[]> => {
  const { data } = await axios.get<Response>(
    `${_getEnvConf()?.endpoints.api}/v1/provider/${id}/evaluation`,
    _getRequestConfig()
  );
  return data.data as ProviderEvaluation[];
};

/**
 * Get provider working hours
 * @param id of the provider
 * @returns A promise with an array of [[ProviderHour]] objects if request was successful
 * @throws Error when the request is not successful
 */
export const getHours = async (id: number): Promise<ProviderHour[]> => {
  const { data } = await axios.get<Response>(
    `${_getEnvConf()?.endpoints.api}/v1/provider/${id}/hour`,
    _getRequestConfig()
  );
  return data.data as ProviderHour[];
};

/**
 * Get provider payment types
 * @param id of the provider
 * @returns A promise with an array of [[ProviderPayment]] objects if request was successful
 * @throws Error when the request is not successful
 */
export const getPayments = async (id: number): Promise<ProviderPayment[]> => {
  const { data } = await axios.get<Response>(
    `${_getEnvConf()?.endpoints.api}/v1/provider/${id}/payment`,
    _getRequestConfig()
  );
  return data.data as ProviderPayment[];
};

/**
 * Get provider representatives list
 * @param id of the provider
 * @returns A promise with an array of [[ProviderRepresentative]] objects if request was successful
 * @throws Error when the request is not successful
 */
export const getRepresentatives = async (id: number): Promise<ProviderRepresentative[]> => {
  const { data } = await axios.get<Response>(
    `${_getEnvConf()?.endpoints.api}/v1/provider/${id}/representative`,
    _getRequestConfig()
  );
  return data.data as ProviderRepresentative[];
};

/**
 * Cretae a provider
 * @param params Object of type [[CreateProviderParams]]
 * @returns A promise with a boolean indicating if the request was successful or not
 */
export const createProvider = async (params: CreateProviderParams): Promise<Provider> => {
  try {
    const { data } = await axios.post<Response>(`${_getEnvConf()?.endpoints.api}/v1/provider`, {
      params,
      ..._getRequestConfig(),
    });
    return data.data as Provider;
  } catch (e: any) {
    switch (e.response?.data?.error?.code) {
      case 408:
        throw new DocumentAlreadyExistsError(e.response.data);
      case 410:
        throw new NameAlreadyExistsError(e.response.data);
      default:
        throw new Error(e.message);
    }
  }
};

/**
 * @private
 */
const _createOrUpdateProduct = async (
  params: ProductParams,
  id: number,
  productId?: number
): Promise<Product> => {
  try {
    const { data } = await axios[productId ? 'put' : 'post']<Response>(
      `${_getEnvConf()?.endpoints.api}/v1/provider/${id}/product/${productId || ''}`,
        params,
        _getRequestConfig(),
    );
    return data.data as Product;
  } catch (e: any) {
    switch (e.response?.data?.error?.code) {
      case 99:
        throw new MissingParamsError(e.response.data);
      case 1400:
        throw new ProviderNotFoundError(e.response.data);
      case 1600:
        throw new ShelfLifeTimeIsNotANumberError(e.response.data);
      case 1601:
        throw new ShelfLifeTimeValueError(e.response.data);
      case 1602:
        throw new ShelfLifeTypeIncompatibleError(e.response.data);
      case 1700:
        throw new SuggestedPriceIsNotANumberError(e.response.data);
      case 1701:
        throw new SuggestedPriceValueError(e.response.data);
      default:
        throw new Error(e.message);
    }
  }
};

/**
 * Create a product for a provider
 * @param id of provider to associate with product
 * @param params Object of type [[ProductParams]]
 * @returns A promise with a [[Product]] object if the request was successful
 * @throws Error when request is unsuccessful
 */
export const createProduct = async (id: number, params: ProductParams): Promise<Product> => {
  return _createOrUpdateProduct(params, id);
};

/**
 * Update a product from a provider
 * @param id of provider to associate with product
 * @param productId id of the product to be updated
 * @param params Object of type [[ProductParams]]
 * @returns A promise with a [[Product]] object if the request was successful
 * @throws Error when request is unsuccessful
 */
export const updateProduct = async (
  id: number,
  productId: number,
  params: ProductParams
): Promise<Product> => {
  return _createOrUpdateProduct(params, id, productId);
};

/**
 * Recommend a provider to the platform
 * @param params Object of type [[RecommendationParams]]
 * @returns A promise with a boolean indicating if the request was successful or not
 */
export const recommendProvider = async (params: RecommendationParams): Promise<boolean> => {
  try {
    await axios.post<Response>(`${_getEnvConf()?.endpoints.api}/v1/provider/indicate`, {
      params,
      ..._getRequestConfig(),
    });
    return true;
  } catch (e) {
    return false;
  }
};

/**
 * Update provider payments list
 * @param id of provider to add payments to
 * @param payments Array of ids of payment types
 * @returns A promise with a boolean indicating if the request was successful or not
 */
export const updatePayments = async (id: number, payments: string[]): Promise<boolean> => {
  try {
    const { data } = await axios.put<Response>(
      `${_getEnvConf()?.endpoints.api}/v1/provider/${id}/payment`,
      {
        params: { payments },
        ..._getRequestConfig(),
      }
    );
    return data.success;
  } catch (e) {
    return false;
  }
};
/**
 * Update provider office hours
 * @param id of provider to add payments to
 * @param hours Array of [[HoursParams]] object
 * @returns A promise with a boolean indicating if the request was successful or not
 */
export const updateHours = async (id: number, hours: HoursParams[]): Promise<boolean> => {
  try {
    const { data } = await axios.put<Response>(
      `${_getEnvConf()?.endpoints.api}/v1/provider/${id}/hour`,
      {
        params: { hours },
        ..._getRequestConfig(),
      }
    );
    return data.success;
  } catch (e) {
    return false;
  }
};

/**
 * Get single provider details
 * @param id of provider to associate with evaluation
 * @param params Object of type [[EvaluationParams]]
 * @returns A promise with a boolean indicating if the request was successful or not
 */
export const createEvaluation = async (
  id: number,
  params: EvaluationParams
): Promise<ProviderEvaluation> => {
  const { data } = await axios.post<Response>(
    `${_getEnvConf()?.endpoints.api}/v1/provider/${id}/evaluation`,
    {
      params,
      ..._getRequestConfig(),
    }
  );
  return data.data as ProviderEvaluation;
};

/**
 * Get orders from provider
 * @param id of provider to get orders
 * @returns A promise with a [[OrderResponse]] with an [[Order]] array request was successful
 * @throws Error when the request is not successful
 */
export const getOrders = async (id: number): Promise<OrderResponse> => {
  const { data } = await axios.get<Response>(
    `${_getEnvConf()?.endpoints.api}/v2/provider/${id}/order`,
    _getRequestConfig()
  );
  return data.data as OrderResponse;
};

/**
 * Get order detail from provider
 * @param id of provider to get order
 * @param order id of the order to fetch
 * @returns A promise with an [[Order]] object if request was successful
 * @throws Error when the request is not successful
 */
export const getOrder = async (id: number, order: number): Promise<Order> => {
  const { data } = await axios.get<Response>(
    `${_getEnvConf()?.endpoints.api}/v1/provider/${id}/order/${order}`,
    _getRequestConfig()
  );
  return data.data as Order;
};

/**
 * Get dashboard data for a given provider id
 * @param id of provider to get dashboard
 * @returns A promise with a [[Dashboard]] if request was successful
 * @throws Error when the request is not successful
 */
export const getDashboard = async (id: number): Promise<Dashboard> => {
  const { data } = await axios.get<Response>(
    `${_getEnvConf()?.endpoints.api}/v1/provider/${id}/dashboard`,
    _getRequestConfig()
  );
  return data.data as Dashboard;
};

/**
 * Check if document doesn't exist in database
 * @param document to verify
 * @returns A promise with a true if document doesn't exist or false if document exists in database
 * @throws Error when the request is not successful
 */
export const checkDocument = async (document: string): Promise<boolean> => {
  const params = { document };
  try {
    const { data } = await axios.get<Response>(
      `${_getEnvConf()?.endpoints.api}/v1/provider/document`,
      {
        params,
        ..._getRequestConfig(),
      }
    );
    return data.success;
  } catch (e: any) {
    switch (e.response?.data?.error?.code) {
      case 408: // DOCUMENT_ALREADY_EXISTS
        return false;
      default:
        throw new Error(e.message);
    }
  }
};

/**
 * Delete a provider product
 * @param id of the provider
 * @param productId id of the product to delete
 * @returns A promise with a boolean indicating if the operation was successful or not
 * @throws Error when the request is not successful
 */
export const deleteProduct = async (id: number, productId: number): Promise<boolean> => {
  try {
    const { data } = await axios.delete<Response>(
      `${_getEnvConf()?.endpoints.api}/v1/provider/${id}/product/${productId}`,
      {
        ..._getRequestConfig(),
      }
    );
    return data.success;
  } catch (e) {
    return false;
  }
};
