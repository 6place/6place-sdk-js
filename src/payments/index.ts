/**
 * Manages order related methods
 * @module
 */
 import axios, { AxiosRequestConfig } from 'axios';
 import { getBearerAuth, _getEnvConf } from '../config';
 import { Payment } from './types';
 
 var _bearer: string | null = ''; 
getBearerAuth().then(token => {_bearer = token});
 
 const _getRequestConfig = (): AxiosRequestConfig => ({
   withCredentials: true,
   headers: {
     authorization: `Bearer ${_bearer}`,
   },
 });
 
 /**
  * Get all order by user
  * @returns A promise with a order array if request is successful
  * @throws Error when the request is not successful
  */
 export const getPayments = async (): Promise<Payment[]> => {
   const { data } = await axios.get<any>(
     `${_getEnvConf()?.endpoints.api}/v1/payment/`,
     _getRequestConfig()
   );
 
   return data.data;
 };
 