export type Payment = {
  type: string;
  typeName: string;
  option?: string;
  optionName?: string;
  parent?: string;
};
