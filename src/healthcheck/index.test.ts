import axios from 'axios';
import { setEnvironment, _unsetEnvConf } from '../config';
import { getHealthcheck } from './index';

describe('index', () => {
  beforeAll(() => {
    setEnvironment(process.env.NODE_ENV ?? 'test');
  });
  afterAll(_unsetEnvConf);
  it('gets health check', async () => {
    jest.spyOn(axios, 'get').mockResolvedValueOnce({
      data: { name: 'mock-environment', version: '0.0.0-test', env: 'test', uptime: 123456 },
    });
    const healthCheck = await getHealthcheck();
    expect(healthCheck).toHaveProperty('uptime');
    expect(healthCheck.uptime).toBeGreaterThan(0);
  });
  it('health check not ok', async () => {
    jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('error'));
    await expect(getHealthcheck()).rejects.toThrow('error');
  });
});
