/**
 * Health check endpoint ping
 * @module
 */
import axios from 'axios';
import { _getEnvConf } from '../config';

/**
 * HealthCheck object interface
 */
export type HealthCheck = {
  /** name of the application */
  name: string;
  /** version tag */
  version: string;
  /** api environment string */
  env: string;
  /** uptime in seconds */
  uptime: number;
};

/**
 * Get health check status from the server
 * @returns Promise with object of type [[HealthCheck]]
 * @throws Error when the request is not successful
 */
export const getHealthcheck = async (): Promise<HealthCheck> => {
  const result = await axios.get<any>(`${_getEnvConf()?.endpoints.api}/v1/`);
  return result.data;
};
