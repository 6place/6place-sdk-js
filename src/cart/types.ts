import { Product } from '../products/types'

export type CartItem = {
  id: number;
  productId: number;
  quantity: number;
  deletedAt: string;
  createdAt: string;
  updatedAt: string;
  product: Product;
  orderId: number;
  customerId: number;
};

export type UpdateCartProductParams = { 
  productId: number 
  quantity: number 
  orderId?: string 
  customerId?: string 
}