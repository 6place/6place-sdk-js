/**
 * Manages user related methods
 * @module
 */
import axios, { AxiosRequestConfig } from 'axios';
import { getBearerAuth, _getEnvConf } from '../config';
import { CartItem, UpdateCartProductParams } from './types';

let _bearer: string | null = '';
getBearerAuth().then((token) => {
  _bearer = token;
});

const _getRequestConfig = (): AxiosRequestConfig => ({
  withCredentials: true,
  headers: {
    authorization: `Bearer ${_bearer}`,
  },
});

/**
 * Get Cart
 * @returns A promise with a cart array if request is successful
 * @throws Error when the request is not successful
 */
export const getCart = async (): Promise<CartItem[]> => {
  const { data } = await axios.get<any>(
    `${_getEnvConf()?.endpoints.api}/v1/cart/`,
    _getRequestConfig()
  );

  return data.data;
};

/**
 * Post Cart
 * create cart with all products
 * @throws Error when the request is not successful
 */
export const postCreateCart = async (params: UpdateCartProductParams): Promise<CartItem[]> => {
  const { data } = await axios.post<any>(
    `${_getEnvConf()?.endpoints.api}/v1/cart/`,
    params,
    _getRequestConfig()
  );

  return data.data;
};
