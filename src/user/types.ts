import { Provider } from '../provider/types';
import { CustomError } from '../_base/types/error';

export enum UserTypesEnum {
  NATURAL = 'user_natural',
  LEGAL = 'user_legal',
}
export enum DocumentTypesEnum {
  CNPJ = 'cnpj',
  CPF = 'cpf',
}

export enum ProviderTypesEnum {
  RETAIL = 'retail',
  WHOLESALE = 'wholesale',
}

export enum ValidationTypes {
  PHONE = 'phone',
  EMAIL = 'email',
}

export class InvalidEmailError extends CustomError {}
export class EmailAlreadyExistsError extends CustomError {}
export class DocumentAlreadyExistsError extends CustomError {}
export class PhoneAlreadyExistsError extends CustomError {}
export class InvalidCompanyError extends CustomError {}
export class CnpjaError extends CustomError {}
export class CodeExpiredError extends CustomError {}
export class CodeInvalidDateError extends CustomError {}
export class CodeInvalidError extends CustomError {}
export class UserInvalidNameError extends CustomError {}
export class UserInvalidTypeError extends CustomError {}

export type CreateUserType = {
  email: string;
  password: string;
  type: UserTypesEnum;
  name: string;
  phone: string;
  document: string;
  documentType: DocumentTypesEnum;
  providerType: ProviderTypesEnum;
};

export type UpdateUserType = {
  email: string;
  type: UserTypesEnum;
  name: string;
  phone: string;
  document: string;
  documentType: DocumentTypesEnum;
  status: string;
  ie: string;
  active: boolean;
  validated: boolean;
  companyName: string;
};

export type Support = {
  id: number;
  email: string;
  phone: string;
  document: string;
  documentType: string;
  ie: string;
  type: string;
  name: string;
  photoUrl?: string;
};

export type User = {
  id: number;
  email: string;
  emailValidated: boolean;
  phone: string;
  phoneValidated: boolean;
  photoUrl?: string;
  validated: boolean;
  name: string;
  status: string;
  type: string;
  document: string;
  documentType: string;
  createdBy?: string;
  companyName: string;
  ie: string;
  fup: boolean;
  lastFupAt?: any;
  nextFupAt?: any;
  originId: number;
  companyTypeId: number;
  commentsSupport: string;
  supportId: number;
  supportStatusId: number;
  createdAt: string;
  origin: {
    id: number;
    name: string;
  };
  companyType: {
    id: number;
    name: string;
  };
  supportStatus: {
    id: number;
    name: string;
  };
  support: Support;
};

export type DetailsResponse = {
  user: User;
  providers: ProviderDefinition[];
};

export declare type ProviderDefinition = {
  provider: Provider;
  type: string;
  commission: number;
};
