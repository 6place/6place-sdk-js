/**
 * Manages user related methods
 * @module
 */
import axios, { AxiosRequestConfig } from 'axios';
import { getBearerAuth, _getEnvConf } from '../config';
import { Response } from '../_base/types/response';
import {
  CodeExpiredError,
  CodeInvalidDateError,
  CodeInvalidError,
  CreateUserType,
  DetailsResponse,
  DocumentAlreadyExistsError,
  EmailAlreadyExistsError,
  InvalidEmailError,
  CnpjaError,
  PhoneAlreadyExistsError,
  InvalidCompanyError,
  UpdateUserType,
  User,
  UserInvalidNameError,
  UserInvalidTypeError,
  ValidationTypes,
} from './types';

var _bearer: string | null = ''; 
getBearerAuth().then(token => {_bearer = token});

const _getRequestConfig = (): AxiosRequestConfig => ({
  withCredentials: true,
  headers: {
    authorization: `Bearer ${_bearer}`,
  },
});

/**
 * Create new user with given data
 * @param user User Object of type [[CreateUserType]]
 * @returns A promise with a [[User]] if request was successful
 * @throws Error when the request is not successful
 */
export const createUser = async (user: CreateUserType): Promise<User> => {
  try {
    const { data } = await axios.post<Response>(
      `${_getEnvConf()?.endpoints.api}/v1/user/`,
      user,
      _getRequestConfig()
    );
    return data.data as User;
  } catch (e: any) {
    switch (e.response?.data.error.code) {
      case 201:
        throw new InvalidEmailError(e.response.data);
      case 401:
        throw new EmailAlreadyExistsError(e.response.data);
      case 402:
        throw new UserInvalidNameError(e.response.data);
      case 404:
        throw new UserInvalidTypeError(e.response.data);
      case 408:
        throw new DocumentAlreadyExistsError(e.response.data);
      case 411:
        throw new PhoneAlreadyExistsError(e.response.data);
      case 412:
        throw new InvalidCompanyError(e.response.data);
      case 1300:
        throw new CnpjaError(e.response.data);
      default:
        throw new Error(e.message);
    }
  }
};

/**
 * Update current user with given data
 * @param user User Object of type [[UpdateUserType]]
 * @returns A promise with a [[User]] if request was successful
 * @throws Error when the request is not successful
 */
export const updateUser = async (user: UpdateUserType): Promise<User> => {
  try {
    const { data } = await axios.put<Response>(
      `${_getEnvConf()?.endpoints.api}/v1/user/`,
      user,
      _getRequestConfig()
    );
    return data.data as User;
  } catch (e: any) {
    switch (e.response?.data.error.code) {
      case 201:
        throw new InvalidEmailError(e.response.data);
      case 401:
        throw new EmailAlreadyExistsError(e.response.data);
      case 402:
        throw new UserInvalidNameError(e.response.data);
      case 404:
        throw new UserInvalidTypeError(e.response.data);
      case 408:
        throw new DocumentAlreadyExistsError(e.response.data);
      case 411:
        throw new PhoneAlreadyExistsError(e.response.data);
      case 412:
        throw new InvalidCompanyError(e.response.data);
      case 1300:
        throw new CnpjaError(e.response.data);
      default:
        throw new Error(e.message);
    }
  }
};

/**
 * Get a user details data
 * @returns A promise with a [[User]] if request was successful
 * @throws Error when the request is not successful
 */
export const getDetails = async (): Promise<DetailsResponse> => {
  const { data } = await axios.get<Response>(
    `${_getEnvConf()?.endpoints.api}/v1/user/details`,
    _getRequestConfig()
  );
  return data.data as DetailsResponse;
};

/**
 * Get user origins
 * @returns A promise with an array of objects with id and name if request was successful
 * @throws Error when the request is not successful
 */
export const getOrigins = async (): Promise<{ id: number; name: string }> => {
  const { data } = await axios.get<Response>(
    `${_getEnvConf()?.endpoints.api}/v1/user/origin`,
    _getRequestConfig()
  );
  return data.data as { id: number; name: string };
};

/**
 * Get user company types
 * @returns A promise with an array of objects with id and name
 * @throws Error when the request is not successful
 */
export const getCompanyTypes = async (): Promise<{ id: number; name: string }> => {
  const { data } = await axios.get<Response>(
    `${_getEnvConf()?.endpoints.api}/v1/user/company`,
    _getRequestConfig()
  );
  return data.data as { id: number; name: string };
};

/**
 * Get user support statuses
 * @returns A promise with an array of objects with id, name and color
 * @throws Error when the request is not successful
 */
export const getSupportStatuses = async (): Promise<{
  id: number;
  name: string;
  color: string;
}> => {
  const { data } = await axios.get<Response>(
    `${_getEnvConf()?.endpoints.api}/v1/user/status`,
    _getRequestConfig()
  );
  return data.data as { id: number; name: string; color: string };
};

/**
 * Check if an e-mail is valid
 * @param email email address to check against database
 * @param userId optional id of the user to verify against record found
 * @returns boolean true if e-mail is valid
 * @throws Error, [[InvalidEmailError]], [[EmailAlreadyExistsError]]
 */
export const checkEmail = async (email: string, userId?: number): Promise<true> => {
  const params = { email, id: userId };
  try {
    await axios.get<Response>(`${_getEnvConf()?.endpoints.api}/v1/user/email`, {
      params,
      ..._getRequestConfig(),
    });
    return true;
  } catch (e: any) {
    switch (e.response?.data.error.code) {
      case 201:
        throw new InvalidEmailError(e.response.data);
      case 401:
        throw new EmailAlreadyExistsError(e.response.data);
      default:
        throw new Error(e.message);
    }
  }
};

/**
 * Check if a document is valid
 * @param document email address to check against database
 * @param userId optional id of the user to verify against record found
 * @returns boolean true if e-mail is valid
 * @throws Error, [[InvalidEmailError]], [[EmailAlreadyExistsError]]
 */
export const checkDocument = async (email: string, userId?: number): Promise<true> => {
  const params = { email, id: userId };
  try {
    await axios.get<Response>(`${_getEnvConf()?.endpoints.api}/v1/user/document`, {
      params,
      ..._getRequestConfig(),
    });
    return true;
  } catch (e: any) {
    switch (e.response?.data?.error?.code) {
      case 408:
        throw new DocumentAlreadyExistsError(e.response.data);
      default:
        throw new Error(e.message);
    }
  }
};

/**
 * Send password recovery e-mail to specified user
 * @param email e-mail used to login
 * @returns A promise with a boolean true if request was successful
 * @throws Error when the request is not successful
 */
export const recoverPassword = async (email: string): Promise<boolean> => {
  const params = { email };
  await axios.post(`${_getEnvConf()?.endpoints.api}/v1/user/recover`, params, _getRequestConfig());
  return true;
};

const _sendValidationCode = async (
  name: string,
  email: string | null = null,
  phone: string | null = null,
  type: ValidationTypes
) => {
  const params = { name, email, phone, type };
  const response = await axios.post<Response>(
    `${_getEnvConf()?.endpoints.api}/v1/user/code`,
    params,
    _getRequestConfig()
  );
  return response.data.success;
};

/**
 * Send e-mail validation message to registering user
 * @param name Name of the user
 * @param email e-mail used to login, which will receive the validation code
 * @returns A promise with a boolean true if request was successful
 * @throws Error when the request is not successful
 */
export const createEmailValidationCode = async (name: string, email: string): Promise<boolean> => {
  return await _sendValidationCode(name, email, null, ValidationTypes.EMAIL);
};

/**
 * Send phone validation message to registering user
 * @param name Name of the user
 * @param phone phone used to login, which will receive the validation code
 * @returns A promise with a boolean true if request was successful
 * @throws Error when the request is not successful
 */
export const createPhoneValidationCode = async (name: string, phone: string): Promise<boolean> => {
  return await _sendValidationCode(name, null, phone, ValidationTypes.PHONE);
};

/**
 * Validates the verification code
 * @param code code to validate
 * @param type type of code that is going to be validated
 * @returns A promise with a boolean true if request was successful
 * @throws Error, [[CodeInvalidError]], [[CodeExpiredError]], [[CodeInvalidDateError]]
 */
export const validateCode = async (code: string, type: ValidationTypes): Promise<boolean> => {
  try {
    const params = { code, type };
    const response = await axios.post<Response>(
      `${_getEnvConf()?.endpoints.api}/v1/user/code/validate`,
      params,
      _getRequestConfig()
    );
    return response.data.success;
  } catch (e: any) {
    switch (e.response?.data?.error?.code) {
      case 1100:
        throw new CodeInvalidError(e.response.data);
      case 1101:
        throw new CodeExpiredError(e.response.data);
      case 1102:
        throw new CodeInvalidDateError(e.response.data);
      default:
        throw new Error(e.message);
    }
  }
};

/**
 * Update password of specified user
 * @param oldPassword oldpassword of user
 * @param newPassword oldpassword of user 
 * @returns A promise with a boolean true if request was successful
 * @throws Error when the request is not successful
 */
export const changePassword = async (userId: number, oldPassword: string, newPassword: string): Promise<Boolean> => {
  const params = { userId, oldPassword, password: newPassword };
  const { data } = await axios.put(`${_getEnvConf()?.endpoints.api}/v1/user/password`, params, _getRequestConfig());
  if (data instanceof Error) {
    throw data;
  }

  return true;
};
