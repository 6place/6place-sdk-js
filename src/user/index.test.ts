import axios from 'axios';
import { createMock } from 'ts-auto-mock';
import { setEnvironment, _unsetEnvConf } from '../config';
import {
  checkDocument,
  checkEmail,
  createEmailValidationCode,
  createPhoneValidationCode,
  createUser,
  getCompanyTypes,
  getDetails,
  getOrigins,
  getSupportStatuses,
  recoverPassword,
  updateUser,
  validateCode,
} from './index';
import {
  CodeExpiredError,
  CodeInvalidDateError,
  CodeInvalidError,
  CreateUserType,
  DetailsResponse,
  DocumentAlreadyExistsError,
  EmailAlreadyExistsError,
  InvalidEmailError,
  PhoneAlreadyExistsError,
  UpdateUserType,
  User,
  UserInvalidNameError,
  UserInvalidTypeError,
  ValidationTypes,
} from './types';

describe('password recovery', () => {
  beforeAll(() => {
    setEnvironment(process.env.NODE_ENV ?? 'test');
  });
  afterAll(_unsetEnvConf);
  it('recovers password for existent user', async () => {
    jest.spyOn(axios, 'post').mockResolvedValueOnce({
      data: { success: true },
    });
    const result = await recoverPassword('existing-user@domain.com');
    expect(result).toBe(true);
  });
  it('throws error for inexistent user', async () => {
    jest.spyOn(axios, 'post').mockRejectedValueOnce(new Error('error'));
    await expect(recoverPassword('i.dont.know.who@that.is')).rejects.toThrow('error');
  });
});
describe('user creation', () => {
  const userResponseMock = createMock<User>();
  beforeAll(() => {
    setEnvironment(process.env.NODE_ENV ?? 'test');
  });
  afterAll(_unsetEnvConf);
  const userMock: CreateUserType = createMock<CreateUserType>();
  it('successfuly creates user with valid data', async () => {
    jest.spyOn(axios, 'post').mockResolvedValueOnce({
      data: { success: true, data: userResponseMock },
    });
    expect(await createUser(userMock)).toBe(userResponseMock);
  });
  it('should throw InvalidEmailError correctly', async () => {
    jest.spyOn(axios, 'post').mockRejectedValueOnce({
      response: {
        data: {
          success: false,
          error: { code: 201 },
        },
      },
    });
    await expect(createUser(userMock)).rejects.toThrow(InvalidEmailError);
  });
  it('should throw EmailAlreadyExistsError correctly', async () => {
    jest.spyOn(axios, 'post').mockRejectedValueOnce({
      response: {
        data: {
          success: false,
          error: { code: 401 },
        },
      },
    });
    await expect(createUser(userMock)).rejects.toThrow(EmailAlreadyExistsError);
  });
  it('should throw UserInvalidNameError correctly', async () => {
    jest.spyOn(axios, 'post').mockRejectedValueOnce({
      response: {
        data: {
          success: false,
          error: { code: 402 },
        },
      },
    });
    await expect(createUser(userMock)).rejects.toThrow(UserInvalidNameError);
  });
  it('should throw UserInvalidTypeError correctly', async () => {
    jest.spyOn(axios, 'post').mockRejectedValueOnce({
      response: {
        data: {
          success: false,
          error: { code: 404 },
        },
      },
    });
    await expect(createUser(userMock)).rejects.toThrow(UserInvalidTypeError);
  });
  it('should throw DocumentAlreadyExistsError correctly', async () => {
    jest.spyOn(axios, 'post').mockRejectedValueOnce({
      response: {
        data: {
          success: false,
          error: { code: 408 },
        },
      },
    });
    await expect(createUser(userMock)).rejects.toThrow(DocumentAlreadyExistsError);
  });
  it('should throw PhoneAlreadyExistsError correctly', async () => {
    jest.spyOn(axios, 'post').mockRejectedValueOnce({
      response: {
        data: {
          success: false,
          error: { code: 411 },
        },
      },
    });
    await expect(createUser(userMock)).rejects.toThrow(PhoneAlreadyExistsError);
  });
  it('throws error when request is not successful', async () => {
    jest.spyOn(axios, 'post').mockRejectedValueOnce(new Error('error'));
    await expect(createUser(userMock)).rejects.toThrow('error');
  });
});
describe('user update', () => {
  const userResponseMock = createMock<User>();
  beforeAll(() => {
    setEnvironment(process.env.NODE_ENV ?? 'test');
  });
  afterAll(_unsetEnvConf);
  const userMock: UpdateUserType = createMock<UpdateUserType>();
  it('successfuly creates user with valid data', async () => {
    jest.spyOn(axios, 'put').mockResolvedValueOnce({
      data: { success: true, data: userResponseMock },
    });
    expect(await updateUser(userMock)).toBe(userResponseMock);
  });
  it('should throw InvalidEmailError correctly', async () => {
    jest.spyOn(axios, 'put').mockRejectedValueOnce({
      response: {
        data: {
          success: false,
          error: { code: 201 },
        },
      },
    });
    await expect(updateUser(userMock)).rejects.toThrow(InvalidEmailError);
  });
  it('should throw EmailAlreadyExistsError correctly', async () => {
    jest.spyOn(axios, 'put').mockRejectedValueOnce({
      response: {
        data: {
          success: false,
          error: { code: 401 },
        },
      },
    });
    await expect(updateUser(userMock)).rejects.toThrow(EmailAlreadyExistsError);
  });
  it('should throw UserInvalidNameError correctly', async () => {
    jest.spyOn(axios, 'put').mockRejectedValueOnce({
      response: {
        data: {
          success: false,
          error: { code: 402 },
        },
      },
    });
    await expect(updateUser(userMock)).rejects.toThrow(UserInvalidNameError);
  });
  it('should throw UserInvalidTypeError correctly', async () => {
    jest.spyOn(axios, 'put').mockRejectedValueOnce({
      response: {
        data: {
          success: false,
          error: { code: 404 },
        },
      },
    });
    await expect(updateUser(userMock)).rejects.toThrow(UserInvalidTypeError);
  });
  it('should throw DocumentAlreadyExistsError correctly', async () => {
    jest.spyOn(axios, 'put').mockRejectedValueOnce({
      response: {
        data: {
          success: false,
          error: { code: 408 },
        },
      },
    });
    await expect(updateUser(userMock)).rejects.toThrow(DocumentAlreadyExistsError);
  });
  it('should throw PhoneAlreadyExistsError correctly', async () => {
    jest.spyOn(axios, 'put').mockRejectedValueOnce({
      response: {
        data: {
          success: false,
          error: { code: 411 },
        },
      },
    });
    await expect(updateUser(userMock)).rejects.toThrow(PhoneAlreadyExistsError);
  });
  it('throws error when request is not successful', async () => {
    jest.spyOn(axios, 'put').mockRejectedValueOnce(new Error('error'));
    await expect(updateUser(userMock)).rejects.toThrow('error');
  });
});
describe('user details', () => {
  const detailsMock = createMock<DetailsResponse>();
  beforeAll(() => {
    setEnvironment(process.env.NODE_ENV ?? 'test');
  });
  afterAll(_unsetEnvConf);
  it('successfuly gets user details with valid data', async () => {
    jest.spyOn(axios, 'get').mockResolvedValueOnce({
      data: { success: true, data: detailsMock },
    });
    expect(await getDetails()).toBe(detailsMock);
  });
  it('throws error when request is not successful', async () => {
    jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('error'));
    await expect(getDetails()).rejects.toThrow('error');
  });
});
describe('user origins', () => {
  const responseMock = [{ id: 1, name: 'origin 1' }];
  beforeAll(() => {
    setEnvironment(process.env.NODE_ENV ?? 'test');
  });
  afterAll(_unsetEnvConf);
  it('successfuly gets user origins with valid data', async () => {
    jest.spyOn(axios, 'get').mockResolvedValueOnce({
      data: { success: true, data: responseMock },
    });
    expect(await getOrigins()).toBe(responseMock);
  });
  it('throws error when request is not successful', async () => {
    jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('error'));
    await expect(getOrigins()).rejects.toThrow('error');
  });
});
describe('user company types', () => {
  const responseMock = [{ id: 1, name: 'company type 1' }];
  beforeAll(() => {
    setEnvironment(process.env.NODE_ENV ?? 'test');
  });
  afterAll(_unsetEnvConf);
  it('successfuly gets user company types with valid data', async () => {
    jest.spyOn(axios, 'get').mockResolvedValueOnce({
      data: { success: true, data: responseMock },
    });
    expect(await getCompanyTypes()).toBe(responseMock);
  });
  it('throws error when request is not successful', async () => {
    jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('error'));
    await expect(getCompanyTypes()).rejects.toThrow('error');
  });
});
describe('user support statuses', () => {
  const responseMock = [{ id: 1, name: 'status 1', color: 'red' }];
  beforeAll(() => {
    setEnvironment(process.env.NODE_ENV ?? 'test');
  });
  afterAll(_unsetEnvConf);
  it('successfuly gets user support statuses with valid data', async () => {
    jest.spyOn(axios, 'get').mockResolvedValueOnce({
      data: { success: true, data: responseMock },
    });
    expect(await getSupportStatuses()).toBe(responseMock);
  });
  it('throws error when request is not successful', async () => {
    jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('error'));
    await expect(getSupportStatuses()).rejects.toThrow('error');
  });
});
describe('check email', () => {
  beforeAll(() => {
    setEnvironment(process.env.NODE_ENV ?? 'test');
  });
  afterAll(_unsetEnvConf);
  it('successfuly gets user support statuses with valid data', async () => {
    jest.spyOn(axios, 'get').mockResolvedValueOnce({
      data: { success: true },
    });
    expect(await checkEmail('valid@email.true')).toBe(true);
  });
  it('should throw InvalidEmailError correctly', async () => {
    jest.spyOn(axios, 'get').mockRejectedValueOnce({
      response: {
        data: {
          success: false,
          error: { code: 201 },
        },
      },
    });
    await expect(checkEmail('invalid email address')).rejects.toThrow(InvalidEmailError);
  });
  it('should throw EmailAlreadyExistsError correctly', async () => {
    jest.spyOn(axios, 'get').mockRejectedValueOnce({
      response: {
        data: {
          success: false,
          error: { code: 401 },
        },
      },
    });
    await expect(checkEmail('email@already.exists')).rejects.toThrow(EmailAlreadyExistsError);
  });
  it('throws error when request is not successful', async () => {
    jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('error'));
    await expect(checkEmail('request error')).rejects.toThrow('error');
  });
});
describe('check document', () => {
  beforeAll(() => {
    setEnvironment(process.env.NODE_ENV ?? 'test');
  });
  afterAll(_unsetEnvConf);
  it('successfuly gets user support statuses with valid data', async () => {
    jest.spyOn(axios, 'get').mockResolvedValueOnce({
      data: { success: true },
    });
    expect(await checkDocument('01234567899')).toBe(true);
  });
  it('should throw DocumentAlreadyExistsError correctly', async () => {
    jest.spyOn(axios, 'get').mockRejectedValueOnce({
      response: {
        data: {
          success: false,
          error: { code: 408 },
        },
      },
    });
    await expect(checkDocument('11111111111')).rejects.toThrow(DocumentAlreadyExistsError);
  });
  it('throws error when request is not successful', async () => {
    jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('error'));
    await expect(checkDocument('request error')).rejects.toThrow('error');
  });
});
describe('verification code', () => {
  beforeAll(() => {
    setEnvironment(process.env.NODE_ENV ?? 'test');
  });
  afterAll(_unsetEnvConf);

  it('should let create a phone validation code', async () => {
    jest.spyOn(axios, 'post').mockResolvedValueOnce({
      data: { success: true },
    });
    expect(await createPhoneValidationCode('jest', '11999995555')).toBe(true);
  });
  it('should let create a e-mail validation code', async () => {
    jest.spyOn(axios, 'post').mockResolvedValueOnce({
      data: { success: true },
    });
    expect(await createEmailValidationCode('jest', 'jest@jesting.mock')).toBe(true);
  });
  it('should throw error when request is not successfull', async () => {
    jest.spyOn(axios, 'post').mockRejectedValueOnce(new Error('oops'));
    await expect(createEmailValidationCode('', '')).rejects.toThrow('oops');
  });
  it('should return true when successfully validating code', async () => {
    jest.spyOn(axios, 'post').mockResolvedValueOnce({
      data: { success: true },
    });
    expect(await validateCode('123456', ValidationTypes.EMAIL)).toBe(true);
  });
  it('should throw CodeInvalidDateError correctly', async () => {
    jest.spyOn(axios, 'post').mockRejectedValueOnce({
      response: {
        data: {
          success: false,
          error: { code: 1102 },
        },
      },
    });
    await expect(validateCode('123456', ValidationTypes.EMAIL)).rejects.toThrow(
      CodeInvalidDateError
    );
  });
  it('should throw CodeExpiredError correctly', async () => {
    jest.spyOn(axios, 'post').mockRejectedValueOnce({
      response: {
        data: {
          success: false,
          error: { code: 1101 },
        },
      },
    });
    await expect(validateCode('123456', ValidationTypes.EMAIL)).rejects.toThrow(CodeExpiredError);
  });
  it('should throw CodeInvalidError correctly', async () => {
    jest.spyOn(axios, 'post').mockRejectedValueOnce({
      response: {
        data: {
          success: false,
          error: { code: 1100 },
        },
      },
    });
    await expect(validateCode('123456', ValidationTypes.EMAIL)).rejects.toThrow(CodeInvalidError);
  });
});
