import axios, { AxiosRequestConfig } from 'axios';
import { getBearerAuth, _getEnvConf } from '../config';
import { Response } from '../_base/types/response';
import { City, IBGEError, Locale, _LocaleTypes, LocationNotFoundError, State } from './types';

var _bearer: string | null = ''; 
getBearerAuth().then(token => {_bearer = token});
const _getRequestConfig = (): AxiosRequestConfig => ({
  withCredentials: true,
  headers: {
    authorization: `Bearer ${_bearer}`,
  },
});

/**
 * Get list of brazilian states
 * @returns Promise with array of objects of types [[State]]
 * @throws [[IBGEError]] when the request is not successful
 */
export const getStates = async (): Promise<Array<State>> => {
  try {
    const response = (await axios.get(
      `${_getEnvConf()?.endpoints.api}/v1/locale/states`,
      _getRequestConfig()
    )) as Response;
    return (response.data?.data || []) as Array<State>;
  } catch (e: any) {
    switch (e.response?.data?.error?.code) {
      case 500:
        throw new IBGEError(e.response.data);
      default:
        throw new Error(e.message);
    }
  }
};

/**
 * Get list of cities for a given state
 * @param state id or initials of state to get the list of cities from
 * @returns Promise with array of objects of type [[City]]
 * @throws [[IBGEError]] when the request is not successful
 */
export const getCities = async (state: number | string): Promise<Array<City>> => {
  try {
    const response = (await axios.get(
      `${_getEnvConf()?.endpoints.api}/v1/locale/states/${state}/cities`,
      _getRequestConfig()
    )) as Response;
    return (response.data?.data || []) as Array<City>;
  } catch (e: any) {
    switch (e.response?.data?.error?.code) {
      case 500:
        throw new IBGEError(e.response.data);
      default:
        throw new Error(e.message);
    }
  }
};

/**
 * Get location info for a given ZIP Code and address query
 * @param postalCode zip code for the address you want info about
 * @param search complement of the zip code, address lines
 * @returns Promise with object of type [[Locale]]
 * @throws LocationNotFoundError when the request is not successful
 */
export const getLocaleByZIP = async (
  postalCode: string = '',
  search: string = ''
): Promise<Locale> => {
  try {
    const params = { postalCode, search, type: _LocaleTypes.ADDRESS };
    const response = (await axios.get(`${_getEnvConf()?.endpoints.api}/v1/locale/`, {
      params,
      ..._getRequestConfig(),
    })) as Response;
    return (response.data?.data || {}) as Locale;
  } catch (e: any) {
    switch (e.response?.data?.error?.code) {
      case 800:
        throw new LocationNotFoundError(e.response.data);
      default:
        throw new Error(e.message);
    }
  }
};

/**
 * Get location info for a given latitude and longitude
 * @param latitude latitude you want info about
 * @param longitude longitude you want info about
 * @returns Promise with object of type [[Locale]]
 * @throws LocationNotFoundError when the request is not successful
 */
export const getLocaleByLatLon = async (latitude: number, longitude: number): Promise<Locale> => {
  try {
    const params = { latitude, longitude, type: _LocaleTypes.GEO };
    const response = (await axios.get(`${_getEnvConf()?.endpoints.api}/v1/locale/`, {
      params,
      ..._getRequestConfig(),
    })) as Response;
    return (response.data?.data || {}) as Locale;
  } catch (e: any) {
    switch (e.response?.data?.error?.code) {
      case 800:
        throw new LocationNotFoundError(e.response.data);
      default:
        throw new Error(e.message);
    }
  }
};
