import { CustomError } from '../_base/types/error';

export class IBGEError extends CustomError {}
export class LocationNotFoundError extends CustomError {}

export type State = {
  id: number;
  name: string;
  initials: string;
};

export type City = {
  id: number;
  name: string;
  stateId: number;
};

export type Locale = {
  street: string | false;
  district: string | false;
  city: string | false;
  state: { long: string | false; short: string | false };
  country: { long: string | false; short: string | false };
  postalCode: string | false;
  latitude: number | false;
  longitude: number | false;
  formattedAddress: string | false;
};

export const _LocaleTypes = Object.freeze({
  ADDRESS: 'address',
  GEO: 'geo',
});
