import axios from 'axios';
import { setEnvironment, _unsetEnvConf } from '../config';
import { getStates, getCities, getLocaleByZIP, getLocaleByLatLon } from './';
import { IBGEError, LocationNotFoundError } from './types';

describe('states', () => {
  beforeAll(() => setEnvironment(process.env.NODE_ENV ?? 'test'));
  afterAll(_unsetEnvConf);

  it('should get states successfuly', async () => {
    const expected = [{ id: 1, name: 'Estado', initials: 'ES' }];
    jest.spyOn(axios, 'get').mockResolvedValueOnce({
      data: { success: true, data: expected },
    });
    const result = await getStates();
    expect(typeof result).toBe('object');
    expect(result).toBe(expected);
  });
  it('throws error for unfulfilled request', async () => {
    jest.spyOn(axios, 'get').mockRejectedValueOnce({
      response: { data: { success: false, error: { code: 500, message: 'IBGE error' } } },
    });
    await expect(getStates()).rejects.toThrow(IBGEError);
  });
});

describe('cities', () => {
  beforeAll(() => setEnvironment(process.env.NODE_ENV ?? 'test'));
  afterAll(_unsetEnvConf);

  it('should get cities successfuly', async () => {
    const expected = [{ id: 1, name: 'Cidade', stateId: 5 }];
    jest.spyOn(axios, 'get').mockResolvedValueOnce({
      data: { success: true, data: expected },
    });
    const result = await getCities(5);
    expect(typeof result).toBe('object');
    expect(result).toBe(expected);
  });
  it('throws error for unfulfilled request', async () => {
    jest.spyOn(axios, 'get').mockRejectedValueOnce({
      response: { data: { success: false, error: { code: 500, message: 'IBGE error' } } },
    });
    await expect(getCities(555)).rejects.toThrow(IBGEError);
  });
});

describe('cities', () => {
  beforeAll(() => setEnvironment(process.env.NODE_ENV ?? 'test'));
  afterAll(_unsetEnvConf);

  it('should get cities successfuly', async () => {
    const expected = [{ id: 1, name: 'Cidade', stateId: 5 }];
    jest.spyOn(axios, 'get').mockResolvedValueOnce({
      data: { success: true, data: expected },
    });
    const result = await getCities(5);
    expect(typeof result).toBe('object');
    expect(result).toBe(expected);
  });
  it('throws error for unfulfilled request', async () => {
    jest.spyOn(axios, 'get').mockRejectedValueOnce({
      response: { data: { success: false, error: { code: 500, message: 'IBGE error' } } },
    });
    await expect(getCities(555)).rejects.toThrow(IBGEError);
  });
});

describe('locales', () => {
  beforeAll(() => setEnvironment(process.env.NODE_ENV ?? 'test'));
  afterAll(_unsetEnvConf);

  const expected = {
    street: 'Avenida Marcos Penteado de Ulhôa Rodrigues',
    district: 'Tamboré',
    city: 'Santana de Parnaíba',
    state: { long: 'São Paulo', short: 'SP' },
    country: { long: 'Brasil', short: 'BR' },
    postalCode: '06543-001',
    latitude: -23.4686241,
    longitude: -46.8500218,
    formattedAddress: 'Av. Marcos Penteado de Ulhôa Rodrigues, Santana de Parnaíba - SP, Brasil',
  };

  it('should query address successfuly', async () => {
    jest.spyOn(axios, 'get').mockResolvedValueOnce({
      data: { success: true, data: expected },
    });
    const result = await getLocaleByZIP('06543-001', 'Avenida Marcos Penteado');
    expect(typeof result).toBe('object');
    expect(result).toStrictEqual(expected);
  });
  it('should query latitude and longitude successfuly', async () => {
    jest.spyOn(axios, 'get').mockResolvedValueOnce({
      data: { success: true, data: expected },
    });
    const result = await getLocaleByLatLon(-23.4686241, -46.8500218);
    expect(typeof result).toBe('object');
    expect(result).toStrictEqual(expected);
  });
  it('throws error for unfulfilled address request', async () => {
    jest.spyOn(axios, 'get').mockRejectedValueOnce({
      response: { data: { success: false, error: { code: 800 } } },
    });
    await expect(getLocaleByZIP('')).rejects.toThrow(LocationNotFoundError);
  });
  it('throws error for unfulfilled latitude and longitude request', async () => {
    jest.spyOn(axios, 'get').mockRejectedValueOnce({
      response: {
        data: {
          success: false,
          error: { code: 800 },
        },
      },
    });
    await expect(getLocaleByLatLon(NaN, NaN)).rejects.toThrow(LocationNotFoundError);
  });
});
