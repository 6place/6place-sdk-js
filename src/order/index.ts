/**
 * Manages order related methods
 * @module
 */
import axios, { AxiosRequestConfig } from 'axios';
import { getBearerAuth, _getEnvConf } from '../config';
import { Order, CreateOrderParams, UpdateOrderParams, OrderCancelReason } from './types';

let _bearer: string | null = '';
getBearerAuth().then((token) => {
  _bearer = token;
});

const _getRequestConfig = (): AxiosRequestConfig => ({
  withCredentials: true,
  headers: {
    authorization: `Bearer ${_bearer}`,
  },
});

/**
 * Get all orders by user
 * @returns A promise with a order array if request is successful
 * @throws Error when the request is not successful
 */
export const getOrders = async (providerId?: number): Promise<{ count: number; rows: Order[] }> => {
  const params = { providerId}
  const { data } = await axios.get<any>(
    `${_getEnvConf()?.endpoints.api}/v2/order/`,
    {params,
    ..._getRequestConfig()}
  );

  return data.data;
};

/**
 * Get order by ID
 * @returns A promise with a order object if request is successful
 * @throws Error when the request is not successful
 */
export const getOrder = async (id: number): Promise<Order> => {
  const params = { id };
  const { data } = await axios.get<any>(`${_getEnvConf()?.endpoints.api}/v1/order/${id}`, {
    params,
    ..._getRequestConfig(),
  });

  return data.data;
};

/**
 * Create order
 * @returns A promise void if request is successful
 * @throws Error when the request is not successful
 */
export const createOrder = async (params: CreateOrderParams): Promise<void> => {
  const { data } = await axios.post<any>(
    `${_getEnvConf()?.endpoints.api}/v1/order/`,
    params,
    _getRequestConfig()
  );

  return data.data;
};

/**
 * Update order by ID
 * @returns A promise void if request is successful
 * @throws Error when the request is not successful
 */
export const updateOrder = async (id: number, body: UpdateOrderParams): Promise<void> => {
  const { data } = await axios.put<any>(
    `${_getEnvConf()?.endpoints.api}/v1/order/${id}`,
    body,
    _getRequestConfig()
  );

  return data.data;
};

/**
 * Update Order status by status name
 * @returns A promise void if request is successful
 * @throws Error when the request is not successful
 */
export const updateOrderStatus = async (id: number, status: string): Promise<void> => {
  const { data } = await axios.put<any>(
    `${_getEnvConf()?.endpoints.api}/v1/order/${id}/status`,
    { status },
    _getRequestConfig()
  );

  return data.data;
};

/**
 * Get all order cancellation reasons
 * @returns A promise with a cancel reasons array if request is successful
 * @throws Error when the request is not successful
 */
export const getCancellationReasons = async (): Promise<OrderCancelReason[]> => {
  const { data } = await axios.get<any>(
    `${_getEnvConf()?.endpoints.api}/v1/order/reasons`,
    _getRequestConfig()
  );

  return data.data;
};

/**
 * Create bankslip transaction for Order
 * @returns A promise any if request is successful
 * @throws Error when the request is not successful
 */
export const createBankslip = async (id: number, orderId: number): Promise<any> => {
  const params = { id };
  const body = { orderId };
  const { data } = await axios.post<any>(
    `${_getEnvConf()?.endpoints.api}/v1/order/${id}/bankslip`,
    {
      params,
      body,
      ..._getRequestConfig(),
    }
  );

  return data.data;
};

/**
 * Create pix transaction for Order
 * @returns A promise any if request is successful
 * @throws Error when the request is not successful
 */
export const createPix = async (id: number, orderId: number): Promise<any> => {
  const params = { id };
  const body = { orderId };
  const { data } = await axios.post<any>(`${_getEnvConf()?.endpoints.api}/v1/order/${id}/pix`, {
    params,
    body,
    ..._getRequestConfig(),
  });

  return data.data;
};

/**
 * Renew payment for Order
 * @returns A promise any if request is successful
 * @throws Error when the request is not successful
 */
export const renewBankslip = async (id: number, orderId: number): Promise<any> => {
  const params = { id };
  const body = { orderId };
  const { data } = await axios.post<any>(
    `${_getEnvConf()?.endpoints.api}/v1/order/${id}/payment/renew`,
    {
      params,
      body,
      ..._getRequestConfig(),
    }
  );

  return data.data;
};

/**
 * @deprecated in favor of {@link getOrders}
 */
export const getAllOrdersByUser = getOrders;
