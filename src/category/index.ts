/**
 * Manages user related methods
 * @module
 */
import axios, { AxiosRequestConfig } from 'axios';
import { getBearerAuth, _getEnvConf } from '../config';
import { Category } from './types';

var _bearer: string | null = ''; 
getBearerAuth().then(token => {_bearer = token});

const _getRequestConfig = (): AxiosRequestConfig => ({
  withCredentials: true,
  headers: {
    authorization: `Bearer ${_bearer}`,
  },
});

/**
 * Get all categories
 * @returns A promise with a categories array if request is successful
 * @throws Error when the request is not successful
 */
export const getAllCategories = async (): Promise<Category[]> => {
  const { data } = await axios.get<any>(
    `${_getEnvConf()?.endpoints.api}/v1/category/`,
    _getRequestConfig()
  );

  return data.data;
};

/**
 * Get one category by id
 * @param categoryId to find by that id
 * @returns A promise with a highlight if request was successful
 * @throws Error when the request is not successful
 */
export const getCategory = async (categoryId: number): Promise<Category> => {
  const { data } = await axios.get<any>(
    `${_getEnvConf()?.endpoints.api}/v1/category/${categoryId}`,
    _getRequestConfig()
  );

  return data.data;
};
