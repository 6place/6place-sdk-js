import axios, { AxiosRequestConfig } from 'axios';
import { getBearerAuth, _getEnvConf } from '../config';
import { Response } from '../_base/types/response';
import {
  CreateEvaluation,
  Product,
  ProductEvaluation,
  ProductResponse,
  ProductUnit,
} from './types';

var _bearer: string | null = ''; 
getBearerAuth().then(token => {_bearer = token});
const _getRequestConfig = (): AxiosRequestConfig => ({
  withCredentials: true,
  headers: {
    authorization: `Bearer ${_bearer}`,
  },
});

/**
 * Get list of products with or without details, with optinal filters
 * @param search search query to filter
 * @param providerId ID of the provider to search products from
 * @param isPromotion set true to search only for products with promotion
 * @param isDetail set to true to fetch exta details of the product
 * @param lat latitude of lng to search by location
 * @param lng longitude of the location to search nearest providers
 * @param minValue search for values <= this param
 * @param page number of page to fetch
 * @param limit limit of products to fetch
 * @returns Promise with [[ProductResponse]] with an array of objects of type [[Product]]
 * @throws IBGEError when the request is not successful
 */
export const getProducts = async (
  lat?: number,
  lng?: number,
  search?: string,
  providerId?: number,
  isPromotion?: boolean,
  isDetail?: boolean,
  categoryId?: number,
  page?: number,
  limit?: number,
  minValue?: number
): Promise<ProductResponse> => {
  const params = {
    lat,
    lng,
    search,
    providerId,
    isPromotion,
    isDetail,
    page,
    limit,
    minValue,
    categoryId
  };
  const response = await axios.get<Response>(`${_getEnvConf()?.endpoints.api}/v3/product`, {
    params,
    ..._getRequestConfig(),
  });
  return (response.data?.data || []) as ProductResponse;
};

/**
 * Get product details by id
 * @param id of the product to retrieve
 * @returns A promise with a product array if request is successful
 * @throws Error when the request is not successful
 */
export const getProduct = async (id: number): Promise<Product> => {
  const { data } = await axios.get<Response>(
    `${_getEnvConf()?.endpoints.api}/v1/product/${id}`,
    _getRequestConfig()
  );

  return data.data as Product;
};

/**
 * Get list of product units
 * @returns A promise with a [[ProductUnit]] array if request is successful
 * @throws Error when the request is not successful
 */
export const getMeasureUnits = async (): Promise<ProductUnit[]> => {
  const { data } = await axios.get<Response>(
    `${_getEnvConf()?.endpoints.api}/v1/product/unit`,
    _getRequestConfig()
  );

  return data.data as ProductUnit[];
};

/**
 * Get total number of products for the current user, in the app or in a given area
 * @param latitude latitude you want to filter products
 * @param longitude longitude you want info about
 * @returns A promise with the number of products if request is successful
 * @throws Error when the request is not successful
 */
export const getCount = async (latitude?: number, longitude?: number): Promise<number> => {
  const params = { lat: latitude, lng: longitude };
  const { data } = await axios.get<Response>(`${_getEnvConf()?.endpoints.api}/v1/product/count`, {
    params,
    ..._getRequestConfig(),
  });

  return data.data as number;
};

/**
 * Get all product evaluations from given product ID
 * @param id of the product to retrieve evaluations from
 * @returns A promise with a [[ProductEvaluation]] array if request is successful
 * @throws Error when the request is not successful
 */
export const getEvaluations = async (id: number): Promise<ProductEvaluation[]> => {
  const { data } = await axios.get<Response>(
    `${_getEnvConf()?.endpoints.api}/v1/product/${id}/evaluation`,
    _getRequestConfig()
  );

  return data.data as ProductEvaluation[];
};

/**
 * Get all product evaluations from given product ID
 * @param id of the product to add the evaluation to
 * @param params of the product evaluation
 * @returns A promise with a [[ProductEvaluation]] array if request is successful
 * @throws Error when the request is not successful
 */
export const createEvaluation = async (
  id: number,
  params: CreateEvaluation
): Promise<ProductEvaluation> => {
  const { data } = await axios.post<Response>(
    `${_getEnvConf()?.endpoints.api}/v1/product/${id}/evaluation`,
    params,
    _getRequestConfig()
  );

  return data.data as ProductEvaluation;
};
