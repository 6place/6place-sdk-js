import { Provider } from '../provider/types';
import { Response } from '../_base/types/response';

export type ProductPrice = {
  id?: number;
  productId?: number;
  type: string;
  value: number;
  range?: number;
  commissionPrivate?: number;
  commissionPublic: number;
  taxTiffinPrivate?: number;
  taxTiffinPublic: number;
  deletedAt?: string;
  createdAt?: string;
  updatedAt?: string;
};

export type ProductProviderUnit = {
  productId?: number;
  providerUnitId?: number;
  stock?: number;
  deletedAt?: string;
  createdAt?: string;
  updatedAt?: string;
  unit?: {
    id: number;
    name: string;
    radius: number;
  };
};

export type ProductCategory = {
  productId: number;
  categoryId: number;
  deletedAt: string;
  createdAt: string;
  updatedAt: string;
  category?: {
    id: number;
    name: string;
    imageUrl: string;
    position?: string | number;
    segmentKey: string;
    deletedAt?: string;
    createdAt: string;
    updatedAt: string;
  };
};

export type ProductUnit = {
  id: number;
  code: string;
  name: string;
};

export type Product = {
  id: number;
  name: string;
  score: string;
  code?: string;
  description?: string;
  isVisible: boolean;
  createdAt: string;
  updatedAt: string;
  multiplier: number;
  images?: Array<{
    position: number;
    imageUrl: string;
  }>;
  unit?: ProductUnit;
  promotion?: {
    type: string;
    value: number;
    startedAt?: string;
    finalizedAt?: string;
    showTimer: boolean;
    createdAt: string;
  };
  providerId?: number;
  highlightId?: number;
  unitId?: number;
  weight?: number;
  descriptionType?: string;
  shelfLifeTime?: number;
  shelfLifeType?: string;
  suggestedPrice?: number;
  minimum?: number;
  height: number;
  width: number;
  length: number;
  cubicMeter: number;
  deletedAt: Date;
  provider: Provider;
  prices: ProductPrice[];
  units: ProductProviderUnit[];
  categories: ProductCategory[];
};

export interface ProductResponse extends Response {
  count: number;
  rows: Array<Product>;
}

export type ProductEvaluation = {
  id?: number;
  userId: number;
  productId: number;
  score: number;
  comment?: string;
  answer?: string;
  deletedAt?: string;
  createdAt: string;
  updatedAt: string;
};

export interface CreateEvaluation {
  score: number;
  comment: string;
}
