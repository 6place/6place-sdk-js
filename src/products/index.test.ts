import axios from 'axios';
import { createMock } from 'ts-auto-mock';
import { setEnvironment, _unsetEnvConf } from '../config';
import {
  createEvaluation,
  getCount,
  getEvaluations,
  getProduct,
  getProducts,
  getMeasureUnits,
} from './';
import {
  CreateEvaluation,
  Product,
  ProductEvaluation,
  ProductResponse,
  ProductUnit,
} from './types';

describe('getting products', () => {
  beforeAll(() => setEnvironment(process.env.NODE_ENV ?? 'test'));
  afterAll(_unsetEnvConf);

  it('should get products successfuly', async () => {
    const expected: ProductResponse = createMock<ProductResponse>();
    jest.spyOn(axios, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
    const result = await getProducts();
    expect(typeof result).toBe('object');
    expect(result).toBe(expected);
  });
  it('throws error for unfulfilled request', async () => {
    jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('unknown error'));
    await expect(getProducts()).rejects.toThrow('unknown error');
  });
  it('should get single product successfuly', async () => {
    const expected: Product = createMock<Product>();
    jest.spyOn(axios, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
    const result = await getProduct(123);
    expect(typeof result).toBe('object');
    expect(result).toBe(expected);
  });
  it('throws error for unfulfilled request', async () => {
    jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('unknown error'));
    await expect(getProduct(444)).rejects.toThrow('unknown error');
  });
  it('should count products successfuly', async () => {
    const expected = 123;
    jest.spyOn(axios, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
    const result = await getCount();
    expect(typeof result).toBe('number');
    expect(result).toBe(expected);
  });
  it('throws error for unfulfilled request', async () => {
    jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('unknown error'));
    await expect(getCount()).rejects.toThrow('unknown error');
  });
});

describe('getting product units', () => {
  beforeAll(() => setEnvironment(process.env.NODE_ENV ?? 'test'));
  afterAll(_unsetEnvConf);
  it('should get product units successfuly', async () => {
    const expected: ProductUnit = createMock<ProductUnit>();
    jest.spyOn(axios, 'get').mockResolvedValueOnce({ data: { success: true, data: [expected] } });
    const result = await getMeasureUnits();
    expect(typeof result).toBe('object');
    expect(result).toStrictEqual([expected]);
  });
  it('throws error for unfulfilled request', async () => {
    jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('unknown error'));
    await expect(getMeasureUnits()).rejects.toThrow('unknown error');
  });

  describe('product evaluations', () => {
    beforeAll(() => setEnvironment(process.env.NODE_ENV ?? 'test'));
    afterAll(_unsetEnvConf);
    it('should get product evaluations successfuly', async () => {
      const expected: ProductEvaluation = createMock<ProductEvaluation>();
      jest.spyOn(axios, 'get').mockResolvedValueOnce({ data: { success: true, data: [expected] } });
      const result = await getEvaluations(123);
      expect(typeof result).toBe('object');
      expect(result).toStrictEqual([expected]);
    });
    it('throws error for unfulfilled request', async () => {
      jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('unknown error'));
      await expect(getEvaluations(555)).rejects.toThrow('unknown error');
    });
    it('should create product evaluation successfuly', async () => {
      const params: CreateEvaluation = createMock<CreateEvaluation>();
      const expected: ProductEvaluation = createMock<ProductEvaluation>({
        score: params.score,
        comment: params.comment,
      });
      jest.spyOn(axios, 'post').mockResolvedValueOnce({ data: { success: true, data: expected } });
      const result = await createEvaluation(123, params);
      expect(typeof result).toBe('object');
      expect(result).toStrictEqual(expected);
    });
    it('throws error for unfulfilled request', async () => {
      const params: CreateEvaluation = createMock<CreateEvaluation>();
      jest.spyOn(axios, 'post').mockRejectedValueOnce(new Error('unknown error'));
      await expect(createEvaluation(555, params)).rejects.toThrow('unknown error');
    });
  });
});
