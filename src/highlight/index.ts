/**
 * Manages user related methods
 * @module
 */
import axios, { AxiosRequestConfig } from 'axios';
import { getBearerAuth, _getEnvConf } from '../config';
import { Hightlight } from './types';

var _bearer: string | null = ''; 
getBearerAuth().then(token => {_bearer = token});

const _getRequestConfig = (): AxiosRequestConfig => ({
  withCredentials: true,
  headers: {
    authorization: `Bearer ${_bearer}`,
  },
});

/**
 * Get all highlights
 * @returns A promise with a highlights array if request is successful
 * @throws Error when the request is not successful
 */
export const getAllHighlights = async (): Promise<Hightlight[]> => {
  const { data } = await axios.get<any>(
    `${_getEnvConf()?.endpoints.api}/v1/highlight/`,
    _getRequestConfig()
  );

  return data.data;
};

/**
 * Get one highlight by id
 * @param highlightId to find by that id
 * @returns A promise with a highlight if request was successful
 * @throws Error when the request is not successful
 */
export const getHighlight = async (highlightId: string): Promise<Hightlight> => {
  const { data } = await axios.get<any>(
    `${_getEnvConf()?.endpoints.api}/v1/highlight/${highlightId}`,
    _getRequestConfig()
  );

  return data.data;
};
