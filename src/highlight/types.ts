export type Hightlight = {
  id: number;
  title: string;
  subtitle: string;
  imageUrl: string;
  providerId: string;
  position: number;
  deletedAt: Date;
  createdAt: Date;
  updatedAt: Date;
};
