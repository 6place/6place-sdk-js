/**
 * Manages user related methods
 * @module
 */
 import axios, { AxiosRequestConfig } from 'axios';
 import { getBearerAuth, _getEnvConf } from '../config';
 import { Faq } from './types';
 
 var _bearer: string | null = ''; 
getBearerAuth().then(token => {_bearer = token});
 
 const _getRequestConfig = (): AxiosRequestConfig => ({
   withCredentials: true,
   headers: {
     authorization: `Bearer ${_bearer}`,
   },
 });
 
 /**
  * Get all faqs
  * @returns A promise with a categories array if request is successful
  * @throws Error when the request is not successful
  */
 export const getAllFaqs = async (): Promise<Faq[]> => {
   const { data } = await axios.get<any>(
     `${_getEnvConf()?.endpoints.api}/v1/faq/`,
     _getRequestConfig()
   );
 
   return data.data;
 };
 
 /**
  * Get one faq by id
  * @param faqId to find by that id
  * @returns A promise with a highlight if request was successful
  * @throws Error when the request is not successful
  */
 export const getFaq = async (faqId: number): Promise<Faq> => {
   const { data } = await axios.get<any>(
     `${_getEnvConf()?.endpoints.api}/v1/faq/${faqId}`,
     _getRequestConfig()
   );
 
   return data.data;
 };
 