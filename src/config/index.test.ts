import {
  setEnvironment,
  _getEnvConf,
  getBearerAuth,
  UNKNOWN_ENV_ERROR,
  UNSET_ENV_ERROR,
  _unsetEnvConf,
  Config,
} from './index';

let testConfig: null | Config = null;

describe('config', () => {
  afterAll(_unsetEnvConf);

  it('throws error when env not set', () => {
    expect(_getEnvConf).toThrow(UNSET_ENV_ERROR);
  });
  it('throws error when setting wrong env', () => {
    expect(() => setEnvironment('wrong_environment_123')).toThrow(UNKNOWN_ENV_ERROR);
  });
  it('sets environment returning env config', () => {
    testConfig = setEnvironment(process.env.NODE_ENV ?? 'test');
    expect(typeof testConfig).toBe('object');
    expect(testConfig).toHaveProperty('endpoints');
    expect(testConfig.endpoints).toHaveProperty('api');
    expect(testConfig.endpoints).toHaveProperty('chat');
  });
  it('sets environment returning env config', () => {
    const newTestConfig = setEnvironment(process.env.NODE_ENV ?? 'test');
    expect(newTestConfig).toBe(testConfig);
  });
  it('get env config correctly', () => {
    const newTestConfig = _getEnvConf();
    expect(newTestConfig).toBe(testConfig);
  });
});
