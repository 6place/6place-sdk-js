/**
 * Application configurations methods
 * @module
 */
const config = {
  development: {
    endpoints: { api: 'http://localhost:3333', chat: 'http://localhost:3334' },
  },
  developmentv1: {
    endpoints: { api: 'https://api.dev.v1.b2b.6place.com.br', chat: 'https://chat.dev.v1.b2b.6place.com.br' },
  },
  developmentv2: {
    endpoints: { api: 'https://api.dev.v2.b2b.6place.com.br', chat: 'https://chat.dev.v2.b2b.6place.com.br' },
  },
  test: {
    endpoints: { api: 'http://localhost:3333', chat: 'http://localhost:3334' },
  },
  staging: {
    endpoints: {
      api: 'https://api.b2b.staging.6place.com.br',
      chat: 'https://chat.b2b.staging.6place.com.br',
    },
  },
  production: {
    endpoints: { api: 'https://api.b2b.6place.com.br', chat: 'https://chat.b2b.6place.com.br' },
  },
};

export type ConfigKeys = keyof typeof config;
export type Config = typeof config[ConfigKeys];
export let envConfig: null | Config = null;

/**
 * Error for when setting an unexistent environment
 */
export const UNKNOWN_ENV_ERROR = new Error(
  `Environment does not exist. Please use one of the following: [${Object.keys(config).join(', ')}]`
);

/**
 * Error for when the user is trying to use the API without setting an environment first
 */
export const UNSET_ENV_ERROR = new Error(
  `Environment not set. Please set environment before using the SDK: [${Object.keys(config).join(
    ', '
  )}]`
);

/**
 * Get configuration for current environment
 * @returns object with the current environment configuration
 * @throws [[UNSET_ENV_ERROR]]
 */
export const _getEnvConf = (): Config => {
  if (envConfig === null) {
    throw UNSET_ENV_ERROR;
  }
  return envConfig;
};

/**
 * Get configuration for current environment
 * @returns object with the current environment configuration
 * @throws [[UNSET_ENV_ERROR]]
 */
export const _unsetEnvConf = (): null => {
  return (envConfig = null);
};

/**
 * Set and keep environment set from the app
 * @returns object with configuration from the env set
 * @throws [[UNKNOWN_ENV_ERROR]]
 */
export const setEnvironment = (env: string): Config => {
  if (envConfig !== null) {
    return envConfig;
  }
  if (!(env in config)) {
    throw UNKNOWN_ENV_ERROR;
  }
  return (envConfig = config[env as ConfigKeys]);
};

/**
 * Gets string fot Basic Authorization header to communicate with API
 * @ignore
 */
export const getBearerAuth = async () => {
  let item: string | null = '';
  if (typeof window !== 'undefined') {
    item = await localStorage.getItem('token');
  }
  return item;
};

export const setBearerAuth = async (token: string) => {
  if (typeof window !== 'undefined') {
    await localStorage.setItem('token', token);
  }
};

export const getBasicAuth = () =>
  'ZDUwZTA1ZTJjN2M5NmZmMzY0NjYwYTdiOWM5M2ZmNDc6QlQ5bUt5a3lQVFBtR000RTlLTGRnMkR2dEdwM2pFRzV1S0RQUnQ1QnlLUHcyR0Z5eXFFQXFndmRjOUNHWUZlUXpmSFhnaw==';
