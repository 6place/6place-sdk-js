import axios from 'axios';
import { createMock } from 'ts-auto-mock';
import { setEnvironment, _unsetEnvConf } from '../config';
import { getAddresses, createAddress, updateAddress, deleteAddress } from './';
import { Address, CreateAddressParams } from './types';

describe('address', () => {
  beforeAll(() => setEnvironment(process.env.NODE_ENV ?? 'test'));
  afterAll(_unsetEnvConf);

  const mockAddress: Address = createMock<Address>();
  const mockAddressParams: CreateAddressParams = createMock<CreateAddressParams>();

  it('should get addresses successfuly', async () => {
    const expected = [mockAddress];
    jest.spyOn(axios, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
    const result = await getAddresses();
    expect(typeof result).toBe('object');
    expect(result).toBe(expected);
  });
  it('throws error for unfulfilled request', async () => {
    jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('unknown error'));
    await expect(getAddresses()).rejects.toThrow('unknown error');
  });
  it('should create address successfuly', async () => {
    const expected = [mockAddress];
    jest.spyOn(axios, 'post').mockResolvedValueOnce({ data: { success: true, data: expected } });
    const result = await createAddress(mockAddressParams);
    expect(typeof result).toBe('object');
    expect(result).toBe(expected);
  });
  it('throws error for unfulfilled request create', async () => {
    jest.spyOn(axios, 'post').mockRejectedValueOnce(new Error('unknown error'));
    await expect(createAddress(mockAddressParams)).rejects.toThrow('unknown error');
  });
  it('should update address successfuly', async () => {
    const expected = [mockAddress];
    jest.spyOn(axios, 'patch').mockResolvedValueOnce({ data: { success: true, data: expected } });
    const result = await updateAddress(123, mockAddressParams);
    expect(typeof result).toBe('object');
    expect(result).toBe(expected);
  });
  it('throws error for unfulfilled request update', async () => {
    jest.spyOn(axios, 'patch').mockRejectedValueOnce(new Error('unknown error'));
    await expect(updateAddress(123, mockAddressParams)).rejects.toThrow('unknown error');
  });
  it('should delete address successfuly', async () => {
    const expected = { id: 123 };
    jest.spyOn(axios, 'delete').mockResolvedValueOnce({ data: { success: true, data: expected } });
    const result = await deleteAddress(mockAddress.id);
    expect(typeof result).toBe('object');
    expect(result).toStrictEqual(expected);
  });
  it('throws error for unfulfilled request delete', async () => {
    jest.spyOn(axios, 'delete').mockRejectedValueOnce(new Error('unknown error'));
    await expect(deleteAddress(3)).rejects.toThrow('unknown error');
  });
});
