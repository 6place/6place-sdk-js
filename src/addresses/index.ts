import axios, { AxiosRequestConfig } from 'axios';
import { getBearerAuth, _getEnvConf } from '../config';
import { Response } from '../_base/types/response';
import { Address, CreateAddressParams } from './types';

var _bearer: string | null = ''; 
getBearerAuth().then(token => {_bearer = token});

const _getRequestConfig = (): AxiosRequestConfig => ({
  withCredentials: true,
  headers: {
    authorization: `Bearer ${_bearer}`,
  },
});

/**
 * Get users list of addresses from logged in user
 * @returns Promise with array of objects of types [[Address]]
 * @throws Error when the request is not successful
 */
export const getAddresses = async (): Promise<Array<Address>> => {
  const response = (await axios.get(`${_getEnvConf()?.endpoints.api}/v1/address`, 
    _getRequestConfig(),
  )) as Response;
  return (response.data?.data || []) as Array<Address>;
};

/**
 * Create address for current logged in user
 * @param data Object of type [[CreateAddressParams]]
 * @returns Promise with address [[Address]]
 * @throws Error when the request is not successful
 */
export const createAddress = async (data: CreateAddressParams): Promise<Address> => {

  const response = (await axios.post(`${_getEnvConf()?.endpoints.api}/v1/address`, 
    data,
    _getRequestConfig(),
  )) as Response;

  return (response.data?.data || []) as Address;
};

/**
 * Update given ID address of logged in user
 * @param id ID of the address to update
 * @param data Object of type [[CreateAddressParams]]
 * @returns Promise with address [[Address]]
 * @throws Error when the request is not successful
 */
export const updateAddress = async (id: number, data: CreateAddressParams): Promise<Address> => {
  const response = (await axios.patch(`${_getEnvConf()?.endpoints.api}/v1/address/${id}`, 
    data,
    _getRequestConfig(),
  )) as Response;
  return (response.data?.data || []) as Address;
};

/**
 * Delete logged in user address for the given ID
 * @param id ID of the address to delete
 * @returns id of the deleted registry
 * @throws Error when the request is not successful
 */
export const deleteAddress = async (id: number): Promise<number> => {
  const response = (await axios.delete(`${_getEnvConf()?.endpoints.api}/v1/address/${id}`, 
    _getRequestConfig(),
  )) as Response;
  return response.data?.data;
};
