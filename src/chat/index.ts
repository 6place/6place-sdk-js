/**
 * Manages user related methods
 * @module
 */
import axios, { AxiosRequestConfig } from 'axios';
import { getBearerAuth, _getEnvConf } from '../config';
import { Response } from '../_base/types/response';
import { Chat, ChatNotFoundError, ChatRequestOptions, ReferenceTypes } from './types';

var _bearer: string | null = ''; 
getBearerAuth().then(token => {_bearer = token});

const _getRequestConfig = (): AxiosRequestConfig => ({
  withCredentials: true,
  headers: {
    authorization: `Bearer ${_bearer}`,
  },
});

/**
 * Get a single chat data from given ID
 * @param id ID of the chat to be fetched
 * @returns A promise with a [[Chat]] object if request was successful
 * @throws Error or [[ChatNotFoundError]] when request is not successful
 */
export const getChat = async (id: number): Promise<Chat> => {
  try {
    const response = (await axios.get(
      `${_getEnvConf()?.endpoints.chat}/v1/chat/${id}`,
      _getRequestConfig()
    )) as Response;
    return response.data?.data as Chat;
  } catch (e: any) {
    switch (e.response?.data?.error?.code) {
      case 1600:
        throw new ChatNotFoundError(e.response.data);
      default:
        throw new Error(e.message);
    }
  }
};

/**
 * Get all chats from given reference and type
 * @param referenceId ID of the user/provider you want to get messages from
 * @param referenceType ID of the user/provider you want to get messages from
 * @param options object of type [[ChatRequestOptions]] with other options to filter and sort query
 * @returns A promise with an array of [[Chat]] object if request was successful
 * @throws Error or [[ChatNotFoundError]] when request is not successful
 */
export const getChats = async (
  referenceId: number,
  referenceType: ReferenceTypes,
  options: ChatRequestOptions
): Promise<Chat> => {
  try {
    const params = { referenceId, referenceType, ...options };
    const response = (await axios.get(`${_getEnvConf()?.endpoints.chat}/v1/chat/`, {
      params,
      ..._getRequestConfig(),
    })) as Response;
    return response.data?.data as Chat;
  } catch (e: any) {
    switch (e.response?.data?.error?.code) {
      default:
        throw new Error(e.message);
    }
  }
};
