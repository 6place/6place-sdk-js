import axios from 'axios';
import { createMock } from 'ts-auto-mock';
import { setEnvironment, _unsetEnvConf } from '../config';
import { getChat, getChats } from './';
import { Chat, ChatNotFoundError, ReferenceTypes } from './types';

describe('chat', () => {
  beforeAll(() => setEnvironment(process.env.NODE_ENV ?? 'test'));
  afterAll(_unsetEnvConf);

  const mockChat: Chat = createMock<Chat>();
  describe('getting a single chat', () => {
    it('should get a chat by id successfuly', async () => {
      const expected = mockChat;
      jest.spyOn(axios, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
      const result = await getChat(123);
      expect(typeof result).toBe('object');
      expect(result).toBe(expected);
    });
    it('throws ChatNotFoundError for unfulfilled request', async () => {
      jest.spyOn(axios, 'get').mockRejectedValueOnce({
        response: { data: { success: false, error: { code: 1600 } } },
      });
      await expect(getChat(123)).rejects.toThrow(ChatNotFoundError);
    });
    it('throws default Error for unfulfilled request', async () => {
      jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('jest error'));
      await expect(getChat(123)).rejects.toThrow('jest error');
    });
  });
  describe('getting multiple chats', () => {
    it('should get chats successfuly', async () => {
      const expected = { count: 1, rows: [mockChat] };
      jest.spyOn(axios, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
      const result = await getChats(123, ReferenceTypes.PROVIDER, {});
      expect(typeof result).toBe('object');
      expect(result).toBe(expected);
    });
    it('throws default Error for unfulfilled request', async () => {
      jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('jest error'));
      await expect(getChats(123, ReferenceTypes.USER, {})).rejects.toThrow('jest error');
    });
  });
  describe('getting chat messages', () => {
    it('should get all messages successfully', async () => {
      const expected = { count: 1, rows: [mockChat] };
      jest.spyOn(axios, 'get').mockResolvedValueOnce({ data: { success: true, data: expected } });
      const result = await getChats(123, ReferenceTypes.PROVIDER, {});
      expect(typeof result).toBe('object');
      expect(result).toBe(expected);
    });
    it('throws default Error for unfulfilled request', async () => {
      jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('jest error'));
      await expect(getChats(123, ReferenceTypes.USER, {})).rejects.toThrow('jest error');
    });
  });
});
