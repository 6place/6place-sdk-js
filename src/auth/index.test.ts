/**
 * @jest-environment jsdom
 */

import axios from 'axios';
import { setEnvironment, _unsetEnvConf } from '../config';
import { isLoggedIn, loginUser, logoutUser, _refreshToken } from './index';

describe('auth with environment', () => {
  beforeAll(() => setEnvironment(process.env.NODE_ENV ?? 'test'));
  afterAll(_unsetEnvConf);

  it('should return true when login request is successful', async () => {
    jest.spyOn(axios, 'post').mockResolvedValueOnce({ data: {} });
    expect(await loginUser('mock-valid-username', '123456')).toBe(true);
  });
  it('should return boolean when status is requested', async () => {
    jest.spyOn(axios, 'get').mockResolvedValueOnce({ data: { data: { loggedIn: true } } });
    expect(await isLoggedIn()).toBe(true);
    jest.spyOn(axios, 'get').mockResolvedValueOnce({ data: { data: { loggedIn: false } } });
    expect(await isLoggedIn()).toBe(false);
    jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error());
    expect(await isLoggedIn()).toBe(false);
  });
  it('refresh should return true when request is successful', async () => {
    jest.spyOn(axios, 'post').mockResolvedValueOnce({ data: {} });
    expect(await _refreshToken()).toBe(true);
  });
  it('should always return true on logout', async () => {
    jest.spyOn(axios, 'post').mockResolvedValueOnce({ data: {} });
    expect(await logoutUser()).toBe(true);
    jest.spyOn(axios, 'post').mockRejectedValueOnce(new Error());
    expect(await logoutUser()).toBe(true);
  });
});
