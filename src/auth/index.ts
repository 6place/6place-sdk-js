/**
 * Manages user authentication methods and session data
 * @module
 */
import axios, { AxiosRequestConfig } from 'axios';
import { getBearerAuth, _getEnvConf, setBearerAuth, getBasicAuth } from '../config';

const _bearer = getBearerAuth();

const _getRequestConfig = (): AxiosRequestConfig => ({
  withCredentials: true,
  headers: {
    authorization: `Bearer ${_bearer}`,
  },
});

/**
 * Logs user using cookies to manage session.
 * @param username e-mail used to login
 * @param password password in plain text
 * @returns A promise with a boolean true if login was successful
 * @throws Error when the request is not successful
 */
export const loginUser = async (username: string, password: string): Promise<string> => {
  const params = new URLSearchParams();
  var token = '';
  params.append('username', username);
  params.append('password', password);
  params.append('grant_type', 'password');
  const response = await axios.post<any>(
    `${_getEnvConf()?.endpoints.api}/v1/oauth/token`,
    params,
    {
      withCredentials: true,
      headers: {
        authorization: `Basic ZDUwZTA1ZTJjN2M5NmZmMzY0NjYwYTdiOWM5M2ZmNDc6QlQ5bUt5a3lQVFBtR000RTlLTGRnMkR2dEdwM2pFRzV1S0RQUnQ1QnlLUHcyR0Z5eXFFQXFndmRjOUNHWUZlUXpmSFhnaw==`,
      },
    }
  );

  var authResult = response.data

  if (authResult.token_type == "Bearer") {
    await setBearerAuth(authResult.access_token);
    token = authResult.access_token;
  }

  return token;
};

/**
 * Checks if the user is logged in by refreshing token
 * @returns boolean
 */
export const isLoggedIn = async (): Promise<boolean> => {
  try {
    const status = await axios.get<any>(
      `${_getEnvConf()?.endpoints.api}/v1/oauth/status`,
      _getRequestConfig()
    );
    return status.data.data.loggedIn;
  } catch (err) {
    return false;
  }
};

/**
 * Refreshes token
 * @internal
 * @returns Promise with boolean true
 * @throws Error when the request is not successful
 */
export const _refreshToken = async (): Promise<boolean> => {
  const params = new URLSearchParams();
  params.append('grant_type', 'refresh_token');
  await axios.post<any>(
    `${_getEnvConf()?.endpoints.api}/v1/oauth/token`,
    params,
    _getRequestConfig()
  );
  return true;
};

/**
 * Logs out the user from the API, erasing session data and related cookies
 * @returns Promise with boolean true always
 */
export const logoutUser = async (): Promise<boolean> => {
  try {
    const params = new URLSearchParams();
    await axios.post<any>(
      `${_getEnvConf()?.endpoints.api}/v1/oauth/revoke`,
      params,
      _getRequestConfig()
    );
  } catch (err) { }

  if (typeof window !== 'undefined')
    localStorage.removeItem('token');
    
  return true;
};
