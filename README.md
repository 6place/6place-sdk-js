# SDK para API b2b

## Documentação

A documentação encontra-se na pasta `./docs` - documentação gerada por [typedoc](https://typedoc.org/)

## Desenvolvimento

Para iniciar o desenvolvimento, é recomendada a aplicação de TDD. Com o comando `yarn && yarn start` você deve entrar no modo _watch_ do [jest](https://jestjs.io/).

Para publicação, você deve, na Branch `master`, executar o comando `yarn && yarn build`.