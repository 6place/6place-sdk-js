module.exports = {
  coverageReporters: ['html', 'text', 'text-summary', 'cobertura'],
  preset: 'ts-jest',
  rootDir: './src',
  collectCoverage: true,
  coverageDirectory: '../coverage',
  transform: {
    '^.+\\.(ts|tsx)?$': 'ts-jest',
  },
  globals: {
    'ts-jest': {
      compiler: 'ttypescript',
    },
  },
  setupFiles: ['<rootDir>test-config.ts'],
};
